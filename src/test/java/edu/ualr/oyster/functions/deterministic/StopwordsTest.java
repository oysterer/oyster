package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class StopwordsTest {
	
	private void createWords(Stopwords func) {
		List<String> words = new ArrayList<String>();
		words.add("John");
		words.add("JANE");
		words.add("JOE");
		words.add("James");
		func.addAllWords(words);
	}

	@Test
	public void testConstructor() {
		Stopwords func = new Stopwords();
		assertEquals(FunctionType.STOPWORDS, func.getType());
		assertEquals(FunctionType.STOPWORDS.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Stopwords func = new Stopwords();
		createWords(func);
		assertEquals("abcdef",func.transform("abcdef"));
		assertEquals("",func.transform("JOHN"));
	}

	@Test
	public void testTokenize() {
		Stopwords func = new Stopwords();
		createWords(func);
		assertEquals("abcdef",func.tokenize("abcdef")[0]);
		assertEquals("",func.tokenize("JOHN")[0]);
	}

	@Test
	public void testCompare() {
		Stopwords func = new Stopwords();
		createWords(func);
		assertTrue(func.compare("JACK","JACK"));
		assertFalse(func.compare("JOHN","JANE"));
		assertFalse(func.compare("JACK","JOE"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("STOPWORDS(JOHN|JANE|JOE|James)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			// Not stop words
			assertTrue(func.compare("JACK","JACK"));
			// Two stop words !=
			assertFalse(func.compare("JOHN","JANE"));
			// One Stop word, one not
			assertFalse(func.compare("JACK","JOE"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("STOPWORDS").build();
		if (func != null) {
		}
	}
}
