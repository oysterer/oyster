package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class MissingTest {

	@Test
	public void testConstructor() {
		Missing func = new Missing();
		assertEquals(FunctionType.MISSING, func.getType());
		assertEquals(FunctionType.MISSING.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Missing func = new Missing();
		assertEquals("ABCDEF",func.transform("ABCDEF"));
		assertEquals("",func.transform(null));
	}

	@Test
	public void testTokenize() {
		Missing func = new Missing();
		String [] tokens = func.tokenize("ABCDEF");
		assertEquals(1,tokens.length);
		assertEquals("ABCDEF",tokens[0]);
	}

	@Test
	public void testCompareEither() {
		Missing func = new Missing();
		func.setMode("either");
		assertTrue(func.compare("","ABCDEF"));
		assertTrue(func.compare("ABCDEF",""));
		assertTrue(func.compare("",""));
		assertFalse(func.compare("ABCDEF","abcdef"));
	}

	@Test
	public void testCompareBoth() {
		Missing func = new Missing();
		func.setMode("both");
		assertFalse(func.compare("","ABCDEF"));
		assertFalse(func.compare("ABCDEF",""));
		assertTrue(func.compare("",""));
		assertFalse(func.compare("ABCDEF","abcdef"));
	}

	@Test
	public void testBadMode() {
		Missing func = new Missing();
		try {
		func.setMode("wrong-mode");
		} catch (IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Missing").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDEF",""));
		}
	}

	@Test
	public void testBuildAndCompareBoth() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Missing(both)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare(null,""));
			assertTrue(func.compare("",""));
			assertFalse(func.compare("ABCDEF",""));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Missing(XYZ)").build();
		if (func != null) {
		}
	}
}
