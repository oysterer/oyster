package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class Caverphone2Test {

	@Test
	public void testConstructor() {
		Caverphone2 func = new Caverphone2();
		assertEquals(FunctionType.CAVERPHONE2, func.getType());
		assertEquals(FunctionType.CAVERPHONE2.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Caverphone2 func = new Caverphone2();
		assertEquals("APKTF11111",func.transform("abcdef"));
	}

	@Test
	public void testTokenize() {
		Caverphone2 func = new Caverphone2();
		assertEquals("APKTF11111",func.tokenize("abcdef")[0]);
	}

	@Test
	public void testCompare() {
		Caverphone2 func = new Caverphone2();
		assertTrue(func.compare("APKTF11111","abcdef"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Caverphone2").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("APKTF11111","abcdef"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("CAVERPHONE2(ABC)").build();
		if (func != null) {
		}
	}
}
