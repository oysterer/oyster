package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.deterministic.Substring.Mode;

public class SubstringTest {

	@Test
	public void testConstructor() {
		Substring func = new Substring();
		assertEquals(FunctionType.SUBSTR, func.getType());
		assertEquals(FunctionType.SUBSTR.getFunctionName(),func.getName());
	}

	@Test
	public void testLeftTransform() {
		Substring func = new Substring();
		func.setMode(Mode.LEFT);
		func.setLength(4);
		assertEquals("ABCD",func.transform("ABCDEF"));
		assertEquals("ABC",func.transform("ABC"));
	}

	@Test
	public void testLeftTokenize() {
		Substring func = new Substring();
		func.setMode(Mode.LEFT);
		func.setLength(4);
		assertEquals("ABCD",func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testLeftCompare() {
		Substring func = new Substring();
		func.setMode(Mode.LEFT);
		func.setLength(4);
		assertTrue(func.compare("ABCD123","ABCDEFG"));
	}
	
	@Test
	public void testMidTransform() {
		Substring func = new Substring();
		func.setMode(Mode.MID);
		func.setStart(2);
		func.setLength(3);
		assertEquals("CDE",func.transform("ABCDEF"));
		assertEquals("C",func.transform("ABC"));
		assertEquals("",func.transform("AB"));
	}

	@Test
	public void testMidTokenize() {
		Substring func = new Substring();
		func.setMode(Mode.MID);
		func.setStart(2);
		func.setLength(3);
		assertEquals("CDE",func.tokenize("ABCDEF")[0]);
	}
	
	@Test
	public void testMidCompare() {
		Substring func = new Substring();
		func.setMode(Mode.MID);
		func.setStart(2);
		func.setLength(3);
		assertTrue(func.compare("12CDE678","ABCDEFGH"));
	}

	@Test
	public void testRightTransform() {
		Substring func = new Substring();
		func.setMode(Mode.RIGHT);
		func.setLength(4);
		assertEquals("CDEF",func.transform("ABCDEF"));
		assertEquals("ABC",func.transform("ABC"));
	}

	@Test
	public void testRightTokenize() {
		Substring func = new Substring();
		func.setMode(Mode.RIGHT);
		func.setLength(4);
		assertEquals("CDEF",func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testRightCompare() {
		Substring func = new Substring();
		func.setMode(Mode.RIGHT);
		func.setLength(4);
		assertTrue(func.compare("123DEFG","ABCDEFG"));
	}

	@Test
	public void testProperTransform() {
		Substring func = new Substring();
		func.setMode(Mode.PROPER);
		func.setLength(4);
		assertEquals("ABCDEF",func.transform("ABCDEF"));
	}

	@Test
	public void testProperCompare() {
		Substring func = new Substring();
		func.setMode(Mode.PROPER);
		func.setLength(3);
		assertTrue(func.compare("ABCD123","D12"));
		assertTrue(func.compare("CD12","ABCD123"));
		assertFalse(func.compare("ABCD123","D1"));
	}
		
	@Test
	public void testLeftBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStr(LEFT,4)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCD123","ABCDEDFG"));
		}
	}
	
	@Test
	public void testMidBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStr(MID,2,3)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("12CDE678","ABCDEFGH"));
		}
	}
	
	@Test
	public void testRightBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStr(RIGHT,4)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("123DEFG","ABCDEFG"));
		}
	}
	
	@Test
	public void testProperBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStr(PROPER,3)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("CD12","ABCD123"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SubStr").build();
		if (func != null) {
		}
	}
}
