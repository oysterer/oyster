package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

/**
 * Unit test as a sampl of using OysterFunction and the defined interfaces
 * 
 * In the RuleTerm or IndexElement you would define a property such as
 * 
 * 	private Transform dataPrep and initialize it as illustrated below
 *  or
 *  private Comparator comparator
 *  
 *  At the time the program wants to do a transformation or a comparison it
 *  doesn't really care what class is doing it, only that the method exists.
 *
 */
public class ScanTest {
	
	@Test
	public void testConstructor() {
		Scan func = new Scan();
		assertEquals(FunctionType.SCAN, func.getType());
		assertEquals(FunctionType.SCAN.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Scan func = new Scan();
		func.configure("LR,ALPHA,3,ToUpper,SameOrder");
		assertEquals("TES",func.transform("Test123"));
	}

	@Test
	public void testTokenize() {
		Scan func = new Scan();
		func.configure("LR,ALPHA,3,ToUpper,SameOrder");
		assertEquals("TES",func.tokenize("Test123")[0]);
	}

	@Test
	public void testCompare() {
		Scan func = new Scan();
		func.configure("LR,ALPHA,2,ToLower,SameOrder");
		assertTrue(func.compare("Test123","Testing"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("!SCAN(LR,ALPHA,2,ToLower,SameOrder)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertFalse(func.compare("Test123","Testing"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SCAN()").build();
		if (func != null) {
		}
	}
}
