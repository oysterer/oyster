package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class SubstringMidTest {

	@Test
	public void testConstructor() {
		SubstringMid func = new SubstringMid();
		assertEquals(FunctionType.SUBSTRMID, func.getType());
		assertEquals(FunctionType.SUBSTRMID.getFunctionName(), func.getName());
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testInvalidStart() {
		SubstringMid func = new SubstringMid();
		func.setStart(-1);
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testInvalidLength() {
		SubstringMid func = new SubstringMid();
		func.setLength(0);
	}

	@Test
	public void testTransform() {
		SubstringMid func = new SubstringMid();
		func.setStart(2);
		func.setLength(3);
		assertEquals("CDE", func.transform("ABCDEF"));
		assertEquals("C", func.transform("ABC"));
		assertEquals("", func.transform("AB"));
	}

	@Test
	public void testTokenize() {
		SubstringMid func = new SubstringMid();
		func.setStart(2);
		func.setLength(3);
		assertEquals("CDE", func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testCompare() {
		SubstringMid func = new SubstringMid();
		func.setStart(2);
		func.setLength(3);
		assertTrue(func.compare("12CDE678", "ABCDEFGH"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStrMid(2,3)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("12CDE678", "ABCDEFGH"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SubStrMid").build();
		if (func != null) {
		}
	}

}
