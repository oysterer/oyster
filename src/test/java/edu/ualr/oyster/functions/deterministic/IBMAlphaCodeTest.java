package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class IBMAlphaCodeTest {

	@Test
	public void testConstructor() {
		IBMAlphaCode func = new IBMAlphaCode();
		assertEquals(FunctionType.IBMALPHACODE, func.getType());
		assertEquals(FunctionType.IBMALPHACODE.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		IBMAlphaCode func = new IBMAlphaCode();
		
	    // FIXME: The article shows Kant returning 02100000000000 and Knuth returning
	    // 07210000000000 I cannot get those results. In fact I get the exact oppisite
	    // could it be possible that the author reversed these by accident?

		assertEquals("07210000000000",func.transform("Kant"));
		assertEquals("02100000000000",func.transform("Knuth"));
	}

	@Test
	public void testTokenize() {
		IBMAlphaCode func = new IBMAlphaCode();
		assertEquals("07210000000000",func.tokenize("Kant")[0]);
	}

	@Test
	public void testCompare() {
		IBMAlphaCode func = new IBMAlphaCode();
		assertTrue(func.compare("Nuth","Knuth"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("IBMAlphaCode").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Nuth","Knuth"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("IBMAlphaCode(ABC)").build();
		if (func != null) {
		}
	}
}
