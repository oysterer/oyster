package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class MetaphoneTest {
	
	@Test
	public void testConstructor() {
		Metaphone func = new Metaphone();
		assertEquals(FunctionType.METAPHONE, func.getType());
		assertEquals(FunctionType.METAPHONE.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Metaphone func = new Metaphone();
		assertEquals("STFN",func.transform("Steffen"));
		assertEquals("STFN",func.transform("Stephen"));
		assertEquals("FLS",func.transform("FALLIS"));
		assertEquals("FLS",func.transform("VALLIS"));
		assertEquals("KLN",func.transform("KLINE"));
		assertEquals("KLN",func.transform("CLINE"));
	}

	@Test
	public void testTokenize() {
		Metaphone func = new Metaphone();
		assertEquals("STFN",func.tokenize("Steffen")[0]);
	}

	@Test
	public void testCompare() {
		Metaphone func = new Metaphone();
		assertTrue(func.compare("Steffen","STFN"));
		assertFalse(func.compare("Steffen",""));
		assertFalse(func.compare(null,"STFN"));
		assertFalse(func.compare(null,""));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("metaphone").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Steffen","STFN"));
			assertFalse(func.compare("Steffen",""));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("METAPHONE(ABC)").build();
		if (func != null) {
		}
	}
}
