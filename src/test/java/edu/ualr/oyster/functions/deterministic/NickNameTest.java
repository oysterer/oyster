package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class NickNameTest {
	
	@Test
	public void testConstructor() {
		NickName func = new NickName();
		assertEquals(FunctionType.NICKNAME, func.getType());
		assertEquals(FunctionType.NICKNAME.getFunctionName(),func.getName());
	}

	@Test
	public void testConfigureDeafult() {
		System.out.println("testConfigureDefault");
		NickName func = new NickName();
		func.configure(null);

		assertTrue(func.compare("ABIJAH", "AB"));
		assertFalse(func.compare("ABIJAH", "ZEBRA"));
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testConfigureBad() {
		System.out.println("testConfigureBad");
		NickName func = new NickName();
		func.configure("data/bad-alias.dat");
	}

	@Test
	public void testBuildAndCompare() {
		System.out.println("testBuildAndCompare");
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("NICKNAME").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABIJAH", "AB"));
			assertFalse(func.compare("ABIJAH", "ZEBRA"));
		}
	}
}
