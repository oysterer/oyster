package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;

public class TransposeTest {

	/*
	 * Instantiate the function directly
	 */
	@Test
	public void testCompare() {
		Transpose func = new Transpose();
		assertTrue(func.compare("ABCDEF", "ABCEDF"));
		assertTrue(func.compare("ABCDEF", "ABCEDF"));
		assertTrue(func.compare("ABCDEF", "ABC"));
		assertFalse(func.compare("ABCDEF", "ABCFDE"));
		assertFalse(func.compare(null, null));
		assertFalse(func.compare("ABCDEF", null));
		assertFalse(func.compare("", ""));
		assertFalse(func.compare("", "ABCDEF"));
	}

	/*
	 * Get the function via the Function builder
	 * Use negative logic
	 */
	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("!TRANSPOSE").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertFalse(func.compare("ABCDEF", "ABCEDF"));
			assertFalse(func.compare("ABCDEF", "ABC"));
			assertTrue(func.compare("ABCDEF", "ABCFDE"));
			assertTrue(func.compare(null, null));
			assertTrue(func.compare("ABCDEF", null));
			assertTrue(func.compare("", ""));
			assertTrue(func.compare("", "ABCDEF"));
		}
	}

	/*
	 * Transpose doesn't take an argument so it should throw an exception
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		new OysterFunction.Builder<Compare>("Transpose(arg)").build();
	}

}
