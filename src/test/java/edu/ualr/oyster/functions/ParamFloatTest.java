package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ParamFloatTest {
	
	@Test
	public void testDefaultConstructor() {
		ParamFloat parm = new ParamFloat();
		assertEquals(0.0f,parm.getMinimum(),0.01);
		assertEquals(1.0f,parm.getMaximum(),0.01);
		assertFalse(parm.isValid());
	}

	@Test
	public void testConstructMinMax() {
		ParamFloat parm = new ParamFloat(0.2f,0.9f);
		assertTrue(parm.isUnderMinimum(0.1f));
		assertFalse(parm.isUnderMinimum(0.3f));
		assertEquals(0.2f,parm.getMinimum(),0.01);
		assertTrue(parm.isOverMaximum(0.95f));
		assertFalse(parm.isOverMaximum(0.8f));
		assertEquals(0.9f,parm.getMaximum(),0.01);
		assertFalse(parm.isValid());
	}

	@Test
	public void testConstructMinMaxparm() {
		ParamFloat parm = new ParamFloat(0.2f,0.9f,0.5f);
		assertEquals(0.2f,parm.getMinimum(),0.01);
		assertEquals(0.9f,parm.getMaximum(),0.01);
		assertTrue(parm.isValid());
		assertEquals(0.5,parm.getValue(),0.01);
	}

	@Test
	public void testSetGetGoodValue() {
		ParamFloat parm = new ParamFloat(0.0f,1.0f);
		parm.setValue("0.5");
		assertEquals(0.5,parm.getValue(),0.01);
	}

	@Test
	public void testSetGetBiggerValue() {
		ParamFloat parm = new ParamFloat(-5.0f,5.0f);
		parm.setValue("-2.5");
		assertEquals(-2.5,parm.getValue(),0.01);
		parm.setValue("2.5");
		assertEquals(2.5,parm.getValue(),0.01);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOutOfDefaultRange() {
		ParamFloat parm = new ParamFloat();
		parm.setValue("1.5");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOutOfSetRange() {
		ParamFloat parm = new ParamFloat(0.0f,3.0f);
		parm.setValue(3.1f);
		assertEquals(0.5,parm.getValue(),0.01);
	}

	@Test
	public void testOutOfSetRangeOutput() {
		ParamFloat parm = new ParamFloat(0.0f,3.0f);
		try {
			// Value is out of range
			parm.setValue(3.16745f);
		} catch (Exception ex) {
			// Print the message
			System.out.println(ex.getMessage());
		}
		assertFalse(parm.isValid());
	}

	@Test
	public void testNonNumericOutput() {
		ParamFloat parm = new ParamFloat(0.0f,3.0f);
		try {
			// Should throw an exception when parsing the string
			parm.setValue("non-numeric");
		} catch (Exception ex) {
			// Catch it and print it
			System.out.println(ex.getMessage());
		}
		assertFalse(parm.isValid());
	}
	@Test
	public void testIsValid() {
		ParamFloat parm = new ParamFloat(0.0f,3.0f);
		assertFalse(parm.isValid());
		parm.setValue(0.8f);
		assertTrue(parm.isValid());
	}

	@Test
	public void testIsNormalized() {
		ParamFloat parm = new ParamFloat(0.0f,2.0f,1.5f);
		assertTrue(parm.isNormalized(1.2f));
		assertFalse(parm.isNormalized(2.1f));
		assertFalse(parm.isNormalized(-1.0f));
	}

	@Test
	public void testCompare() {
		ParamFloat parm = new ParamFloat(0.0f,2.0f,1.5f);
		assertEquals(0,parm.compareValue(1.5f));
		assertEquals(1,parm.compareValue(1.51f));
		assertEquals(-1,parm.compareValue(1.49f));
	}

	@Test
	public void testTest() {
		ParamFloat parm = new ParamFloat(0.0f,2.0f,1.5f);
		assertFalse(parm.test(1.49f));
		assertTrue(parm.test(1.5f));
		assertTrue(parm.test(1.51f));
	}
}
