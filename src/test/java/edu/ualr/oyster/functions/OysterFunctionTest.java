package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test as a sampl of using OysterFunction and the defined interfaces
 * 
 * In the RuleTerm or IndexElement you would define a property such as
 * 
 * 	private Transform dataPrep and initialize it as illustrated below
 *  or
 *  private Comparator comparator
 *  
 *  At the time the program wants to do a transformation or a comparison it
 *  doesn't really care what class is doing it, only that the method exists.
 *
 */
public class OysterFunctionTest {
	private enum TestEnum { A, B, C; }
	
	@Test
	public void testIsArgValid() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		OysterFunction func = new OysterFunction.Builder<OysterFunction>("EXACT").build();
		assertTrue(func.isArgValid("ABC"));
		assertTrue(func.isArgValid("ABC","DEF"));
		assertTrue(func.isArgValid("ABC","DEF","GHI"));
		assertFalse(func.isArgValid((String)null));
		assertFalse(func.isArgValid(""));
		assertFalse(func.isArgValid("    "));
		assertFalse(func.isArgValid("","ABC"));
		assertFalse(func.isArgValid("ABC",null));
	}
	
	@Test
	public void testgetOptions() {
		assertEquals("[A, B, C]", OysterFunction.getOptions(TestEnum.class));
	}
	
	@Test
	public void testParseArgs() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		OysterFunction func = new OysterFunction.Builder<OysterFunction>("EXACT").build();
		String [] args = func.parseArgs("A  ,  B, 'C D E', ");
		assertEquals(3,args.length);
		assertEquals("A",args[0]);
		assertEquals("B",args[1]);
		assertEquals("C D E",args[2]);
	}

	@Test
	public void testTransform() {
		Transform func = null;
		try {
			func = new OysterFunction.Builder<Transform>("SCAN(LR,ALPHA,3,ToUpper,SameOrder)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals("SCAN",func.getName());
			assertEquals("LR,ALPHA,3,ToUpper,SameOrder",func.getParameters());
			assertFalse(func.isReversed());
			assertTrue(func.isImplemented(Transform.class));
			assertTrue(func.isImplemented("Transform"));
			assertFalse(func.isImplemented("score"));
			assertEquals("TES",func.transform("Test123"));
		}
	}

	@Test
	public void testCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SCAN(LR,ALPHA,2,ToLower,SameOrder)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Test123","Testing"));
		}
	}

	@Test
	public void testScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("LED(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		}
	}

	@Test(expected = java.lang.ClassCastException.class)
	public void testBadAction() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Transform func = new OysterFunction.Builder<Transform>("LED(0.72").build();
		assertEquals("LED",func.getName());
	}

	// NOTFOUND is defined as a function but it has an invalid class name
	@Test(expected = java.lang.ClassNotFoundException.class)
	public void tesClassNotFound() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		new OysterFunction.Builder<Compare>("NOTFOUND").build();
	}
	
	// NONEXISTANT is not defined as a function at all
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testFunctionNotDefined() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		new OysterFunction.Builder<Compare>("NONEXISTANT(PARMS)").build();
	}
	
	// NONEXISTANT is not defined as a function at all
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testFunctionParmsNotDefined() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		new OysterFunction.Builder<Compare>("NONEXISTANT(PARMS)").build();
	}

}
