package edu.ualr.oyster.functions.tokenizer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Tokenize;

public class MatrixTokenizerWeightedTest {

	private String inStr = "Edgar Jones, 21 Oak St, Little Rock,G34,20001104";

	@Test
	public void testConstructor() {
		MatrixTokenizerWeighted func = new MatrixTokenizerWeighted();
		assertEquals(FunctionType.MATRIXTOKENIZERWEIGHTED, func.getType());
		assertEquals(FunctionType.MATRIXTOKENIZERWEIGHTED.getFunctionName(), func.getName());
	}

	@Test
	public void testTokenize() {
		MatrixTokenizerWeighted func = new MatrixTokenizerWeighted();
		func.configure("0.20");
		assertEquals(7, func.tokenize(inStr).length);
	}

	@Test
	public void testBuildAndTokenize() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("!MatrixTokenizerWeighted(0.2)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(7, func.tokenize(inStr).length);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MatrixTokenizerWeighted()").build();
		if (func != null) {
		}
	}
}
