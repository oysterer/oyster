package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;

public class Metaphone3Test {
	
	@Test
	public void testConstructor() {
		new Metaphone3();
	}

	// To enable test replace @Ignore with @Test
	@Ignore
	public void testTransform() {
		Metaphone3 func = new Metaphone3();
		assertEquals("STFN",func.transform("Steffen"));
	}

	// To enable test replace @Ignore with @Test
	@Ignore
	public void testCompare() {
		Metaphone3 func = new Metaphone3();
		assertTrue(func.compare("Steffen","STFN"));
		assertFalse(func.compare("Steffen",""));
		assertFalse(func.compare(null,"STFN"));
		assertFalse(func.compare(null,""));
	}

	// To enable test replace @Ignore with @Test
	@Ignore
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("metaphone3").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Steffen","STFN"));
			assertFalse(func.compare("Steffen",""));
		}
	}

	
	// To enable test replace @Ignore with @Test(expected = java.lang.IllegalArgumentException.class)
	@Ignore
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("METAPHONE3(ABC)").build();
		if (func != null) {
		}
	}
}
