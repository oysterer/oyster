package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

public class KeyboardEditDistanceTest {

	@Test
	public void testOysterKeyboardEditDistance() {
		KeyboardEditDistance ked = new KeyboardEditDistance();
		assertNotNull(ked);
	}

	@Test
	public void testGetDistance() {
	}

	@Ignore
	public void testSetDistance() {
		fail("Not yet implemented");
	}

	@Ignore
	public void testClear() {
		fail("Not yet implemented");
	}

	@Ignore
	public void testComputeDistance() {
		fail("Not yet implemented");
	}

	@Test
	public void testComputeNormalizedScore() {
        String [][] strings = {
        		{"Saturday", "Sunday"},
                {"kitten","sitting"},
                {"Robert","Rovert"}
                };
		KeyboardEditDistance ked = new KeyboardEditDistance();
		for (int i = 0; i < strings.length; i++) {
			int result = ked.computeDistance(strings[i][0], strings[i][1]);
			System.out.println(strings[i][0] + "\t" + strings[i][1] + "\t" + result);
		}
	}
}
