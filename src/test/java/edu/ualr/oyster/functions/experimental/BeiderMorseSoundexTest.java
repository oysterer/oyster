package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

public class BeiderMorseSoundexTest {
	
	@Test
	public void testConstructor() {
		new BeiderMorseSoundex();
	}

	// To enable test replace @Ignore with @Test
	@Ignore
	public void testTransform() {
        BeiderMorseSoundex bms = new BeiderMorseSoundex();
        System.out.format("%1$12s %2$12s %3$12s%n", "Renault", "french", bms.identiyLanguage("Renault"));
        
        System.out.format("%1$12s %2$12s %3$12s%n", "Mickiewicz", "polish", bms.identiyLanguage("Mickiewicz"));
        // this also hits german and greeklatin
        System.out.format("%1$12s %2$12s %3$12s%n", "Thompson", "english", bms.identiyLanguage("Thompson"));
        // NuÃ±ez
        System.out.format("%1$12s %2$12s %3$12s%n", "Nu\u00f1ez", "spanish", bms.identiyLanguage("Nu\u00f1ez"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Carvalho", "portuguese", bms.identiyLanguage("Carvalho"));
        // ÄŒapek
        System.out.format("%1$12s %2$12s %3$12s%n", "\u010capek", "czech", bms.identiyLanguage("\u010capek"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Sjneijder", "dutch", bms.identiyLanguage("Sjneijder"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Klausewitz", "german", bms.identiyLanguage("Klausewitz"));
        // KÃ¼Ã§Ã¼k
        System.out.format("%1$12s %2$12s %3$12s%n", "K\u00fc\u00e7\u00fck", "turkish", bms.identiyLanguage("K\u00fc\u00e7\u00fck"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Giacometti", "italian", bms.identiyLanguage("Giacometti"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Nagy", "hungarian", bms.identiyLanguage("Nagy"));
        // CeauÅŸescu
        System.out.format("%1$12s %2$12s %3$12s%n", "Ceau\u015fescu", "romanian", bms.identiyLanguage("Ceau\u015fescu"));
        System.out.format("%1$12s %2$12s %3$12s%n", "Angelopoulos", "greeklatin", bms.identiyLanguage("Angelopoulos"));
        // Î‘Î³Î³ÎµÎ»ÏŒÏ€Î¿Ï…Î»Î¿Ï‚
        System.out.format("%1$12s %2$12s %3$12s%n", "\u0391\u03b3\u03b3\u03b5\u03bb\u03cc\u03c0\u03bf\u03c5\u03bb\u03bf\u03c2", "greek", bms.identiyLanguage("\u0391\u03b3\u03b3\u03b5\u03bb\u03cc\u03c0\u03bf\u03c5\u03bb\u03bf\u03c2"));
        // ÐŸÑƒÑˆÐºÐ¸Ð½
        System.out.format("%1$12s %2$12s %3$12s%n", "\u041f\u0443\u0448\u043a\u0438\u043d", "cyrillic", bms.identiyLanguage("\u041f\u0443\u0448\u043a\u0438\u043d"));
        // ×›×”×Ÿ
        System.out.format("%1$12s %2$12s %3$12s%n", "\u05db\u05d4\u05df", "hebrew", bms.identiyLanguage("\u05db\u05d4\u05df"));
        // Ã¡cz
        System.out.format("%1$12s %2$12s %3$12s%n", "\u00e1cz", "any", bms.identiyLanguage("\u00e1cz"));
        // Ã¡tz
        System.out.format("%1$12s %2$12s %3$12s%n", "\u00e1tz", "any", bms.identiyLanguage("\u00e1tz"));		fail("Not yet implemented");
	}

}
