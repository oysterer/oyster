package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * TODO Extend this test.  It does not provide adequate coverage
 * 
 */

public class NeedlemanWunschTest {

	@Test
	public void testNeedlemanWunsch() {
        NeedlemanWunsch sw = new NeedlemanWunsch();
        sw.configure("10,-1,-5");
        // Sequence 1 = A-CACACTA
        // Sequence 2 = AGCACAC-A
        float score = sw.score("AGCT", "AGCT");
        System.out.println("Score  : " + score);
        assertEquals(0.0f,score,0.0f);
	}
}
