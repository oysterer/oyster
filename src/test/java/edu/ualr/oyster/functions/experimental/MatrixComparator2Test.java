package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.experimental.MatrixComparator2;

public class MatrixComparator2Test {
	
	private static String stopWordList = "`NC|SALEM|WINSTON|DR|RD|ALLEN|ADAMS|KERNERSVILLE|27284|27106|ST|CT|27105|27103|LN|27104|27127|27107|AVE|NA|ALEXANDER|CLEMMONS|27012|CA|27101|CIR|PFAFFTOWN|27040|ANDERSON|S|E|N|LEWISVILLE|A|27023|W|FL|RIDGE|HALL|L|RURAL|M|D|PARK|WALKERTOWN|CREEK|27045|C|TX|ALLISON|27051|ALSTON|DAVID|R|MICHAEL|ELIZABETH|B|J|AMOS|JAMES|LEE|ALLRED|JOHN|TRL|FOREST|PLACE|ADKINS|WILLIAM|ALFORD|WAY|ANN|31|OLD|THOMAS|27109|WFU|APT|68|48|17|45|H|75|ABBOTT|VILLAGE|26|PL|ALBRIGHT|82|TOBACCOVILLE|ROBERT|78|MARIE|81|66|86|LYNN|97|DENISE|CHRISTOPHER|G|70|30|38|27050|ALVAREZ|83|10|14|HILL|20|58|12|41|24|CHARLES|57|BLVD|94|43|69|52|22|23|98|73|T|79|MARY|ANTHONY|60|63|ALDRIDGE|ALDERMAN|47|32|39|19|55|88|AR|AARON|62|65|GROVE|50|F|21|15|16|59|85|AGUILAR|44|33|29|93|ALLEY|ACEVEDO|GREEN|99|49|76|80|27110|77`";
	private static String s = "JOHN  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,875-75-7727,41998,";
	private static String t = "WILLIAM  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,866-22-8052,18264,";
	
	@Test
	public void testConstructor() {
		MatrixComparator2 func = new MatrixComparator2();
		assertEquals(FunctionType.MATRIXCOMPARATOR2, func.getType());
		assertEquals(FunctionType.MATRIXCOMPARATOR2.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		MatrixComparator2 func = new MatrixComparator2();
		func.configure("0.71,"+stopWordList+")");
	    assertEquals(0.64,func.score(s, t),0.1);
	}

	@Test
	public void testCompare() {
		MatrixComparator2 func = new MatrixComparator2();
		func.configure("0.71,"+stopWordList+")");
	    assertFalse(func.compare(s, t));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("!MATRIXCOMPARATOR2(0.71,"+stopWordList+")").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
		    assertTrue(func.compare(s, t));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MATRIXCOMPARATOR2()").build();
		if (func != null) {
		}
	}
}
