package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ParamWordlistTest {

	@Test
	public void testWordlist() {
		ParamWordlist list = new ParamWordlist();
		assertEquals('|', list.getDelimiter());
		assertFalse(list.hasWords());
		list.setDelimiter('.');
		assertEquals('.', list.getDelimiter());
		list.setWords("ONE.two.Three.fouR");
		assertEquals(4,list.getStopWords().size());
	}

	@Test
	public void testWordlistString() {
		ParamWordlist list = new ParamWordlist("ONE|two|Three|fouR");
		assertEquals('|', list.getDelimiter());
		assertTrue(list.hasWords());
		assertTrue(list.isWord("THREE"));
		assertTrue(list.isWord("one"));
		assertFalse(list.isWord("five"));
	}

	@Test
	public void testWordlistCharString() {
		ParamWordlist list = new ParamWordlist('.',"ONE.two.Three.fouR");
		assertEquals('.', list.getDelimiter());
		assertTrue(list.hasWords());
		assertTrue(list.isWord("THREE"));
		assertTrue(list.isWord("one"));
		assertFalse(list.isWord("five"));
	}
}
