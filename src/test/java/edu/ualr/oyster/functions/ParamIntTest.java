package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ParamIntTest {
	
	@Test
	public void testDefaultConstructor() {
		ParamInt parm = new ParamInt();
		assertEquals(Integer.MIN_VALUE,parm.getMinimum());
		assertNotNull(parm.getMaximum());
		assertEquals(Integer.MAX_VALUE,parm.getMaximum());
		assertFalse(parm.isValid());
	}

	@Test
	public void testConstructMinMax() {
		ParamInt parm = new ParamInt(-10,10);
		assertEquals((-10),parm.getMinimum());
		assertTrue(parm.isUnderMinimum(-15));
		assertFalse(parm.isUnderMinimum(-5));
		assertEquals(10,parm.getMaximum());
		assertTrue(parm.isOverMaximum(15));
		assertFalse(parm.isOverMaximum(5));
		assertFalse(parm.isValid());
	}

	@Test
	public void testConstructMinMaxparm() {
		ParamInt parm = new ParamInt(-10,10,7);
		assertEquals((-10),parm.getMinimum());
		assertEquals(10,parm.getMaximum());
		assertTrue(parm.isValid());
		assertEquals(7,parm.getValue());
	}

	@Test
	public void testSetGetGoodValue() {
		ParamInt parm = new ParamInt(-10,10);
		parm.setValue("7");
		assertEquals(7,parm.getValue());
	}

	@Test
	public void testSetValue() {
		ParamInt parm = new ParamInt(-10,10);
		parm.setValue("-7");
		assertEquals((-7),parm.getValue());
		parm.setValue("7");
		assertEquals(7,parm.getValue());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetOutOfRange() {
		ParamInt parm = new ParamInt(-10,10);
		parm.setValue("15");
	}

	@Test
	public void testOutOfSetRangeOutput() {
		ParamInt parm = new ParamInt(-10,10);
		try {
			// Value is out of range
			parm.setValue(200);
		} catch (Exception ex) {
			// Print the message
			System.out.println(ex.getMessage());
		}
		assertFalse(parm.isValid());
	}

	@Test
	public void testNonNumericOutput() {
		ParamInt parm = new ParamInt();
		try {
			// Should throw an exception when parsing the string
			parm.setValue("non-numeric");
		} catch (Exception ex) {
			// Catch it and print it
			System.out.println(ex.getMessage());
		}
		assertFalse(parm.isValid());
	}
	
	@Test
	public void testIsValid() {
		ParamInt parm = new ParamInt();
		assertFalse(parm.isValid());
		parm.setValue(8);
		assertTrue(parm.isValid());
	}

	@Test
	public void testIsNormalized() {
		ParamInt parm = new ParamInt(-10,10,7);
		assertTrue(parm.isNormalized(1));
		assertFalse(parm.isNormalized(12));
		assertFalse(parm.isNormalized(-12));
	}

	@Test
	public void testCompare() {
		ParamInt parm = new ParamInt(-10,10,7);
		assertEquals(0,parm.compareValue(7));
		assertEquals(1,parm.compareValue(8));
		assertEquals(-1,parm.compareValue(6));
	}

	@Test
	public void testTest() {
		ParamInt parm = new ParamInt(-10,10);
		parm.setValue("7");
		assertFalse(parm.test(6));
		assertTrue(parm.test(7));
		assertTrue(parm.test(8));
	}

}
