package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.DataPairs;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class MaxQGramTest {
	
	@Test
	public void testConstructor() {
		MaxQGram func = new MaxQGram();
		assertEquals(FunctionType.MAXQGRAM, func.getType());
		assertEquals(FunctionType.MAXQGRAM.getFunctionName(),func.getName());
	}

	// Just get scores and print them to see how this works
	@Ignore
	public void determineScores() {
		MaxQGram func = new MaxQGram();
		for (int i = 0; i < DataPairs.entries.length; i++) {
			String str1 = DataPairs.entries[i][0];
			String str2 = DataPairs.entries[i][1];
			float score = func.score(str1,str2);
			System.out.println(str1 + " : " + str2 + " = " + score);
		}
	}
	
	@Test
	public void testCompare() {
		MaxQGram func = new MaxQGram();
		func.setThreshold(0.90f);
		assertTrue(func.compare("PENNINGTON","PENINGTON"));
		assertFalse(func.compare("JOHNSON","JAMISON"));
	}
	
	@Test
	public void testScore() {
		MaxQGram func = new MaxQGram();
		assertEquals(0.95,func.score("PENNINGTON","PENINGTON"),0.01);
		assertEquals(0.57,func.score("JOHNSON","JAMISON"),0.01);
		assertEquals(0.0,func.score("SARAH","Molly"),0.01);
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("MAXQGRAM(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			//assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MAXQGRAM").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
