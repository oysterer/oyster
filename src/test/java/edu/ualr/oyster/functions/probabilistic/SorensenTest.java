package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class SorensenTest {
	
	@Test
	public void testConstructor() {
		Sorensen func = new Sorensen();
		assertEquals(FunctionType.SORENSEN, func.getType());
		assertEquals(FunctionType.SORENSEN.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		Sorensen func = new Sorensen();
		assertEquals(0.33,func.score("CRUSAN","CHRZAN"),0.1);
		assertEquals(0.36,func.score("PENNINGTON","PENINGTON"),0.1);
	}

	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("Sorensen").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(0.45,func.score("SMITH","SMITHE"),0.1);
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SORENSEN").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
