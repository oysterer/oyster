package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class SmithWatermanTest {
	
	@Test
	public void testConstructor() {
		SmithWaterman func = new SmithWaterman();
		assertEquals(FunctionType.SMITHWATERMAN, func.getType());
		assertEquals(FunctionType.SMITHWATERMAN.getFunctionName(),func.getName());
	}
	@Test
	public void testScore1() {
        SmithWaterman func = new SmithWaterman();
        func.configure("2, -1, -1");
        // Sequence 1 = A-CACACTA
        // Sequence 2 = AGCACAC-A
        String s = "ACACACTA";
        String t = "AGCACACA";
        assertEquals(0.87,func.score(s, t),0.01);
	}
	
	@Test
	public void testScore2() {
        SmithWaterman func = new SmithWaterman();
        func.configure("1, 0, 0");
        String s = "CORDACORPRETADIAMETRO95MMDUREZA75SHOREFORMATOREDONDOMATERIALBORRACHANITRILICABUNAN";
        String t = "CORDACORPRETADIAMETRO65MMDUREZA75SHOREAFORMATOREDONDOMATERIALBORRACHANITRILICABUNAN";
        assertEquals(0.99,func.score(s, t),0.01);
	}
	
	@Test
	public void testScore3() {
        SmithWaterman func = new SmithWaterman();
        func.configure("1, -1, -1");
        String s = "cgggtatccaa";
        String t = "ccctaggtccca";
        assertEquals(0.92,func.score(s, t),0.01);
	}
	
	@Test
	public void testScore4() {
        SmithWaterman func = new SmithWaterman();
        func.configure("1, -3, -1");
        String s = "ACTGAACTG";
        String t = "ATGGACCTG";
        assertEquals(0.88,func.score(s, t),0.01);
	}
	
	@Test
	public void testScore5() {
        SmithWaterman func = new SmithWaterman();
        func.configure("10, -3, -3");
        String s = "CTGG";
        String t = "CATTG";
        assertEquals(0.8,func.score(s, t),0.01);
	}
	
	@Test
	public void testScore6() {
        SmithWaterman func = new SmithWaterman();
        func.configure("5, -1, -3");
        String s = "2801 South University, Little Rock";
        String t = "2801 S. Univers., Little Rock";
        assertEquals(0.97,func.score(s, t),0.01);
	}
	
	@Test
	public void testCompare() {
        SmithWaterman func = new SmithWaterman();
        func.configure("5, -1, -3, 0.9");
        String s = "2801 South University, Little Rock";
        String t = "2801 S. Univers., Little Rock";
        assertTrue(func.compare(s, t));
        assertFalse(func.compare("CTGG", "CATTG"));
	}

	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("SMITHWATERMAN(10, -3, -3)").build();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
	        assertEquals(0.8,func.score("CTGG", "CATTG"),0.01);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Score func = new OysterFunction.Builder<Score>("SMITHWATERMAN").build();
		if (func != null) {
		}
	}
}
