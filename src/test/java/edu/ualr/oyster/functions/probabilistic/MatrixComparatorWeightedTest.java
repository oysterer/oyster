package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class MatrixComparatorWeightedTest {
	private static String s = "JOHN  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,875-75-7727,41998,";
	private static String t = "WILLIAM  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,866-22-8052,18264,";
	
	@Test
	public void testConstructor() {
		MatrixComparatorWeighted func = new MatrixComparatorWeighted();
		assertEquals(FunctionType.MATRIXCOMPARATORWEIGHTED, func.getType());
		assertEquals(FunctionType.MATRIXCOMPARATORWEIGHTED.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		MatrixComparatorWeighted func = new MatrixComparatorWeighted();
		// Configure or it will throw an error
		func.configure("0.71");
		float score = func.score(s, t);
	    assertEquals(0.615,score,0.01);
	}

	@Test
	public void testCompare() {
		MatrixComparatorWeighted func = new MatrixComparatorWeighted();
		// Configure or it will throw an error
		func.configure("0.71, data/tokenWeightTable.dat");
	    assertFalse(func.compare(s, t));
	}

	/*
	 * Note that negated result does not alter the score
	 */
	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("~MATRIXCOMPARATORWEIGHTED(0.80)").build();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			float score = func.score(s, t);
		    assertEquals(0.615,score,0.01);
		}
	}

	/*
	 * Score for the test is 0.64 and threshold it 0.80 with negated logic so result should be true
	 */
	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("!MATRIXCOMPARATORWEIGHTED(0.80)").build();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
		    assertTrue(func.compare(s, t));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MATRIXCOMPARATORWEIGHTED()").build();
		if (func != null) {
		}
	}
}
