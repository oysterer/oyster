package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class LevenshteinEditDistanceTest {
	
	@Test
	public void testConstructor() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		assertEquals(FunctionType.LED, func.getType());
		assertEquals(FunctionType.LED.getFunctionName(),func.getName());
	}
	
	@Test
	public void testCompare() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		func.setThreshold(0.8f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertFalse(func.compare("TRUE","Truely"));
	}
	
	@Test
	public void testScore() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		assertEquals(0.15,func.score("TRUE","Truely"),0.1);
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("LED(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("LED").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
