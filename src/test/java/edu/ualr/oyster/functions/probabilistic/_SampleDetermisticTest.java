package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

/*
 * This is a sample unit test for a probabilistic function
 * 
 * Copy this file and create a unit test for your function
 * follow the naming pattern <Function>Test
 * 
 * Edit that file and change LED to your function name with 
 * proper casing for the Function enumeration name (CAPS)
 * and the class name (CamelCase)
 * 
 * Then change the data and the expected result in each test
 * to match valid values for your function
 * 
 * @author James True
 */
public class _SampleDetermisticTest {

	/*
	 * Test the constructor and that the function name can be resolved
	 */
	@Test
	public void testConstructor() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		assertEquals(FunctionType.LED, func.getType());
		assertEquals(FunctionType.LED.getFunctionName(),func.getName());
	}

	/*
	 * Test the compare method for both true and false results
	 */	@Test
	public void testCompare() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		func.setThreshold(0.8f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertFalse(func.compare("TRUE","Truely"));
	}
	
	 /*
	  * Test the score method for expected scores
	  */
	@Test
	public void testScore() {
		LevenshteinEditDistance func = new LevenshteinEditDistance();
		assertEquals(0.00,func.score("",null),0.1);
		assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		assertEquals(0.15,func.score("TRUE","Truely"),0.1);
	}

	/*
	 * This test uses the OysterFunction Builder to construct the class
	 * This will validate that the enumeration is correct and that all
	 * the resources are accessible.
	 */	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("LED(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}


	/*
	 * This test expects an IllegalArgumentException because the function
	 * expects a parameter its signature so an error is the expected result
	 */	
	 @Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("LED").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
