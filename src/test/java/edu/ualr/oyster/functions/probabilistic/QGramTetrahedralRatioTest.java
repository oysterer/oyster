package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class QGramTetrahedralRatioTest {
	
	@Test
	public void testConstructor() {
		QGramTetrahedralRatio func = new QGramTetrahedralRatio();
		assertEquals(FunctionType.QTR, func.getType());
		assertEquals(FunctionType.QTR.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		QGramTetrahedralRatio func = new QGramTetrahedralRatio();
		assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		assertEquals(0.61,func.score("TRUE","Truely"),0.1);
	}

	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("QTR").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Score func = new OysterFunction.Builder<Score>("QTE(0.5").build();
		if (func != null) {
		}
	}
}
