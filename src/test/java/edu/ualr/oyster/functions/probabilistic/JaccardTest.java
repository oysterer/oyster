package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class JaccardTest {
	
	@Test
	public void testConstructor() {
		Jaccard func = new Jaccard();
		assertEquals(FunctionType.JACCARD, func.getType());
		assertEquals(FunctionType.JACCARD.getFunctionName(),func.getName());
	}

	@Test
	public void testScore() {
		Jaccard func = new Jaccard();
		assertEquals(0.80,func.score("ISLE", "ISELEY"),0.01);
		assertEquals(0.71,func.score("PENNING", "PENINGTON"),0.01);
		assertEquals(0.83,func.score("SMITH","SMITHE"),0.01);
		assertEquals(0.71,func.score("PENNING","PENINGTON"),0.01);
	}
	
	@Test
	public void testCompare() {
		Jaccard func = new Jaccard();
		func.setThreshold(0.8f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertFalse(func.compare("PENNING","PENINGTON"));
	}

	@Test
	public void testBuildAndScore() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("JACCARD(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
			assertFalse(func.compare("PENNING","PENINGTON"));
		}
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("JACCARD(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareUninitialized() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("JACCARD").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("JACCARD(2.6").build();
		if (func != null) {
		}
	}
	
	/*
	 * Jaccard(); s = "CRUSAN"; t = "CHRZAN";
	 * System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
	 * 
	 * s = "ISLE"; t = "ISELEY"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "PENNING"; t = "PENINGTON"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "PENNINGTON"; t = "PENINGTON"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "STROHMAN"; t = "STROHM"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "EDUARDO"; t = "EDWARD"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "JOHNSON"; t = "JAMISON"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "RAPHAEL"; t = "RAFAEL"; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t);
	 * 
	 * s = "Eric"; t = null; System.out.format("%1$,6.5f %2$s %3$s%n",
	 * j.computeDistance(s, t), s, t); }
	 */


}
