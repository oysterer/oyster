package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

/*
 * @author James True
 */
public class DamerauLevenshteinTest {

	/*
	 * Test the constructor and that the function name can be resolved
	 */
	@Test
	public void testConstructor() {
		DamerauLevenshtein func = new DamerauLevenshtein();
		assertEquals(FunctionType.DLED, func.getType());
		assertEquals(FunctionType.DLED.getFunctionName(),func.getName());
	}

	/*
	 * Test the compare method for both true and false results
	 */	@Test
	public void testCompare() {
		DamerauLevenshtein func = new DamerauLevenshtein();
		func.setThreshold(0.8f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertFalse(func.compare("TRUE","Truely"));
	}
	
	 /*
	  * Test the score method for expected scores
	  */
	@Test
	public void testScore() {
		DamerauLevenshtein func = new DamerauLevenshtein();
		assertEquals(0.00,func.score("",null),0.1);
		assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		assertEquals(0.15,func.score("TRUE","Truely"),0.1);
		assertEquals(0.75,func.score("MARY","MAYR"),0.1);
	}

	/*
	 * This test uses the OysterFunction Builder to construct the class
	 * This will validate that the enumeration is correct and that all
	 * the resources are accessible.
	 */	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("DLED(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}


	/*
	 * This test expects an IllegalArgumentException because the function
	 * expects a parameter its signature so an error is the expected result
	 */	
	 @Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("DLED").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
