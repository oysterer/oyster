package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class CosineTest {

	private String qbf = "The quick brown fox jumpted over the lazy yellow dog";
	
	@Test
	public void testConstructor() {
		Cosine func = new Cosine();
		assertEquals(FunctionType.COSINE, func.getType());
		assertEquals(FunctionType.COSINE.getFunctionName(),func.getName());
	}

	@Test
	public void testCompare() {
		Cosine func = new Cosine();
		func.setThreshold(0.60f);
		assertTrue(func.compare("JOE SMITH JR","JOESEPH SMITH SR"));
		assertTrue(func.compare("Allice Blue Gown","allice Blue gown"));
	}
	
	@Test
	public void testScore() {
		Cosine func = new Cosine();
		assertEquals(0.66,func.score("JOE SMITH JR","JOESEPH SMITH SR"),0.1);
		assertEquals(0.66,func.score("Allice Blue Gown","allice Blue gown"),0.1);
		assertEquals(1.0,func.score(qbf.toLowerCase(),qbf.toUpperCase()),0.1);
		assertEquals(0.0,func.score("","    "),0.1);
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("COSINE(0.83)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("COSINE").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
