package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class TverskyIndexTest {
	
	@Test
	public void testConstructor() {
		TverskyIndex func = new TverskyIndex();
		func.configure("1.0,1.0");
		assertEquals(FunctionType.TVERSKY, func.getType());
		assertEquals(FunctionType.TVERSKY.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		TverskyIndex func = new TverskyIndex();
		func.configure("1.0,1.0");
		assertEquals(0.5,func.score("CRUSAN","CHRZAN"),0.1);
		assertEquals(1.0,func.score("PENNINGTON","PENINGTON"),0.1);
	}

	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("TVERSKY(0.5,0.6").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(0.90,func.score("SMITH","SMITHE"),0.1);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Score func = new OysterFunction.Builder<Score>("TVERSKY").build();
		if (func != null) {
		}
	}

}
