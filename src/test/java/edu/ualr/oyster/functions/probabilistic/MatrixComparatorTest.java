package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class MatrixComparatorTest {
	
	private static String s = "JOHN  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,875-75-7727,41998,";
	private static String t = "WILLIAM  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,866-22-8052,18264,";
	
	@Test
	public void testConstructor() {
		MatrixComparator func = new MatrixComparator();
		assertEquals(FunctionType.MATRIXCOMPARATOR, func.getType());
		assertEquals(FunctionType.MATRIXCOMPARATOR.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		MatrixComparator func = new MatrixComparator();
		func.configure("0.71, 'NC|SALEM|WINSTON|DR|RD|ALLEN|ADAMS|LEWISVILLE|41998|18264'");
	    assertEquals(0.64,func.score(s, t),0.1);
	}

	@Test
	public void testCompare() {
		MatrixComparator func = new MatrixComparator();
		func.configure("0.71, 'NC|SALEM|WINSTON|DR|RD|ALLEN|ADAMS|LEWISVILLE|41998|18264'");
	    assertFalse(func.compare(s, t));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("!MATRIXCOMPARATOR(0.62, 'NC|SALEM|WINSTON|DR|RD|ALLEN|ADAMS|LEWISVILLE|41998|18264')").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
		    assertTrue(func.compare(s, t));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MATRIXCOMPARATOR()").build();
		if (func != null) {
		}
	}
}
