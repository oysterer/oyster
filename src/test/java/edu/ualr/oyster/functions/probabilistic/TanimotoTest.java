package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Score;

public class TanimotoTest {
	
	@Test
	public void testConstructor() {
		Tanimoto func = new Tanimoto();
		assertEquals(FunctionType.TANIMOTO, func.getType());
		assertEquals(FunctionType.TANIMOTO.getFunctionName(),func.getName());
	}
	
	@Test
	public void testScore() {
		Tanimoto func = new Tanimoto();
		assertEquals(0.5,func.score("CRUSAN","CHRZAN"),0.1);
		assertEquals(1.0,func.score("PENNINGTON","PENINGTON"),0.1);
	}

	@Test
	public void testBuildAndScore() {
		Score func = null;
		try {
			func = new OysterFunction.Builder<Score>("Tanimoto(0.5)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(0.84,func.score("SMITH","SMITHE"),0.1);
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Tanimoto").build();
		if (func != null) {
			assertTrue(func.compare("PENNINGTON","PENINGTON"));
		}
	}

}
