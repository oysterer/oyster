package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.DataPairs;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class JaroWinklerTest {
	
	@Test
	public void testConstructor() {
		JaroWinkler func = new JaroWinkler();
		assertEquals(FunctionType.JAROWINKLER, func.getType());
		assertEquals(FunctionType.JAROWINKLER.getFunctionName(),func.getName());
	}

	// Just get scores and print them to see how this works
	@Ignore
	public void determineScores() {
		JaroWinkler func = new JaroWinkler();
		for (int i = 0; i < DataPairs.entries.length; i++) {
			String str1 = DataPairs.entries[i][0];
			String str2 = DataPairs.entries[i][1];
			float score = func.score(str1,str2);
			System.out.println(str1 + " : " + str2 + " = " + score);
		}
	}
	
	@Test
	public void testCompare() {
		JaroWinkler func = new JaroWinkler();
		func.setThreshold(0.8f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertFalse(func.compare("TRUE","Truely"));
	}
	
	@Test
	public void testScore() {
		JaroWinkler func = new JaroWinkler();
		assertEquals(0.97,func.score("SMITH","SMITHE"),0.1);
		assertEquals(0.47,func.score("TRUE","Truely"),0.1);
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("JAROWINKLER(0.8)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("JAROWINKLER").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
