package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class ListOverlapTest {
	
	@Test
	public void testConstructor() {
		ListOverlap func = new ListOverlap();
		assertEquals(FunctionType.LISTOVERLAP, func.getType());
		assertEquals(FunctionType.LISTOVERLAP.getFunctionName(),func.getName());
	}
	
	@Test
	public void testCompare() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.60f);
		func.setListDelimiter("|");
		assertTrue(func.compare("A|B|C|D|E","A|B|C"));
	}
	
	@Test
	public void testScore() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.60f);
		func.setListDelimiter("|");
		assertEquals(0.60,func.score("A|B|C|D|E","A|B|C"),0.1);
	}

	@Test
	public void test1() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.50f);
		func.setListDelimiter(",");
		assertTrue(func.compare("a,b,c,d,e,f", "a,,b,c,d,g,h"));
	}
	
	@Test
	public void test2() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.50f);
		func.setListDelimiter(",");
		assertFalse(func.compare("a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	@Test
	public void test3() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.30f);
		func.setListDelimiter(";");
		assertTrue(func.compare("a;b;c;d;e;f", "a;;b;;;g;h"));
	}
	
	@Test
	public void test4() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.50f);
		func.setListDelimiter("|");
		assertTrue(func.compare("a|b|c|d|e|f", "a||b|c|d|g|h"));
	}
	
	@Test
	public void test5() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.50f);
		func.setListDelimiter(":");
		assertTrue(func.compare("a:b:c:d:e:f", "a::b:c:d:g:h"));
	}

	@Test
	public void test6() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.40f);
		func.setListDelimiter(",");
		assertTrue(func.compare("a,b,c,d,e,f", "a,,b,c,d,g,h"));
	}
	
	@Test
	public void test7() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.80f);
		func.setListDelimiter(",");
		assertFalse(func.compare("a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	@Test
	public void test8() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.80f);
		func.setListDelimiter(";");
		assertFalse(func.compare("a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	
	@Test
	public void test9() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.55f);
		func.setListDelimiter("|");
		assertTrue(func.compare("a|b|c|d|e|f", "a||b|c|d|g|h"));
	}
	
	@Test
	public void test10() {
		ListOverlap func = new ListOverlap();
		func.setThreshold(0.50f);
		func.setListDelimiter(":");
		assertTrue(func.compare("a:b:c:d:e:f", "a::b:c:d:g:h"));
	}
	
	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("LISTOVERLAP(0.6,|)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("A|B|C|D|E","A|B|C"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("LISTOVERLAP").build();
		if (func != null) {
			assertTrue(func.compare("A|B|C|D|E","A|B|C"));
		}
	}

}
