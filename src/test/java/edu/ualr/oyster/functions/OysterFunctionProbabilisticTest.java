package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class OysterFunctionProbabilisticTest {
	
	/**
	 * Note that because this class is not defined as a FnctionType
	 * The FUnction name in messages will be "null"
	 * 
	 * @author James True
	 *
	 */
	private class TestClass extends OysterFunctionProbabilistic {
		
		public TestClass() {
			super();
		}
		public TestClass(float min, float max) {
			super(min,max);
		}
		@Override
		public float score(String x, String y) {
			return 0.5f;
		}
	}

	/*
	 * Threshold has not been set so default compare should throw an exception
	 */
	@Test(expected = IllegalStateException.class)
	public void testThresholdNotSet() {
		TestClass func = new TestClass();
		func.compare("ABC","XYZ");
	}

	/*
	 * Score of 0.5 is not within the min-max range (not normalized)
	 */
	@Test(expected = IllegalStateException.class)
	public void testScoreNotNormalized() {
		TestClass func = new TestClass(0.0f,0.45f);
		func.setThreshold("0.3");
		func.compare("ABC","XYZ");
	}

	/*
	 * Min = 0.0, max = 2.0
	 * setting threshold to 2.5 is out of ranges
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetThresholdOutOfRange() {
		TestClass func = new TestClass(0.0f,2.0f);
		func.setThreshold("2.5");
	}

	/*
	 * This really only tests our own override method
	 */
	@Test
	public void testScore() {
		TestClass func = new TestClass();
		assertEquals(0.5f,func.score("ABC","XYZ"),0.01);
	}

	/*
	 * compare is not overridden in the TestClass
	 * so this exercises the default method in the parent
	 */
	@Test
	public void testCompare() {
		TestClass func = new TestClass();
		func.setThreshold("0.8f");
		assertFalse(func.compare("ABC","XYZ"));
		func.setThreshold("0.4f");
		assertTrue(func.compare("ABC","XYZ"));
	}

}
