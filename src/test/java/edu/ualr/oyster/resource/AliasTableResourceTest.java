package edu.ualr.oyster.resource;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

import edu.ualr.oyster.resource.AliasTableResource;

public class AliasTableResourceTest {

	@Test
	public void test() {
		AliasTableResource reference1 = AliasTableResource.getInstance("data/alias.dat");
		assertNotNull(reference1);
		Map<String, ArrayList<String>> table1 = reference1.getAliasTable();
		System.out.println("Reference 1:" + reference1 + " size:"+table1.size());
		
		AliasTableResource reference2 = AliasTableResource.getInstance("data/alias.dat");
		assertNotNull(reference2);
		Map<String, ArrayList<String>> table2 = reference2.getAliasTable();
		System.out.println("Reference 2:" + reference2 + " size:"+table2.size());
		
		AliasTableResource reference3 = AliasTableResource.getInstance("data/alias-redux.dat");
		assertNotNull(reference3);
		Map<String, ArrayList<String>> table3 = reference3.getAliasTable();
		System.out.println("Reference 3:" + reference3 + " size:"+table3.size());

		assertThat(reference1, is(equalTo(reference2)));
		assertThat(reference1, not(equalTo(reference3)));
		assertThat(reference2, not(equalTo(reference3)));
	}

}
