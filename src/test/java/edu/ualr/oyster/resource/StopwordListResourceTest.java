package edu.ualr.oyster.resource;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Set;

import org.junit.Test;

import edu.ualr.oyster.resource.StopwordListResource;

public class StopwordListResourceTest {

	@Test
	public void test() {
		StopwordListResource reference1 = StopwordListResource.getInstance("data/stop-words.dat");
		assertNotNull(reference1);
		Set<String> table1 = reference1.getStopwordList();
		System.out.println("Reference 1:" + reference1 + " size:"+table1.size());
		
		StopwordListResource reference2 = StopwordListResource.getInstance("data/stop-words.dat");
		assertNotNull(reference2);
		Set<String> table2 = reference2.getStopwordList();
		System.out.println("Reference 2:" + reference2 + " size:"+table2.size());
		
		StopwordListResource reference3 = StopwordListResource.getInstance("data/alias.dat");
		assertNotNull(reference3);
		Set<String> table3 = reference3.getStopwordList();
		System.out.println("Reference 3:" + reference3 + " size:"+table3.size());

		assertThat(reference1, is(equalTo(reference2)));
		assertThat(reference1, not(equalTo(reference3)));
		assertThat(reference2, not(equalTo(reference3)));
	}

}
