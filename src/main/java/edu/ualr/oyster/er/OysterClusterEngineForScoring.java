/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.er;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

//import edu.ualr.oyster.association.matching.OysterComparator;
import edu.ualr.oyster.core.ScoringRuleTerm;
import edu.ualr.oyster.data.ClusterRecord;
import edu.ualr.oyster.data.OysterIdentityRecord;
import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.resource.WeightPair;
import edu.ualr.oyster.resource.WeightTableResource;
import edu.ualr.oyster.functions.Compare;

/**
 * Implements resolution rules in reference source to determine if reference is
 * new entity or same as an existing entity. This Engine uses the Fellegi-Sunter
 * Clustering methodology.
 * 
 * Responsibilities:
 * <ul>
 * <li>Apply scoring rules from reference source</li>
 * <li>Resolve reference</li>
 * </ul>
 * 
 * @author Cheng Chen
 */
public class OysterClusterEngineForScoring extends OysterResolutionEngine {
	// Class variables add the John R. Talburt for Scoring Rule Experiments
	//private char parm1, parm2, parm3, parm4, parm5;
	/**
	 * Creates a new instance of OysterClusterEngine
	 * 
	 * @param logFile
	 * @param logLevel
	 */
	public OysterClusterEngineForScoring(String logFile, Level logLevel, int recordType) {
		super(logFile, logLevel, recordType);
		mergedList = false;
	}
	
	/**
	 * Creates a new instance of OysterClusterEngine
	 * 
	 * @param log
	 */
	public OysterClusterEngineForScoring(Logger log, int recordType) {
		super(log, recordType);
		mergedList = false;
		postConsolidation = false;
	}
	
	/**
	 * Creates a new instance of OysterClusterEngine
	 */
	public OysterClusterEngineForScoring(int recordType) {
		super(recordType);
		mergedList = false;
		postConsolidation = false;
	}
	
	
	/**
	 * This is the heart of the default <code>OysterClusterEngine</code>. The
	 * engine takes the current reference items pulls a candidate list of
	 * possibles from the <code>OysterIdentityRepository</code>. The reference
	 * items are compared against the candidate list one at a time to
	 * determine if a match is produced.
	 * 
	 * Two mechanism are used to control the performance and quality of the
	 * match. First, an index is used to produce candidates. These candidates
	 * can be returned in a random or descending frequency sorted order. In
	 * the sorted order, the records at the top of the list have the greatest
	 * chance of matching because it contains more elements that matched to
	 * the index. The second mechanism, is the filter. The filter is a list of
	 * minimum number elements that must exist for a rule to pass. This is
	 * used to boost performance it removes rules from the candidate list that
	 * would have never passed before any complex calculations are performed.
	 * 
	 * Currently, ALL candidates that pass the filter are evaluated UNLESS
	 * they have been consolidated in a previous iteration of during this
	 * specific call.
	 * 
	 * @param sort
	 *              true if a sorted candidate list is to be used otherwise
	 *              false.
	 * @param recordCount
	 *              the current record count
	 */
	@Override
	public void integrateSource(boolean sort, long recordCount) {
		PrintWriter bw = repository.getClericalReviewWriter();
		postConsolidation = false;
		
		/*
		 * Here are the parameters for logic
		 * logic.charAt(0) must be a $ (dollar sign)
		 */
		
		boolean matched = false;
		int matchedCount = 0;
		ClusterRecord cr = null;
		long start = System.currentTimeMillis(), stop;
		
		if (logger.isLoggable(Level.INFO)) {
			StringBuilder sb = new StringBuilder(100);
			sb.append(System.lineSeparator()).append("Input: ").append(clusterRecord.getMergedRecord());
			logger.info(sb.toString());
		}
		/*
		 * if
		 * (clusterRecord.getMergedRecord().get("@RefID").equals("Test17e.2020"
		 * )){ System.out.println(); }
		 */
		// Ask IdentityRepository for a list of candidates
		Map<String, ClusterRecord> list;
		if ((list = repository.getCandidateList(clusterRecord, sort, primaryFilter, lcrdMinSize, isByPassFilter(), mergedList)).isEmpty()) {
			if (secondaryFilter != null) {
				list = repository.getCandidateList(clusterRecord, sort, secondaryFilter, lcrdMinSize, isByPassFilter(), mergedList);
			}
		}
		
		int unfiltered = list.size();
		totalCandidatesSize += list.size();
		
		// Remove duplicate Clusters
		list = removeDuplicateClusters(list);
		
		int filtered = list.size();
		totalCandidatesDeDupSize += list.size();
		if (list.size() > 0) {
			totalCandidates++;
		}
		
		if (logger.isLoggable(Level.FINE)) {
			logger.fine("\tCandidate List:");
			for (Iterator<Entry<String, ClusterRecord>> it = list.entrySet().iterator(); it.hasNext();) {
				Entry<String, ClusterRecord> entry = it.next();
				cr = entry.getValue();
				for (int i = 0; i < cr.getSize(); i++) {
					OysterIdentityRecord oir = cr.getOysterIdentityRecord(i);
					StringBuilder sb = new StringBuilder(100);
					sb.append("\t\t").append(entry.getKey()).append("=").append(oir.toExplanationString());
					logger.fine(sb.toString());
				}
			}
			logger.fine("");
		}
		/*
		 * if (unfiltered > 100) { StringBuilder sb = new
		 * StringBuilder(100);
		 * sb.append("Input: ").append(clusterRecord.getMergedRecord());
		 * logger.severe(sb.toString()); sb = new StringBuilder(100);
		 * sb.append
		 * ("Unfilter: ").append(unfiltered).append("\tFilter: ").append
		 * (filtered); logger.fine(sb.toString()); } }
		 */
		String index = "", rule = "";
		Map<String, String> consolidate = new LinkedHashMap<String, String>();
		ArrayList<String> prevRefIDs = new ArrayList<String>();
		int identityCount = 1;
		for (Iterator<Entry<String, ClusterRecord>> it = list.entrySet().iterator(); it.hasNext();) {
			Entry<String, ClusterRecord> entry = it.next();
			index = entry.getKey();
			cr = entry.getValue();
			
			matched = false;
			for (int i = 0; i < cr.getSize(); i++) {
				OysterIdentityRecord oir = cr.getOysterIdentityRecord(i);
				if (logger.isLoggable(Level.INFO)) {
					StringBuilder sb = new StringBuilder(100);
					sb.append("\tCompare Input ").append(recordCount).append(" to Identity ").append(identityCount).append(" ").append(oir.toExplanationString());
					logger.info(sb.toString());
				}
				
				if (!matched && !prevRefIDs.contains(oir.get("@RefID"))) {
					double score = applyScoringRules(oir, clusterRecord.getRecords(false));
					
					if (logger.isLoggable(Level.FINE)) {
						logger.fine("Scoring");
						logger.fine("Score: " + score);
					}
					
					// if score greater than the match score then it's
					// a match
					if (score >= attributes.getScoringRule().getMatchScore()) {
						matched = true;
						rule = attributes.getScoringRule().getRuleIdentifer();
					}
					else if(score>=attributes.getScoringRule().getReviewScore()){
						bw.println(oir.toString());
						bw.println(clusterRecord.getRecords(false).toString());
						bw.println("Similarity Score: "+score);
						bw.println("Match Score: "+attributes.getScoringRule().getMatchScore());
						bw.println("Review Score: "+attributes.getScoringRule().getReviewScore());
						bw.println();
					}
					totalMatchedCount++;
					matchedCount++;
					prevRefIDs.add(oir.get("@RefID"));
				}
				
				// save the current RefID
				// prevRefIDs.addElement(oir.get("@RefID"));
				
				// check to see if this result can be consildated with a
				// previous result
				if (matched) {
					consolidate.put(index, rule);
					if (capture) {
						matched = false;
					}
					break;
				}
			}
			
			// take the first match if in non-capture mode
			if (matched && !capture) {
				break;
			}
			
			identityCount++;
		}
		stop = System.currentTimeMillis();
		
		// reset matched flag and get the matched index
		if (consolidate.size() > 0) {
			matched = true;
		}
		
		// Ask IdentityRespository to update/create identity
		if (index != null) {
			manageEntity(cr, index, rule, matched, recordCount, consolidate);
		} else {
			System.out.println("Null index in Candidate List!!!");
			if (logger.isLoggable(Level.INFO)) {
				logger.info("Null index in Candidate List!!!");
			}
		}
		
		if (matched && logger.isLoggable(Level.FINEST)) {
			dump();
		}
		
		// do the latency stats
		long latency = stop - start;
		if (matched) {
			matchingLatency += latency;
			maxMatchingLatency = Math.max(maxMatchingLatency, latency);
			minMatchingLatency = Math.min(minMatchingLatency, latency);
		} else {
			nonMatchingLatency += latency;
			maxNonMatchingLatency = Math.max(maxNonMatchingLatency, latency);
			minNonMatchingLatency = Math.min(minNonMatchingLatency, latency);
		}
	}
	
	private double applyScoringRules(OysterIdentityRecord candidate, LinkedHashSet<OysterIdentityRecord> linkedHashSet) {
		
		double score = 0.0;
		
		// left hand side holds the answers to each term match
		int currentRec=1;
		for (Iterator<OysterIdentityRecord> itRecs = linkedHashSet.iterator(); itRecs.hasNext();) {
			OysterIdentityRecord oir = itRecs.next();
			double ComparisonScore = 0;
			boolean flag = false;
			
			for (Iterator<ScoringRuleTerm> it2 = scoringRule.getTermList().iterator(); it2.hasNext();) {
				ScoringRuleTerm srt = it2.next();
				String item = srt.getItem();
				double overallAgreeWgt = srt.getAgreewgt();
				double overallDisagreeWgt = srt.getDisagreewgt();
				double missingWgt = srt.getMissing();
				boolean hasMissingWgt=srt.isHasmissingweight();
				boolean hasWeightTable = false;
				boolean usesDisagree = false;
				ArrayList<Compare> similarityFunctions = srt.getSimilarityFunctions();
				ArrayList<Transform> dataPrepFunctions = srt.getDataPrepFunctions();
				WeightTableResource owt = srt.getWeightTable();
				if (owt != null) {
					hasWeightTable = true;
					usesDisagree = owt.usesDisagreement();
				}

				// get comparator, source and target
				String source = oir.get(item);
				String target = candidate.get(item);
				if (source==null) source="";
				if (target==null) target="";
				int dataPrepCount = dataPrepFunctions.size();
				if(dataPrepCount>0){
					for (int j=0; j<dataPrepCount; j++) {
	                	source = (dataPrepFunctions.get(j)).transform(source);
	                	target = (dataPrepFunctions.get(j)).transform(target);
					}
				}
				// Setup all of the match conditions
				boolean sourceIsMissing = false;
				if (source.equals("")) sourceIsMissing = true;
				boolean targetIsMissing = false;
				if (target.equals("")) targetIsMissing = true;
				boolean valuesAgree = false;
				for (int j=0; j<similarityFunctions.size(); j++) {
					if((similarityFunctions.get(j)).compare(source, target)) {
						valuesAgree = true;
						break;
					}	
				}
				// particulars for source value
				boolean sourceInTable = false;
				double sourceTableAgreeWgt = 0.0;
				double sourceTableDisagreeWgt = 0.0;
				if (!sourceIsMissing && hasWeightTable) {
					WeightPair sourcePair = owt.getWeightPair(source);
					if (sourcePair!=null) {
						sourceInTable = true;
						sourceTableAgreeWgt = sourcePair.getAgreeWgt();
						sourceTableDisagreeWgt = sourcePair.getDisagreeWgt();
					}
				}
				// particulars for target value
				boolean targetInTable = false;
				double targetTableAgreeWgt = 0.0;
				double targetTableDisagreeWgt = 0.0;
				if (!targetIsMissing && hasWeightTable) {
					WeightPair targetPair = owt.getWeightPair(target);
					if (targetPair!=null) {
						targetInTable = true;
						targetTableAgreeWgt = targetPair.getAgreeWgt();
						targetTableDisagreeWgt = targetPair.getDisagreeWgt();
					}
				}
				double weightToAdd = 0.0;
				double sourceWgt = 0.0;
				double targetWgt = 0.0;
				// Case 1 = neither value is missing
				if (!sourceIsMissing && !targetIsMissing) {
					if (valuesAgree) {
						// Case 1.1 Values Agree
						if (scoringRule.parm1=='X') {
							// Case 1.1.1 Ignore Table
							weightToAdd = overallAgreeWgt;
						} else {
							// Case 1.1.2 Use Table Weights if present
							sourceWgt = overallAgreeWgt;
							if (sourceInTable) sourceWgt = sourceTableAgreeWgt;
							targetWgt = overallAgreeWgt;
							if (targetInTable) targetWgt = targetTableAgreeWgt;
							if (scoringRule.parm2=='L') weightToAdd = Math.max(sourceWgt, targetWgt);
							if (scoringRule.parm2=='S') weightToAdd = Math.min(sourceWgt, targetWgt);
							if (scoringRule.parm2=='A') weightToAdd = (sourceWgt + targetWgt)/2.0;
							if (scoringRule.parm4=='T') {
								if (sourceInTable && !targetInTable) weightToAdd = sourceTableAgreeWgt;
								if (!sourceInTable && targetInTable) weightToAdd = targetTableAgreeWgt;
							}
						}
					} else {
						// Case 1.2 Values Disagree
						if (scoringRule.parm1=='X' || scoringRule.parm1=='A') {
							// Case 1.2.1 Ignore Table
							weightToAdd = overallDisagreeWgt;
						} else {
							// Case 1.2.2 Use Table Weights if present
							sourceWgt = overallDisagreeWgt;
							if (sourceInTable) sourceWgt = sourceTableDisagreeWgt;
							targetWgt = overallDisagreeWgt;
							if (targetInTable) targetWgt = targetTableDisagreeWgt;
							if (scoringRule.parm3=='L') weightToAdd = Math.max(sourceWgt, targetWgt);
							if (scoringRule.parm3=='S') weightToAdd = Math.min(sourceWgt, targetWgt);
							if (scoringRule.parm3=='A') weightToAdd = (sourceWgt + targetWgt)/2.0;
							if (scoringRule.parm5=='T') {
								if (sourceInTable && !targetInTable) weightToAdd = sourceTableDisagreeWgt;
								if (!sourceInTable && targetInTable) weightToAdd = targetTableDisagreeWgt;
							}
						}
					}		
				}
				// Case 2 = source is missing, but target is not missing
				if (sourceIsMissing && !targetIsMissing) {
					weightToAdd = overallDisagreeWgt;
					if ((scoringRule.parm1=='B') && (scoringRule.parm6=='N' || scoringRule.parm6=='B') && targetInTable) {
							sourceWgt = overallDisagreeWgt;
							targetWgt = targetTableDisagreeWgt;
							if (scoringRule.parm3=='L') weightToAdd = Math.max(sourceWgt, targetWgt);
							if (scoringRule.parm3=='S') weightToAdd = Math.min(sourceWgt, targetWgt);
							if (scoringRule.parm3=='A') weightToAdd = (sourceWgt + targetWgt)/2.0;
							if (scoringRule.parm5=='T') weightToAdd = targetTableDisagreeWgt;
					}
					if (scoringRule.parm6=='E' && hasMissingWgt) weightToAdd = missingWgt;
				}	
				// Case 3 = source is not missing, but target is missing
				if (!sourceIsMissing && targetIsMissing) {
					weightToAdd = overallDisagreeWgt;
					if ((scoringRule.parm1=='B') && (scoringRule.parm6=='N' || scoringRule.parm6=='B') && sourceInTable) {
							sourceWgt = sourceTableDisagreeWgt;
							targetWgt = overallDisagreeWgt;
							if (scoringRule.parm3=='L') weightToAdd = Math.max(sourceWgt, targetWgt);
							if (scoringRule.parm3=='S') weightToAdd = Math.min(sourceWgt, targetWgt);
							if (scoringRule.parm3=='A') weightToAdd = (sourceWgt + targetWgt)/2.0;
							if (scoringRule.parm5=='T') weightToAdd = sourceTableDisagreeWgt;
					}
					if (scoringRule.parm6=='E' && hasMissingWgt) weightToAdd = missingWgt;
				}	
				// Case 4 = both source and target are missing
				if (sourceIsMissing && targetIsMissing) {
					weightToAdd = overallDisagreeWgt;
					if ((scoringRule.parm6=='B' || scoringRule.parm6=='E') && hasMissingWgt) weightToAdd = missingWgt;
				}
				
				ComparisonScore += weightToAdd;				
			}		
			//find the lowest score for source cluster
			if(currentRec==1){
				score=ComparisonScore;
			}else if(ComparisonScore<score){
				score=ComparisonScore;
			}		
			currentRec++;
		}
		
		return score;
	}
	
	/**
	 * This method determines the status of the input record then updates the
	 * Entity Map, Entity Set, Link amp and Value Index.
	 * 
	 * @param cr
	 *              the matched ClusterRecord (can be null is not matched)
	 * @param index
	 *              the RefID number that was matched (can be empty is not
	 *              matched)
	 * @param rule
	 *              the rule that was matched on (can be empty is not matched)
	 * @param match
	 *              whether the input record matched a candidate record or not
	 * @param recordCount
	 *              the current record count
	 * @param consolidate
	 *              a list of all the candidates that were found to match (can
	 *              be empty is not matched)
	 */
	private void manageEntity(ClusterRecord cr, String index, String rule, boolean match, long recordCount, Map<String, String> consolidate) {
		// if the index is empty or match flag is false then this is a new
		// record that matches nothing
		if (index.equals("") || !match) {
			if (capture) {
				String id = clusterRecord.getOysterID();
				if (id == null || id.trim().equals("")) {
					id = repository.getNextID(clusterRecord.getMergedRecord(), false);
					clusterRecord.setOysterID(id);
					
					cr = clusterRecord.clone();
					repository.addNewIdentity(id, cr);
				} else {
					cr = clusterRecord.clone();
					repository.updateIdentity(id, cr);
				}
				
				repository.addIndex(cr, mergedList);
				repository.addLink(cr.getMergedRecord(), id, rule);
				
				if (logger.isLoggable(Level.INFO)) {
					StringBuilder sb = new StringBuilder(250);
					sb.append("\tNew Entity ").append(cr.getOysterID()).append(" ").append(cr.getMergedRecord().toExplanationString()).append(System.lineSeparator());
					sb.append("\tNew Index: ").append(cr.getMergedRecord()).append(System.lineSeparator());
					sb.append("\tNew LinkMap{").append(cr.getMergedRecord().get("@RefID")).append("}, ").append(cr.getMergedRecord().toExplanationString());
					logger.info(sb.toString());
				}
			} else {
				/*
				 * String id = repository.getNextID(oir, true);
				 * repository.addLink(oir, id, rule);
				 */
				cr = clusterRecord.clone();
				
				repository.addLink(cr.getMergedRecord(), "XXXXXXXXXXXXXXXX", rule);
				
				if (logger.isLoggable(Level.INFO)) {
					StringBuilder sb = new StringBuilder(100);
					sb.append("\tNo Capture: LinkMap{").append(cr.getMergedRecord().get("@RefID")).append("}, ").append(cr.getMergedRecord().toExplanationString());
					logger.info(sb.toString());
				}
			}
		} else if (match) {
			int min = Integer.MAX_VALUE;
			String minOysterID = "";
			ClusterRecord minCR;
			// minCR.merge(clusterRecord);
			
			// handle any negative assertion
			// first let's see if there is any negative info for the
			// matching candidates
			Map<String, String> negative = new LinkedHashMap<String, String>();
			Map<String, String> negBookKeeping = new LinkedHashMap<String, String>();
			for (Iterator<Entry<String, String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
				Entry<String, String> entry = it.next();
				String oysterID = repository.getLinkMap().get(entry.getKey());
				
				if (oysterID == null) {
					oysterID = repository.getRefIDLookup().get(entry.getKey());
				}
				cr = repository.getEntityMap().getCluster(oysterID);
				
				negBookKeeping.put(cr.getOysterID(), entry.getKey());
				for (Iterator<String> it2 = cr.getNegStrToStr().iterator(); it2.hasNext();) {
					negative.put(it2.next(), cr.getOysterID());
				}
			}
			
			// now lets see what we can reject
			for (Iterator<Entry<String, String>> it = negative.entrySet().iterator(); it.hasNext();) {
				Entry<String, String> entry = it.next();
				if (negative.containsValue(entry.getKey())) {
					// remove cluster
					String refID = negBookKeeping.get(entry.getValue());
					consolidate.remove(refID);
				}
			}
			
			// now lets see if the remainder has a negative connection to
			// the input record
			for (Iterator<String> it = this.clusterRecord.getNegStrToStr().iterator(); it.hasNext();) {
				String s = it.next();
				if (negative.containsValue(s)) {
					// remove cluster
					
				}
			}
			
			// get the min Oyster ID
			if (consolidate.size() > 1) {
				Object[] keys = consolidate.keySet().toArray();
				String refID = (String) keys[0];
				String oysterID = repository.getLinkMap().get(refID);
				
				if (oysterID == null) {
					oysterID = repository.getRefIDLookup().get(refID);
				}
				cr = repository.getEntityMap().getCluster(oysterID);
				
				for (int i = 1; i < keys.length; i++) {
					refID = (String) keys[i];
					oysterID = repository.getLinkMap().get(refID);
					
					if (oysterID == null) {
						oysterID = repository.getRefIDLookup().get(refID);
					}
					ClusterRecord cr2 = repository.getEntityMap().getCluster(oysterID);
					minOysterID = repository.getMinOysterID(cr, cr2);
					
					if (minOysterID.equals(cr2.getOysterID())) {
						cr = cr2;
					}
				}
				minCR = repository.getEntityMap().getCluster(minOysterID);
			} else if (consolidate.size() == 1) {
				for (Iterator<Entry<String, String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
					Entry<String, String> entry = it.next();
					
					String oysterID = repository.getLinkMap().get(entry.getKey());
					
					if (oysterID == null) {
						oysterID = repository.getRefIDLookup().get(entry.getKey());
					}
					
					minOysterID = oysterID;
				}
				minCR = repository.getEntityMap().getCluster(minOysterID);
			} else {
				// this is here just in case neagtive assertions remove
				// all the candidates
				minOysterID = repository.getNextID(clusterRecord.getMergedRecord(), false);
				clusterRecord.setOysterID(minOysterID);
				minCR = clusterRecord.clone();
			}
			
			Set<String> rset = new LinkedHashSet<String>();
			for (Iterator<Entry<String, String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
				Entry<String, String> entry = it.next();
				rule = consolidate.get(entry.getKey());
				rset.add(rule);
				
				String oysterID = repository.getLinkMap().get(entry.getKey());
				
				if (oysterID == null) {
					oysterID = repository.getRefIDLookup().get(entry.getKey());
				}
				
				cr = repository.getEntityMap().getCluster(oysterID);
				
				if (logger.isLoggable(Level.INFO)) {
					StringBuilder sb = new StringBuilder(100);
					sb.append(System.lineSeparator()).append("\tSatisfies Rule ").append(rule);
					logger.info(sb.toString());
				}
				
				if (cr != null && !cr.getOysterID().equals(minCR.getOysterID())) {
					// merge with the min ci
					Set<String> set = new LinkedHashSet<String>();
					set.add(rule);
					minCR.merge(cr, repository.getMid(), set, repository.isTraceOn());
					
					if (cr.isPersistent()) {
						minCR.setPersistent(true);
						
						if (minCR.getCreationDate() == null) {
							minCR.setCreationDate(cr.getCreationDate());
						}
						
						if (minCR.getCreationDate().after(cr.getCreationDate())) {
							minCR.setCreationDate(cr.getCreationDate());
						}
					}
				}
			}
			minCR.merge(clusterRecord, repository.getMid(), rset, repository.isTraceOn());
			
			if (capture) {
				// cr = minCR.clone();
				repository.updateIdentity(minOysterID, minCR);
				
				// remove the merged clusters from the entity map
				for (Iterator<String> it = consolidate.keySet().iterator(); it.hasNext();) {
					String refID = it.next();
					String oysterID = repository.getLinkMap().get(refID);
					
					if (oysterID == null) {
						oysterID = repository.getRefIDLookup().get(refID);
					}
					
					if (!minOysterID.equals(oysterID)) {
						repository.getEntityMap().removeCluster(oysterID);
					}
				}
				
				repository.addIndex(minCR, mergedList);
				// update Link Map
				Set<String> s = minCR.getField("@RefID");
				for (Iterator<String> it = s.iterator(); it.hasNext();) {
					String refID = it.next();
					rule = consolidate.get(refID);
					repository.updateLink(refID, minOysterID, rule, clusterRecord.getValuesByAttribute("@RefID"));
				}
				
				if (logger.isLoggable(Level.INFO)) {
					StringBuilder sb = new StringBuilder(500);
					sb.append("\tAssign Entity ").append(index).append(" to Input ").append(recordCount).append(System.lineSeparator());
					if (cr != null) {
						sb.append("\tUpdate Entity: ").append(cr.getOysterID()).append(" ").append(cr.getMergedRecord().toExplanationString()).append(System.lineSeparator());
					} else {
						sb.append("\tUpdate Entity: ").append(cr.getOysterID()).append(" ").append(clusterRecord.getMergedRecord().toExplanationString())
						            .append(System.lineSeparator());
					}
					sb.append("\t             : ").append(minCR.toString()).append(System.lineSeparator());
					sb.append("\tUpdate Index: ").append(index).append(" for ri ").append(clusterRecord.getMergedRecord()).append(System.lineSeparator());
					sb.append("\tUpdate LinkMap: ").append(minCR.getOysterID()).append(" for ").append(s);
					logger.info(sb.toString());
				}
			} else {
				for (int i = 0; i < minCR.getSize(); i++) {
					OysterIdentityRecord oir = minCR.getOysterIdentityRecord(i);
					rule = consolidate.get(oir.get("@RefID"));
					repository.addLink(oir, minOysterID, rule);
				}
			}
			// I always count this but only use the counter in resolution
			// mode
			resolvedRecords++;
		}
	}
	
	/**
	 * This method is not implemented in this class
	 * 
	 * @param sort
	 * @param recordCount
	 * @param countPoint
	 */
	@Override
	public void postConsolidation(boolean sort, long recordCount, long countPoint) {
	}
}
