/*
 * Copyright 2018 John Talburt, Eric Nelson, and the OYSTER team
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.er;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ualr.oyster.OysterMain;
import edu.ualr.oyster.core.OysterRule;
import edu.ualr.oyster.core.RuleTerm;
import edu.ualr.oyster.data.ClusterRecord;
import edu.ualr.oyster.data.OysterIdentityRecord;
import edu.ualr.oyster.functions.Score;
import edu.ualr.oyster.functions.Compare;

/**
 * Implements identity resolution rules in reference source to determine if reference is 
 * matches an existing entity. This engine finds and grades all matches and returns
 * the best match according the the identity resolution research done by
 * Dr. Fumiko Kobayashi
 * 
 * Responsibilities:
 * <ul>
 * <li>Apply resolution rules from reference source</li>
 * <li>Resolve reference</li>
 * </ul>
 * @author Eric D. Nelson and John R. Talburt
 */
public class OysterKobayashiClusterEngine extends OysterResolutionEngine {
    /**
     * Creates a new instance of OysterKobayashiClusterEngine
     * @param logFile
     * @param logLevel
     */
    public OysterKobayashiClusterEngine (String logFile, Level logLevel, int recordType) {
        super(logFile, logLevel, recordType);
        mergedList = false;
        postConsolidation = false;
    }

    /**
     * Creates a new instance of OysterKobayashiClusterEngine
     * @param log
     */
    public OysterKobayashiClusterEngine (Logger log, int recordType) {
        super(log, recordType);
        mergedList = false;
        postConsolidation = false;
    }
    
    /**
     * Creates a new instance of OysterKobayashiClusterEngine
     */
    public OysterKobayashiClusterEngine (int recordType) {
        super(recordType);
        mergedList = false;
        postConsolidation = false;
    }
    
    /**
     * Adaptation of the OysterClusterEngine to perform Identity Resolution
     * @param sort true if a sorted candidate list is to be used otherwise false.
     * @param recordCount the current record count
     */
    @Override
    public void integrateSource (boolean sort, long recordCount) {
        boolean matched, overallMatch = false;
        int matchedCount = 0;
        ClusterRecord cr = null;
        long start = System.currentTimeMillis(), stop;
        
        if (logger.isLoggable(Level.INFO)) {
            StringBuilder sb = new StringBuilder(100);
            sb.append(System.lineSeparator())
              .append("Input: ").append(clusterRecord.getMergedRecord());
            logger.info(sb.toString());
        }        
/*
        if (clusterRecord.getMergedRecord().get("@RefID").equals("Test17e.2020")){
            System.out.println();
        }
*/
        // Ask IdentityRepository for a list of candidates
        Map<String, ClusterRecord> list;
        if ((list = repository.getCandidateList(clusterRecord, sort, primaryFilter, lcrdMinSize, isByPassFilter(), mergedList)).isEmpty()){
            if (secondaryFilter != null) {
                list = repository.getCandidateList(clusterRecord, sort, secondaryFilter, lcrdMinSize, isByPassFilter(), mergedList);
            }
        }
        
        int unfiltered = list.size();
        totalCandidatesSize += list.size();
        
        // Remove duplicate Clusters
        list = removeDuplicateClusters(list);
        
        int filtered = list.size();
        totalCandidatesDeDupSize += list.size();
        if (list.size() > 0) {
            totalCandidates++;
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("\tCandidate List:");
            for (Iterator<Entry<String, ClusterRecord>> it = list.entrySet().iterator(); it.hasNext();){
                Entry<String, ClusterRecord> entry = it.next();
                cr = entry.getValue();
                for (int i = 0; i < cr.getSize(); i++) {
                    OysterIdentityRecord oir = cr.getOysterIdentityRecord(i);
                    StringBuilder sb = new StringBuilder(100);
                    sb.append("\t\t").append(entry.getKey()).append("=").append(oir.toExplanationString());
                    logger.fine(sb.toString());
                }
            }
            logger.fine("");
        }

        String index, overallIndex = "", rule = "";
        Map<String, String> consolidate = new LinkedHashMap<String, String>();
        ArrayList<String> prevRefIDs = new ArrayList<String>();
        int identityCount = 1;
        for(Iterator<Entry<String, ClusterRecord>> it = list.entrySet().iterator(); it.hasNext();){
            Entry<String, ClusterRecord> entry = it.next();
            index = entry.getKey();
            cr = entry.getValue();
            
            matched = false;
            for (int i = 0; i < cr.getSize(); i++){
                OysterIdentityRecord oir = cr.getOysterIdentityRecord(i);
                if (logger.isLoggable(Level.INFO)) {
                    StringBuilder sb = new StringBuilder(100);
                    sb.append("\tCompare Input ").append(recordCount).append(" to Identity ").append(identityCount).append(" ").append(oir.toExplanationString());
                    logger.info(sb.toString());
                }

                Set<String> completeRules = new TreeSet<String>();
                if (!matched && !prevRefIDs.contains(oir.get("@RefID"))) {
                    Map<String, Boolean> ruleMatches = applyRules(oir, clusterRecord.getRecords(false));

                    if (logger.isLoggable(Level.FINE)) {
                        logger.fine("matrix");
                    }

                    // does the matrix match any of the masks
                    for (Iterator<String> it2 = ruleMatches.keySet().iterator(); it2.hasNext();) {
                        String key = it2.next();
                        boolean b = ruleMatches.get(key);

                        if (logger.isLoggable(Level.FINE)) {
                            logger.fine(String.valueOf(b));
                        }

                        // If the mask matches the temp rule then it's a match
                        if (b) {
                            matched = overallMatch = true;
                            overallIndex = index;
                            completeRules.add(key);
                            rule = key;

                            long value = 0;
                            if (completeRuleFiring.containsKey(completeRules.toString())) {
                                value = completeRuleFiring.get(completeRules.toString());
                            }
                            value++;
                            completeRuleFiring.put(completeRules.toString(), value);

                            if (matchedCount == 0) {
                                rule = key;
                                value = 0;
                                if (ruleFreq.containsKey(key)) {
                                    value = ruleFreq.get(key);
                                }
                                value++;
                                ruleFreq.put(key, value);

                                if (logger.isLoggable(Level.FINE)) {
                                    StringBuilder sb = new StringBuilder(100);
                                    sb.append("\tMatched Rule:").append(key);
                                    logger.fine(sb.toString());
                                    logger.fine("");
                                }
                                totalMatchedCount++;
                            }
                            matchedCount++;
                        }
                    }
                    prevRefIDs.add(oir.get("@RefID"));
                }
                                
                // check to see if this result can be consolidated with a previous result
                if (matched){
                    consolidate.put(index, rule);
                    if (capture) {
                        matched = false;
                    }
                    break;
                }
            }           
            identityCount++;
        }

        matched = overallMatch;
        index = overallIndex;

        stop = System.currentTimeMillis();
  
        // reset matched flag and get the matched index
        
        // new code added to find the maximum matrix score 
        // for each source record over all of the entity matches
        // in the consolidate list
        if (consolidate.size() > 0) {
            matched = true;
            double maxScore = -1;
            String maxConsolidateID = "";
            OysterIdentityRecord maxSourceRecord = null;
            LinkedHashSet<OysterIdentityRecord> sourceRecs = clusterRecord.getRecords(false);
            ArrayList<OysterRule> rules = getRuleList ();
            Set<String> consolidateKeys = consolidate.keySet();
            // first iterate over matched entity records in the consolidate set
        	double consolidateScore = 0.0;
            for(Iterator<String> it = consolidateKeys.iterator(); it.hasNext();) {
            	String consolidateRecID = it.next();
            	// next get the cluster associated with the consolidate RecID
            	// and iterate over the records in the cluster
            	String oysterID = repository.getRefIDLookup().get(consolidateRecID);
            	ClusterRecord consolidateCluster = repository.getEntityMap().getCluster(oysterID);
            	LinkedHashSet<OysterIdentityRecord> consolidateRecs = consolidateCluster.getRecords(false);
            	for(Iterator<OysterIdentityRecord> it2 = consolidateRecs.iterator(); it2.hasNext();) {
            		OysterIdentityRecord consolidateRecord = it2.next();
            		// Next iterate over the records in the source cluster
            		for(Iterator<OysterIdentityRecord> it3 = sourceRecs.iterator(); it3.hasNext();) {
            			OysterIdentityRecord sourceRecord = it3.next();
            			maxSourceRecord = sourceRecord;
    					// Check for special rule setup for Kobayashi Engine
    					boolean badSetup = false;
            			// Check if only one rule
    					if (rules.size()>1) badSetup = true;
            			OysterRule or = rules.get(0);
            			// Check if only one term in rule
            			ArrayList<RuleTerm> termList = new ArrayList<RuleTerm>();
            			termList.addAll(or.getTermList());
            			if (termList.size()>1) badSetup = true;
            			// Must only have one similarity in rule term
            			RuleTerm rt = termList.get(0);
            			ArrayList<String> similaritySignatures = rt.getSimilaritySignatures();
            			if (similaritySignatures.size()>1) badSetup = true;
            			// The single similarity must be a MatrixComparator
            			String sig = similaritySignatures.get(0);
            			sig = sig.toUpperCase();
            			if (!sig.startsWith("MATRIXCOMPARATOR(")) badSetup = true;
            			// If not correct setup, stop here
            			if (badSetup) {
        					Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n###\n###ERROR: Kobayashi Engine must have one rule with one term using MatrixComparator!!!\n###\n");
        					System.out.println("\n###\n###ERROR: Kobayashi Engine must have one rule with one term using MatrixComparator!!!\n###\n");
        					System.exit(0);
            			}
            			String itemToMatch = rt.getItem();
            			String targetValue = consolidateRecord.get(itemToMatch);
    					String sourceValue = sourceRecord.get(itemToMatch);
    					// Get MatrixComparator from rule
    					Compare func = rt.getSimilarityFunction(0);
    					double score = ((Score)func).score(targetValue, sourceValue);
        				consolidateScore = consolidateScore + score;
            		}
            		// here is the turn-around of the source iterator
            	}
            	// here is the turn-around of the consolidate iterator
            	if(consolidateScore > maxScore){
            		maxScore = consolidateScore;
            		maxConsolidateID = consolidateRecID;
            	}
            	consolidateScore = 0.0;
            }
            // This the end of the iteration
            // Here will empty consolidate list and insert entity with highest match
            consolidate.clear();
            String maxScoreString = String.valueOf(maxScore);
            rule = maxScoreString;
            consolidate.put(maxConsolidateID, maxScoreString);
            String oysterID = repository.getRefIDLookup().get(maxConsolidateID);
            rule = rule.concat(", "+maxConsolidateID);
            repository.addLink(maxSourceRecord, oysterID, rule);
        }
        
    
        // Ask IdentityRespository to update/create identity
        if (index != null){
            manageEntity(cr, index, rule, matched, recordCount, consolidate);
        } else{
            System.out.println("Null index in Candidate List!!!");
            if (logger.isLoggable(Level.INFO)){
                logger.info("Null index in Candidate List!!!");
            }
        }
        
        if (matched && logger.isLoggable(Level.FINEST)){
            dump();
        }
        
        // do the latency stats
        long latency = stop - start;
        if (matched){
            matchingLatency += latency;
            maxMatchingLatency = Math.max(maxMatchingLatency, latency);
            minMatchingLatency = Math.min(minMatchingLatency, latency);
        } else {
            nonMatchingLatency += latency;
            maxNonMatchingLatency = Math.max(maxNonMatchingLatency, latency);
            minNonMatchingLatency = Math.min(minNonMatchingLatency, latency);
        }
    }

    /**
     * This method determines the status of the input record then updates the 
     * Entity Map, Entity Set, Link amp and Value Index.
     * @param cr the matched ClusterRecord (can be null is not matched)
     * @param index the RefID number that was matched  (can be empty is not matched)
     * @param rule the rule that was matched on (can be empty is not matched)
     * @param match whether the input record matched a candidate record or not
     * @param recordCount the current record count
     * @param consolidate a list of all the candidates that were found to match 
     * (can be empty is not matched)
     */
    private void manageEntity(ClusterRecord cr, String index, String rule, boolean match, long recordCount, Map<String, String> consolidate) {
        // if the index is empty or match flag is false then this is a new 
        // record that matches nothing
        if (index.equals("") || !match) {
            if (capture) {
                String id = clusterRecord.getOysterID();
                if (id == null || id.trim().equals("")) {
                    id = repository.getNextID(clusterRecord.getMergedRecord(), false);
                    clusterRecord.setOysterID(id);

                    cr = clusterRecord.clone();
                    repository.addNewIdentity(id, cr);
                } else {
                    cr = clusterRecord.clone();
                    repository.updateIdentity(id, cr);
                }

                repository.addIndex(cr, mergedList);
                repository.addLink(cr.getMergedRecord(), id, rule);
                    
                if (logger.isLoggable(Level.INFO)) {
                    StringBuilder sb = new StringBuilder(250);
                    sb.append("\tNew Entity ").append(cr.getOysterID())
                      .append(" ")
                      .append(cr.getMergedRecord().toExplanationString())
                      .append(System.lineSeparator());
                    sb.append("\tNew Index: ").append(cr.getMergedRecord())
                      .append(System.lineSeparator());
                    sb.append("\tNew LinkMap{")
                      .append(cr.getMergedRecord().get("@RefID"))
                      .append("}, ")
                      .append(cr.getMergedRecord().toExplanationString());
                    logger.info(sb.toString());
                }
            } else {
                /*                    
                String id = repository.getNextID(oir, true);
                repository.addLink(oir, id, rule);
                 */
                cr = clusterRecord.clone();
                
                repository.addLink(cr.getMergedRecord(), "XXXXXXXXXXXXXXXX", rule);

                if (logger.isLoggable(Level.INFO)) {
                    StringBuilder sb = new StringBuilder(100);
                    sb.append("\tNo Capture: LinkMap{")
                      .append(cr.getMergedRecord().get("@RefID"))
                      .append("}, ")
                      .append(cr.getMergedRecord().toExplanationString());
                    logger.info(sb.toString());
                }
            }
        } else if (match) {
            int min = Integer.MAX_VALUE;
            String minOysterID = "";
            ClusterRecord minCR;
// ************************************************************************************
//          minCR.merge(clusterRecord);
            
            // handle any negative assertion
            // first let's see if there is any negative info for the matching candidates
            Map<String, String> negative = new LinkedHashMap<String, String>();
            Map<String, String> negBookKeeping = new LinkedHashMap<String, String>();
            for (Iterator<Entry<String,String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
                Entry<String,String> entry = it.next();
                String oysterID = repository.getLinkMap().get(entry.getKey());

                if (oysterID == null) {
                    oysterID = repository.getRefIDLookup().get(entry.getKey());
                }
                cr = repository.getEntityMap().getCluster(oysterID);
                
                negBookKeeping.put(cr.getOysterID(), entry.getKey());
                for (Iterator<String> it2 = cr.getNegStrToStr().iterator(); it2.hasNext();) {
                    negative.put(it2.next(), cr.getOysterID());
                }
            }
            
            // now lets see what we can reject
            for (Iterator<Entry<String,String>> it = negative.entrySet().iterator(); it.hasNext();) {
                Entry<String,String> entry = it.next();
                if (negative.containsValue(entry.getKey())){
                    // remove cluster
                    String refID = negBookKeeping.get(entry.getValue());
                    consolidate.remove(refID);
                }
            }
            
            // now lets see if the remainder has a negative connection to the input record
            for (Iterator<String> it = this.clusterRecord.getNegStrToStr().iterator(); it.hasNext();) {
                String s = it.next();
                if (negative.containsValue(s)){
                    // remove cluster
                    
                }
            }
            
            // get the min Oyster ID
            if (consolidate.size() > 1) {
                Object [] keys = consolidate.keySet().toArray();
                String refID = (String) keys[0];
                String oysterID = repository.getLinkMap().get(refID);

                if (oysterID == null) {
                    oysterID = repository.getRefIDLookup().get(refID);
                }
                cr = repository.getEntityMap().getCluster(oysterID);

                for (int i = 1; i < keys.length; i++) {
                    refID = (String) keys[i];
                    oysterID = repository.getLinkMap().get(refID);

                    if (oysterID == null) {
                        oysterID = repository.getRefIDLookup().get(refID);
                    }
                    ClusterRecord cr2 = repository.getEntityMap().getCluster(oysterID);
                    minOysterID = repository.getMinOysterID(cr, cr2);
                    
                    if (minOysterID.equals(cr2.getOysterID())){
                        cr = cr2;
                    }
                }
                minCR = repository.getEntityMap().getCluster(minOysterID);
            } else if (consolidate.size() == 1) {
                for (Iterator<Entry<String, String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
                    Entry<String, String> entry = it.next();

                    String oysterID = repository.getLinkMap().get(entry.getKey());

                    if (oysterID == null) {
                        oysterID = repository.getRefIDLookup().get(entry.getKey());
                    }

                    minOysterID = oysterID;
                }
                minCR = repository.getEntityMap().getCluster(minOysterID);
            } else {
                // this is here just in case neagtive assertions remove all the candidates
                minOysterID = repository.getNextID(clusterRecord.getMergedRecord(), false);
                clusterRecord.setOysterID(minOysterID);
                minCR = clusterRecord.clone();
            }
            
            Set<String> rset = new LinkedHashSet<String>();
            for (Iterator<Entry<String, String>> it = consolidate.entrySet().iterator(); it.hasNext();) {
                Entry<String, String> entry = it.next();
                rule = consolidate.get(entry.getKey());
                rset.add(rule);
                
                String oysterID = repository.getLinkMap().get(entry.getKey());

                if (oysterID == null) {
                    oysterID = repository.getRefIDLookup().get(entry.getKey());
                }
                
                cr = repository.getEntityMap().getCluster(oysterID);

                if (logger.isLoggable(Level.INFO)) {
                    StringBuilder sb = new StringBuilder(100);
                    sb.append(System.lineSeparator())
                      .append("\tSatisfies Rule ").append(rule);
                    logger.info(sb.toString());
                }

                if (cr != null && !cr.getOysterID().equals(minCR.getOysterID())) {
                    // merge with the min ci
                    Set<String> set = new LinkedHashSet<String>();
                    set.add(rule);
                    minCR.merge(cr, repository.getMid(), set, repository.isTraceOn());
                    
                    if (cr.isPersistent()){
                        minCR.setPersistent(true);
                        
                        if (minCR.getCreationDate() == null) {
                            minCR.setCreationDate(cr.getCreationDate());
                        }
                        
                        if (minCR.getCreationDate().after(cr.getCreationDate())) {
                            minCR.setCreationDate(cr.getCreationDate());
                        }
                    }
                }
            }
            // minCR.merge(clusterRecord, repository.getMid(), rset, repository.isTraceOn());

            if (capture) {
//                cr = minCR.clone();
                repository.updateIdentity(minOysterID, minCR);
                
                // remove the merged clusters from the entity map
                for (Iterator<String> it = consolidate.keySet().iterator(); it.hasNext();) {
                    String refID = it.next();
                    String oysterID = repository.getLinkMap().get(refID);

                    if (oysterID == null) {
                        oysterID = repository.getRefIDLookup().get(refID);
                    }
                    
                    if(!minOysterID.equals(oysterID)) {
                        repository.getEntityMap().removeCluster(oysterID);
                    }
                }

                repository.addIndex(minCR, mergedList);
                // update Link Map
                Set<String> s = minCR.getField("@RefID");
                for (Iterator<String> it = s.iterator(); it.hasNext();) {
                    String refID = it.next();

                    if (clusterRecord.getMergedRecord().get("@RefID").equals(refID)){
                        for (Iterator<String> it2 = rset.iterator(); it2.hasNext();){
                            repository.updateLink(refID, minOysterID, it2.next(), clusterRecord.getValuesByAttribute("@RefID"));
                        }
                    } else {
                        repository.updateLink(refID, minOysterID, "", clusterRecord.getValuesByAttribute("@RefID"));
                    }
                }
                
                if (logger.isLoggable(Level.INFO)) {
                    StringBuilder sb = new StringBuilder(500);
                    sb.append("\tAssign Entity ")
                      .append(index)
                      .append(" to Input ")
                      .append(recordCount)
                      .append(System.lineSeparator());
                    if (cr != null) {
                        sb.append("\tUpdate Entity: ")
                          .append(cr.getOysterID())
                          .append(" ")
                          .append(cr.getMergedRecord().toExplanationString())
                          .append(System.lineSeparator());
                    } else {
                        sb.append("\tUpdate Entity: ")
                          .append(cr.getOysterID())
                          .append(" ")
                          .append(clusterRecord.getMergedRecord().toExplanationString())
                          .append(System.lineSeparator());
                    }
                    sb.append("\t             : ")
                      .append(minCR.toString())
                      .append(System.lineSeparator());
                    sb.append("\tUpdate Index: ")
                      .append(index)
                      .append(" for ri ")
                      .append(clusterRecord.getMergedRecord())
                      .append(System.lineSeparator());
                    sb.append("\tUpdate LinkMap: ")
                      .append(minCR.getOysterID())
                      .append(" for ")
                      .append(s);
                    logger.info(sb.toString());
                }
            } else {
                for (int i = 0; i < minCR.getSize(); i++) {
                    OysterIdentityRecord oir = minCR.getOysterIdentityRecord(i);
                    rule = consolidate.get(oir.get("@RefID"));
                    repository.addLink(oir, minOysterID, rule);
                }
            }
            // I always count this but only use the counter in resolution mode
            resolvedRecords++;
        }
    }

    /**
     * This method is not implemented in this class
     * @param sort
     * @param recordCount
     * @param countPoint
     */
    @Override
    public void postConsolidation(boolean sort, long recordCount, long countPoint) {
    }
}
