package edu.ualr.oyster.functions;

import org.apache.commons.lang3.StringUtils;

public class ParamInt extends Param {
	/*
	 * Minimum allowed value
	 */
	protected int minimum;
	
	/**
	 * @return the minimum limit or null
	 */
	protected int getMinimum() {
		return minimum;
	}
	
	/**
	 * Is the specified value under the minimum limit
	 * 
	 * Null-safe
	 * 
	 * @param value
	 * @return
	 */
	protected boolean isUnderMinimum(int value) {
		if (value < this.minimum) {
			return true;
		}
		return false;
	}
	
	/**
	 * Set the minimum 
	 * 
	 * @param minimum limit
	 */
	protected void setMinimum(int minimum) {
		this.minimum = minimum;
	}

	protected int maximum;

	/**
	 * @return the maximum limit or null
	 */
	protected int getMaximum() {
		return maximum;
	}
	
	/**
	 * Is the specified value over the maximum limit
	 * 
	 * Null-safe
	 * 
	 * @param value
	 * @return
	 */
	protected boolean isOverMaximum(int value) {
		if (value > this.maximum) {
			return true;
		}
		return false;
	}
	/**
	 * @param maximum limit
	 */
	protected void setMaximum(int maximum) {
		this.maximum = maximum;
	}

	/**
	 * Current value
	 */
	protected int value;

	/**
	 * Return the current value
	 * 
	 * @return a value in the range of min to max
	 */
	public int getValue() {
		return this.value;
	}
	
	/**
	 * Compares a passed value to the current value
	 * 
	 * Null-safe
	 * 
	 * @param value to compare
	 * @return 0=equals, 1=greater than, -1=less than
	 */
	public int compareValue(int value) {
		if (value < this.value) {
			return -1;
		}
		if (value > this.value) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * Set the threshold from a string value This parses the string to a int and
	 * call the numeric set'er
	 * 
	 * @param value as a string
	 * @throws IllegalArgumentException for non-decimal values
	 */
	public void setValue(String value) throws IllegalArgumentException {
		this.valid = false;
		try {
			setValue(Integer.parseInt(StringUtils.trim(value)));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format("%1$s value %2$s is not a valid number",
					this.getClass().getSimpleName(),value));
		}
	}

	/**
	 * Set the threshold from a number and validates the range
	 * 
	 * @param value to set
	 * @throws IllegalArgumentException for values below the minimum or over the maximum
	 */
	public void setValue(Integer value) throws IllegalArgumentException {
		this.valid = false;
		if (!isNormalized(value)) {
			throw new IllegalArgumentException(String.format("%1$s value %2$d must be between %3$d and %4$d",
					this.getClass().getSimpleName(),value,this.minimum, this.maximum));
		}
		this.valid = true;
		this.value = value;
	}

	/**
	 * No-argument constructor
	 * Defaults range values
	 */
	public ParamInt() {
		super();
		setMinimum(Integer.MIN_VALUE);
		setMaximum(Integer.MAX_VALUE);
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	public ParamInt(int minimum, int maximum) {
		super();
		setMinimum(minimum);
		setMaximum(maximum);
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	public ParamInt(int minimum, int maximum, int value) {
		super();
		setMinimum(minimum);
		setMaximum(maximum);
		setValue(value);
	}
	
	/**
	 * Determine if the value is is normalized ie. falls within the acceptable range
	 * 
	 * @return true=valid, false=invalid
	 */
	public boolean isNormalized(int value) {
		if (isUnderMinimum(value)) {
			return false;
		}
		if (isOverMaximum(value)) {
			return false;
		}
		return true;
	};
	
	/**
	 * Test a value against the value
	 * 
	 * @param value to test
	 * @return true=value is >= value, false=value < value
	 */
	public boolean test(int value) {
		return (compareValue(value) >= 0); 
	}

}
