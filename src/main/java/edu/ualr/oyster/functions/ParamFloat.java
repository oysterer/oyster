package edu.ualr.oyster.functions;

import org.apache.commons.lang3.StringUtils;

public class ParamFloat extends Param {

	/*
	 * Minimum allowed value
	 */
	protected float minimum;
	
	/**
	 * @return the minimum limit or null
	 */
	protected float getMinimum() {
		return minimum;
	}
	
	/**
	 * Is the specified value under the minimum limit
	 * 
	 * Null-safe
	 * 
	 * @param value
	 * @return
	 */
	protected boolean isUnderMinimum(float value) {
		if (value < this.minimum) {
			return true;
		}
		return false;
	}
	
	/**
	 * Set the minimum 
	 * 
	 * @param minimum limit
	 */
	protected void setMinimum(float minimum) {
		this.minimum = minimum;
	}

	protected float maximum;

	/**
	 * @return the maximum limit or null
	 */
	protected float getMaximum() {
		return maximum;
	}
	
	/**
	 * Is the specified value over the maximum limit
	 * 
	 * Null-safe
	 * 
	 * @param value
	 * @return
	 */
	protected boolean isOverMaximum(float value) {
		if (value > this.maximum) {
			return true;
		}
		return false;
	}
	/**
	 * @param maximum limit
	 */
	protected void setMaximum(float maximum) {
		this.maximum = maximum;
	}

	/**
	 * Current value
	 */
	protected float value;

	/**
	 * Return the current value
	 * 
	 * @return a value in the range of min to max
	 */
	public float getValue() {
		return this.value;
	}
	
	/**
	 * Compares a passed value to the current value
	 * 
	 * @param value to compare
	 * @return 0=equals, 1=greater than, -1=less than
	 */
	public int compareValue(float value) {
		if (value < this.value) {
			return -1;
		}
		if (value > this.value) {
			return 1;
		}
		return 0;
	}

	/**
	 * Set the threshold from a string value This parses the string to a float and
	 * call the numeric set'er
	 * 
	 * @param value as a string
	 * @throws IllegalArgumentException for non-decimal values
	 */
	public void setValue(String value) throws IllegalArgumentException {
		this.valid = false;
		try {
			setValue(Float.parseFloat(StringUtils.trim(value)));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format("%1$s value %2$s is not a valid decimal",
					this.getClass().getSimpleName(),value));
		}
	}

	/**
	 * Set the threshold from a float and validates the range
	 * 
	 * @param value as a float
	 * @throws IllegalArgumentException for values below the minimum or over the maximum
	 */
	public void setValue(Float value) throws IllegalArgumentException {
		this.valid = false;
		if ((value < this.minimum) || (value > this.maximum)) {
			throw new IllegalArgumentException(String.format("%1$s value %2$f must be between %3$f and %4$f",
					this.getClass().getSimpleName(),value,this.minimum, this.maximum));
		}
		this.valid = true;
		this.value = value;
	}

	/**
	 * No-argument constructor
	 * Defaults range values
	 */
	public ParamFloat() {
		// Standard range is normalized from 0.0 - 1.0
		super();
		setMinimum(0.0f);
		setMaximum(1.0f);
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	public ParamFloat(float minimum, float maximum) {
		super();
		setMinimum(minimum);
		setMaximum(maximum);
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	public ParamFloat(float minimum, float maximum, float value) {
		super();
		setMinimum(minimum);
		setMaximum(maximum);
		setValue(value);
	}
	
	
	/**
	 * Determine if the value is is normalized ie. falls within the acceptable range
	 * 
	 * @return true=valid, false=invalid
	 */
	public boolean isNormalized(float value) {
		if (isUnderMinimum(value)) {
			return false;
		}
		if (isOverMaximum(value)) {
			return false;
		}
		return true;
	};
	
	/**
	 * Test a value against the value
	 * 
	 * @param value to test
	 * @return true=value is >= value, false=value < value
	 */
	public boolean test(float value) {
		if (value < this.value) {
			return false;
		}
		return true; 
	}

}
