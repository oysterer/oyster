/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Base class for all Oyster functions
 * 
 * This is an abstract class and cannot be instantiated
 * 
 * If the subclass takes configuration parameters, it should 
 * @Override the configure() method which is called with the 
 * configuration parameters after the instance of the class is constructed
 * 
 * To instantiate a new instance of a subclass use the builder
 * OysterFuction function = new OysteFunctionSample.Builder(configuration).build();
 */
public abstract class OysterFunction implements OysterAction {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    
    /**
     * =====================================================================
     * Enumeration of defined function subclasses
     * This serves as a nexus for all OysterFunction subclasses and provides
     * a centralized mechanism to provide the class for the builder.
     * 
     * The getClass method is protected so that it cannot be called externally.
     * =====================================================================
     */
    public static enum FunctionType {

    	// Deterministic functions
    	ALIAS("edu.ualr.oyster.functions.deterministic.Alias"),
    	CAVERPHONE("edu.ualr.oyster.functions.deterministic.Caverphone1"),
    	CAVERPHONE2("edu.ualr.oyster.functions.deterministic.Caverphone2"),
    	COSINE("edu.ualr.oyster.functions.probabilistic.Cosine"),
    	DMSOUNDEX("edu.ualr.oyster.functions.deterministic.DaitchMokotoffSoundex"),
    	EXACT("edu.ualr.oyster.functions.deterministic.Exact"),
    	MISSING("edu.ualr.oyster.functions.deterministic.Missing"),
    	FINGERPRINT("edu.ualr.oyster.functions.deterministic.Fingerprint"),
    	IBMALPHACODE("edu.ualr.oyster.functions.deterministic.IBMAlphaCode"),
    	INITIAL("edu.ualr.oyster.functions.deterministic.Initial"),
    	MATCHRATING("edu.ualr.oyster.functions.deterministic.MatchRatingApproach"),
    	METAPHONE("edu.ualr.oyster.functions.deterministic.Metaphone"),
    	METAPHONE2("edu.ualr.oyster.functions.deterministic.DoubleMetaphone"),
		NICKNAME("edu.ualr.oyster.functions.deterministic.NickName"),
    	NYSIIS("edu.ualr.oyster.functions.deterministic.NYSIISCode"),
    	SCAN("edu.ualr.oyster.functions.deterministic.Scan"),
    	SOUNDEX("edu.ualr.oyster.functions.deterministic.Soundex"),
    	PSUBSTR("edu.ualr.oyster.functions.deterministic.SubstringProper"),
    	SUBSTR("edu.ualr.oyster.functions.deterministic.Substring"),
    	SUBSTRLEFT("edu.ualr.oyster.functions.deterministic.SubstringLeft"),
    	SUBSTRMID("edu.ualr.oyster.functions.deterministic.SubstringMid"),
    	SUBSTRRIGHT("edu.ualr.oyster.functions.deterministic.SubstringRight"),
    	STOPWORDS("edu.ualr.oyster.functions.deterministic.Stopwords"),
    	STOPWORDSFILE("edu.ualr.oyster.functions.deterministic.StopwordsFile"),
    	TRANSPOSE("edu.ualr.oyster.functions.deterministic.Transpose"),
    	
    	// Probabilistic functions
        DLED("edu.ualr.oyster.functions.probabilistic.DamerauLevenshtein"),
    	JACCARD("edu.ualr.oyster.functions.probabilistic.Jaccard"),
    	JAROWINKLER("edu.ualr.oyster.functions.probabilistic.JaroWinkler"),
    	LED("edu.ualr.oyster.functions.probabilistic.LevenshteinEditDistance"),
    	LEDSCORE("edu.ualr.oyster.functions.probabilistic.LevenshteinEditDistance"),
    	LISTOVERLAP("edu.ualr.oyster.functions.probabilistic.ListOverlap"),
    	LISTOVERLAPPV("edu.ualr.oyster.functions.probabilistic.ListOverlapPV"),
    	MAXQGRAM("edu.ualr.oyster.functions.probabilistic.MaxQGram"),
    	QTR("edu.ualr.oyster.functions.probabilistic.QGramTetrahedralRatio"),
       	SMITHWATERMAN("edu.ualr.oyster.functions.probabilistic.SmithWaterman"),
       	SORENSEN("edu.ualr.oyster.functions.probabilistic.Sorensen"),
    	TANIMOTO("edu.ualr.oyster.functions.probabilistic.Tanimoto"),
    	TVERSKY("edu.ualr.oyster.functions.probabilistic.TverskyIndex"),

    	// List & Matrix functions
    	LISTTOKENIZER("edu.ualr.oyster.functions.tokenizer.ListTokenizer"),
    	LISTTOKENIZERPV("edu.ualr.oyster.functions.tokenizer.ListTokenizerPV"),
    	MATRIXCOMPARATOR("edu.ualr.oyster.functions.probabilistic.MatrixComparator"),
    	MATRIXCOMPARATOR2("edu.ualr.oyster.functions.experimental.MatrixComparator2"),
    	MATRIXTOKENIZER("edu.ualr.oyster.functions.tokenizer.MatrixTokenizer"),
    	MATRIXCOMPARATORWEIGHTED("edu.ualr.oyster.functions.probabilistic.MatrixComparatorWeighted"),
    	MATRIXTOKENIZERWEIGHTED("edu.ualr.oyster.functions.tokenizer.MatrixTokenizerWeighted"),
		
    	// Test functions: for unit testing only, do not document
    	NOTFOUND("edu.ualr.oyster.sample.notfoundclass");
    	
    	protected final static Logger logger = LoggerFactory.getLogger(FunctionType.class.getName());
    
    	private final String functionClassName;
    	private Class<? extends OysterFunction> functionClass;
    	
    	/**
    	 * Constructor takes a function name and the associated classname
    	 * It retrieves the class definition for the class name from the class loader
    	 * If the class is not found this will throw a classNotFoundExcepiton 
    	 * 
    	 * The unchecked warning is suppressed because we have validated the class
    	 * 
    	 * @param className
    	 */
		private FunctionType(String className) {
    		this.functionClassName = className;
    		this.functionClass = null;
    	}

    	/**
    	 * Get the function name
    	 * 
    	 * @return the user friendly name of the function
    	 */
    	public String getFunctionName() {
    		return this.name();
    	}

    	/**
    	 * Get the class name
    	 * 
    	 * @return the fully qualified name of the function class
    	 */
    	public String getFunctionClassName() {
    		return this.functionClassName;
    	}
    	
    	@Override
    	public String toString() {
    		if (this.equals(NOTFOUND)) {
    			return "";
    		} else {
    			return this.name();
    		}
    	}

    	/**
    	 * Get the class.  This can return a null if the class could not be found
    	 * 
    	 * This uses lazy initialization and caching so that the class is
    	 * retrieved only when used and saved for reuse so that Class loader
    	 * exceptions can be delayed until the program is fully initialized and
    	 * can be handled by the program logic rather than causing program loading
    	 * to fail
    	 * 
    	 * @return the Class definition of this function
    	 */
    	@SuppressWarnings("unchecked")
		public Class<? extends OysterFunction> getFunctionClass() {
    		if (this.functionClass != null) {
        		return this.functionClass;    			
    		}
    		try {
   				Class<?> functionClass = Class.forName(this.functionClassName);
   				Object function = functionClass.newInstance();
   				if (function instanceof OysterFunction) {
   	    			this.functionClass = (Class<? extends OysterFunction>) functionClass;
   	    			return this.functionClass;    			
   				} else {
   					logger.error("Function {} Class {} is not an instance of OysterFunction and cannot be used", 
   							this.getFunctionName(),this.functionClassName);
   				}
			} catch (ClassNotFoundException e) {
				logger.error("Function {} Class {} not found and cannot be used", 
						this.getFunctionName(),this.functionClassName);
				
			} catch (InstantiationException e) {
				logger.error("Function {} Class {} could not be instantiated and cannot be used", 
						this.getFunctionName(),this.functionClassName);
			} catch (IllegalAccessException e) {
				logger.error("Function {} Class {} cannot be accessed and cannot be used", 
						this.getFunctionName(),this.functionClassName);
			}
    		return null;
    	}

    	/**
    	 * Get the enumeration by the class name (ignoring case)
    	 * 
    	 * @param name
    	 * @return Enum or null if no match
    	 */
    	public static FunctionType getByClassName(String name) {
    		for (FunctionType entry : FunctionType.values()) {
    			if (StringUtils.equals(entry.getFunctionClassName(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}

    	/**
    	 * Get the enumeration by the name (ignoring case)
    	 * 
    	 * @param name
    	 * @return Enum or null if no match
    	 */
    	public static FunctionType getByName(String name) {
    		for (FunctionType entry : FunctionType.values()) {
    			if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}
    }
	
	/**
	 * Helper function to generate a list of arguments from an enum. 
	 * The enum should override the toString method if it needs to modify 
	 * the list, either to transform the enum constants or exclude items from teh list
	 * 
	 * @param e enumeration to list
	 * @return String
	 */
	public static String getOptions(Class<? extends Enum<?>> e) {
		List<String> optionList = new ArrayList<String>();
		for (Enum<?> func : e.getEnumConstants()) {
			// toString lets an enum element exclude itself from the list
			String option = func.toString();
			if (!StringUtils.isBlank(option)) {
				optionList.add(func.toString());
			}
		}
		return new ToStringBuilder(e,ToStringStyle.SIMPLE_STYLE)
				.append(optionList)
				.toString();
	}
    
    
    /**
	 * ==============================================================
     * Static builder for OysterFunctionSample
     * There the only constructor requires a string with the specific 
     * function name and any parameters     
	 * ==============================================================
     */
    public static class Builder<T extends OysterAction> {
    	
    	protected final static Logger logger = LoggerFactory.getLogger(Builder.class.getName());
    	
    	/*
    	 *  Create a reference to the result type to validate interface availability
    	 *  This has to be initialized by passing the actual class to the constructor
    	 *  which kind of defeats the purpose of the template
    	 */
    	//private Class<T> resultType;
    	
    	private FunctionType type;
    	private String name;
    	
        private boolean reverseLogic;
		public boolean getReverseLogic() {
			return reverseLogic;
		}
		public void setReverseLogic(boolean reverseLogic) {
			this.reverseLogic = reverseLogic;
		}

    	private String parameters;        
		public void setParameterList(String parameterList) {
			this.parameters = parameterList;
		}
    	
		/**
		 * Constructor that takes the function definition string from from the configuration
		 * 
		 * The parameter list is everything between the first ( and before the last )
		 * 
		 * @param signature definition string
		 * @throws ClassNotFoundException if the class name is not supplied
		 */
    	public Builder(String signature) throws IllegalArgumentException {
    		if (StringUtils.isBlank(signature)) {
    			throw new IllegalArgumentException("Missing OysterFunction name"); 
    		}
    		if (StringUtils.startsWith(signature, "!") || StringUtils.startsWith(signature, "~")) {
        		this.reverseLogic = true;
        		// Remove the !
        		signature = signature.substring(1);
    		} else {
        		this.reverseLogic = false;
    		}
    		this.name = StringUtils.substringBefore(signature, "(");
    		if (StringUtils.contains(signature, "(")) {
    			this.parameters = StringUtils.substringBeforeLast(StringUtils.substringAfter(signature,"("),")");
    		} else {
    			this.parameters = "";
    		}
    		this.type = FunctionType.getByName(name);
    		if (this.type == null) {
    			logger.error("Oyster Function {} not recognized, valid choices are:", this.name);
    			logger.error("\t{}", OysterFunction.getOptions(FunctionType.class));
				throw new IllegalArgumentException(String.format("'%1$s' is not a valid Oyster function name!",this.name));
    		}
    	}
    	
    	/**
    	 * Build the requested function
    	 * @return The requested OysterFunction subclass
    	 * 
    	 * @param returnType of function to be validated
    	 * @throws InstantiationException if the subclass cannot be constructed
    	 * @throws IllegalAccessException if the caller does not have access to the constructor
    	 * @throws ClassNotFoundException if the className could not be resolved to a class by the classloader
    	 */
		@SuppressWarnings("unchecked")
		public T build() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    		
    		// Get the class for this type and validate it
    		Class<? extends OysterFunction> functionClass = type.getFunctionClass();
    		if (functionClass == null) {
    			throw new ClassNotFoundException(String.format("Oyster Function %1$s class %2$s not defined ",
    					type.getFunctionName(),type.getFunctionClassName()));
    		}
    		
    		// Construct and initialize the class
    		try {
	    		// Instantiate the requested class
    			OysterFunction functionInstance = functionClass.newInstance();
    			functionInstance.setParameters(this.parameters);
    			functionInstance.setReverseLogic(this.reverseLogic);
    			
	    		// Have the subclass parse its parameters (encapsulation)
	    		functionInstance.configure(parameters);
	    		
    			// Return the requested type
    			return getTypedInstance((T)functionInstance);
    		} catch (Exception ex) {
    			// Class Not Found, Class Cast Failed, etc.
    			logger.error("{} : {}", ex.getClass().getName(),ex.getMessage());
    			throw new IllegalArgumentException(String.format("Oyster Function %1$s could not be created",
    					type.getFunctionName()),ex);
    		}
        }
	    
		/**
		 * This is an attempt to force a ClassCastException inside the build method
		 * Not successful so far but will keep trying.  When it works it will catch this exception
		 * and re-throw the IllegalArgumentException (with detailed information)
		 * https://www.javacodegeeks.com/2013/12/advanced-java-generics-retreiving-generic-type-arguments.html
		 * 
		 * @param instance
		 * @return
		 */
	    private T getTypedInstance(T instance) {
			logger.debug("Built {}",instance.toString());
			return instance;
	    	
	    }
    }
    
    /*
	 * ==============================================================
     * OysterFunction properties and methods
	 * ==============================================================
     */
    
    // Enumerated type of the function
	protected FunctionType type;
	public FunctionType getType() {
		return type;
	}
	protected void setType(FunctionType type) {
		this.type = type;
	}
	
	/*
	 * Map of implemented interfaces
	 * Names are lower case for comparison
	 */
	protected Map<String,Class<?>> interfaces;
	
	/**
	 * Validate interface implementation by name
	 * @param interface name
	 * @return true=implemented, false=not implemented
	 */
	public boolean isImplemented(String name) {
		if (!isArgValid(name)) {
			return false;
		}
		return interfaces.containsKey(name.toLowerCase());
	}
	/**
	 * Validate interface implementation by class
	 * @param interface class
	 * @return true=implemented, false=not implemented
	 */
	public boolean isImplemented(Class<? extends OysterAction> action) {
		if (action == null) {
			return false;
		}
		for (Class<?> value : interfaces.values()) {
			if (value == action) {
				return true;
			}
		}
		return false;
	}
	
    /*
     * User friendly function name
     */
    protected String name;
	public String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}

	/*
	 * Save the configuration parameter string
	 */
	protected String parameters;
	public String getParameters() {
		return parameters;
	}
	protected void setParameters(String parameters) {
		this.parameters = parameters;
	}


	/*
	 * This function is defined with reverse logic (ie. !SCAN(...)
	 * Default is not reversed
	 */
	private boolean reverseLogic;
	public boolean isReversed() {
		return reverseLogic;
	}
	protected void setReverseLogic(boolean reverseLogic) {
		this.reverseLogic = reverseLogic;
	}
	
	/**
	 * Default constructor
	 * 
	 * Type and Name inference is performed in the constructor for cases where a 
	 * new instance of the class is created directly rather than through the builder.  
	 * There is a bit of extra overhead since the builder already knows the 
	 * FunctionType and name but it ensures complete initialization.
	 */
	protected OysterFunction() {
		// Map the supported interfaces
		this.interfaces = new HashMap<String,Class<?>>();
		for (Class<?> action : getClass().getInterfaces()) {
			interfaces.put(action.getSimpleName().toLowerCase(), action);
		}
		// Infer the type from the class
		this.type = FunctionType.getByClassName(this.getClass().getName());
		// Infer the name from the type
		this.name =  (this.type != null) ? this.type.getFunctionName() : "";
		this.parameters = "";
		this.reverseLogic = false;
	}
	
	/**
	 * Default configuration function just stores the string
	 * 
	 * @param parameters string from configuration
	 */
	protected void configure(String parameters) {
		this.parameters = parameters;
		if (!StringUtils.isBlank(parameters)) {
			 throw new IllegalArgumentException(String.format("Function %1$s does not take parameters", this.name));
		 }
	}
	
	/**
	 * Parse function parameter string into an array of arguments
	 * This uses the string tokenizer class
	 *  Arguments are delimited by comma
	 *  Embedded quote is single quote
	 *  Spaces are ignored unless they are inside quotes
	 * 
	 * @param parameters string from configuration
	 * @return Array of argument strings or an empty array if the parameter string is null or empty
	 */
	protected String[] parseArgs(String parameters) {
		if (!isArgValid(parameters)) {
			return new String[0];
		}
		return new StrTokenizer(parameters)
				.setDelimiterChar(',')
				.setQuoteChar('\'')
				.setIgnoredChar(' ')
		        .getTokenArray();
	}
	
	/**
	 * Encapsulate parameter validation for most cases.
	 * This ensures that the logic is consistent.
	 * The child classes can override this method as needed
	 * 
	 * Invalid arguments are:
	 * 	null reference
	 *  empty or all spaces (blank)
	 * 
	 * @param args 1-n arguments to be validated
	 * @return true=all args are valid, false=any arg is not valid
	 */
	public boolean isArgValid(String... args) {
		// Test each argument
		for (String arg : args) {
			// Null or blank arguments are invalid
			if (StringUtils.isBlank(arg)) {
				return false;
			}
		}
		// All arguments are valid
		return true;
	}
	
	/**
	 * Helper function to invert a value depending on whether the function
	 * is set to use reverse logic
	 * 
	 * @param input
	 * @return the input value or the inverted value depending on
	 * whether the function is defined with reverse logic ('!' prefix)
	 */
	public boolean rightAnswer(boolean input) {
		if (reverseLogic) {
			return !input;
		} else {
			return input;
		}
	}
	
	/**
	 * Helper function to format an error message prefixed with the function name
	 * 
	 * @param message to format
	 * @return FUNCTION-NAME and message
	 */
	public String formatMessage(String message) {
		return new StringBuilder()
				.append(getName())
				.append(" ")
				.append(message)
				.toString();
	}
	
	@Override
	public String toString() {
    	return new ToStringBuilder(this,ToStringStyle.SHORT_PREFIX_STYLE)
    			.append("Type",this.type)
    			.append("Name",this.name)
    			.append("Parameters", StringUtils.wrap(this.parameters,"\""))
    			.append("Negated", this.reverseLogic)
    			.append("Interfaces", interfaces.keySet())
    			.toString();
	}
}
