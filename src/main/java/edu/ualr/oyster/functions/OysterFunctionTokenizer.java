package edu.ualr.oyster.functions;

import org.apache.commons.lang3.StringUtils;

public abstract class OysterFunctionTokenizer extends OysterFunction implements Tokenize {

	/*
	 * Delimiter between lists elements (key,value pairs)
	 */
	protected String listDelimiters;

	public String getListDelimiters() {
		return listDelimiters;
	}

	public void setListDelimiters(String listDelimiters) {
		if (!isArgValid(listDelimiters)) {
			this.listDelimiters = "|";
			logger.info("Using default List Delimiters: {}", this.listDelimiters);
			return;
		}
		this.listDelimiters = listDelimiters;
	}

	/*
	 * Delimiter between key-value in list element pairs
	 */
	protected String pairDelimiters;

	public String getPairDelimiters() {
		return pairDelimiters;
	}

	public void setPairDelimiters(String pairDelimiters) {
		if (!isArgValid(pairDelimiters)) {
			this.pairDelimiters = ":";
			logger.info("Using default Pair Delimiters: {}", this.listDelimiters);
			return;
		}
		this.pairDelimiters = pairDelimiters;
	}

	/*
	 * Minimum token length
	 */
	protected int minimumLength;

	public int getMinimumLength() {
		return minimumLength;
	}

	public void setMinimumLength(String minimumLength) {
		try {
			setMinimumLength(Integer.parseInt(StringUtils.trim(minimumLength)));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format(
					"%1$s Minimum Length value %2$s is not a valid integer value", this.getName(), minimumLength));
		}
	}

	public void setMinimumLength(int minimumLength) {
		if (minimumLength < 1) {
			throw new IllegalArgumentException(String.format("%1$s Minimum Length value %2$d must be breater than 0",
					this.getName(), minimumLength));
		}
		this.minimumLength = minimumLength;
	}

	/*
	 * Stop word list
	 * These words are excluded from the list
	 */
	protected final ParamWordlist stopWords;

	public ParamWordlist getStopWords() {
		return stopWords;
	}

	/**
	 * no-argument constructor
	 * Initializes the stop words map
	 */
	public OysterFunctionTokenizer() {
		super();
		stopWords = new ParamWordlist();
	}

	/**
	 * Child class must implement the tokenize method at a minimum
	 */
	@Override
	public abstract String[] tokenize(String arg);

}
