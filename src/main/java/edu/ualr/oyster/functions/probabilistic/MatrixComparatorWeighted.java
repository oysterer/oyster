/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.functions.probabilistic;

import java.util.ArrayList;
import java.util.Locale;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;
import edu.ualr.oyster.resource.TokenWeightTableResource;

/**
 * Matrix Weighted takes two strings, parses the strings into a list of tokens
 * according to a regular expression given by the user. After removing any
 * excluded tokens given by the user, all pairs of tokens between the two lists
 * are compared using using the Levenshtein edit distance function. After
 * scoring all pairs, the highest scores for each row and column are used in a
 * weighted averaged. If the weighted average is greater than or equal to the
 * threshold parameter, the function returns true, otherwise false. Matrix takes
 * only one control parameter 1) Threshold value between 0.00 and 1.00
 */
public class MatrixComparatorWeighted extends OysterFunctionProbabilistic implements Compare, Score {

	private final LevenshteinEditDistance function;
	private TokenWeightTableResource tokenWeightTable;

	/**
	 * Constructor allocates Levenshtein function
	 */
	public MatrixComparatorWeighted() {
		super();
		this.function = new LevenshteinEditDistance();
		this.tokenWeightTable = null;
	}

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		String[] tokens = parseArgs(parameters);
		if ((tokens.length < 1) || (tokens.length > 2)) {
			throw new IllegalArgumentException(
					this.getName() + " parameters should be (Threshold [TokenWeightTableFilename)");
		}
		// Threshold cannot be optional since it is the first parameter
		setThreshold(tokens[0]);
		// Get the weight table
		if (tokens.length < 2) {
			// Use default file name
			this.tokenWeightTable = TokenWeightTableResource.getInstance();
		} else {
			// Use specified file name
			this.tokenWeightTable = TokenWeightTableResource.getInstance(tokens[1]);
		}
	}

	@Override
	public float score(String arg1, String arg2) {
		float result = 0.0f;

		// Tokenize arguments
		ArrayList<String> list1 = getTokens(arg1);
		int len1 = list1.size();
		ArrayList<String> list2 = getTokens(arg2);
		int len2 = list2.size();

		// in case either or both token arrays are empty return 0.0
		if (len1 < 1 || len2 < 1) {
			return result;
		}

		// if both strings have values, generate matrix of LED values
		float[][] matrix = new float[len1][len2];
		int trials = Math.min(len1, len2);
		float matrixMax = 0.0f;
		// Collect all words with exact agreement between strings
		// words.clear();
		for (int i = 0; i < len1; i++) {
			for (int j = 0; j < len2; j++) {
				String token_i = list1.get(i);
				String token_j = list2.get(j);
				float dist = (float) function.score(token_i, token_j);
				float weight_i = tokenWeightTable.getTokenWeight(token_i);
				float weight_j = tokenWeightTable.getTokenWeight(token_j);
				float weight = Math.min(weight_i, weight_j);
				float adjWgt = weight * dist;
				matrix[i][j] = adjWgt;
				if (matrixMax < adjWgt) {
					matrixMax = adjWgt;
				}
			}
		}
		// if greatest cell value is less than threshold, then stop here
		if (matrixMax < this.getThreshold()) {
			return matrixMax;
		}

		float matrixTotal = 0.0f;
		// start process the average the largest value in each row and column
		int loopCnt = 0;
		for (int k = 0; k < trials; k++) {
			// search for largest value in the matrix
			float maxValue = 0.0f;
			// remember cell of largest values
			int save_i = 0;
			int save_j = 0;
			// start search
			// search_loop:
			for (int i = 0; i < len1; i++) {
				for (int j = 0; j < len2; j++) {
					if (matrix[i][j] > maxValue) {
						maxValue = matrix[i][j];
						save_i = i;
						save_j = j;
					}
				}
			}
			// only continue searching matrix if the last maximum value found is positive
			if (maxValue <= 0.001)
				break;
			loopCnt++;
			matrixTotal = matrixTotal + maxValue;
			// remove this row and column from further consideration
			for (int i = 0; i < len1; i++) {
				matrix[i][save_j] = -1.0f;
			}
			for (int j = 0; j < len2; j++) {
				matrix[save_i][j] = -1.0f;
			}
			// for (int i=0; i<len1; i++){
			// for (int j=0; j<len2; j++){
			// if (j==0) System.out.format("%n%.6f ", matrix[i][j]); else
			// System.out.format("%.6f ", matrix[i][j]);
			// }
			// }
		}
		// calculate matrix average
		if (loopCnt > 0) {
			result = matrixTotal / (float) (loopCnt + 1);
		}
		return result;
	}

	private ArrayList<String> getTokens(String arg) {
		// Null or empty returns empty list
		if (!isArgValid(arg)) {
			return new ArrayList<String>();
		}
		arg = arg.toUpperCase(Locale.US);
		String[] temp = arg.split("[\\W+]");
		ArrayList<String> cleanList = new ArrayList<String>();
		for (int k = 0; k < temp.length; k++) {
			String token = temp[k];
			if (token.length() > 0) {
				cleanList.add(token);
			}
		}
		return cleanList;
	}

}
