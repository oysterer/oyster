/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * This is a sample probabilistic function 
 * 
 * The only method that must be coded is score(String, String)
 * 
 * If no special initialization, configuration or compare logic is
 * required those should be removed from the class so that the
 * corresponding methods in the parent class will be used. 
 * 
 * @author jatrue
 *
 */
public class _SampleProbabilistic extends OysterFunctionProbabilistic implements Compare, Score {

	// Define the external algorithm class if needed
	//private final LevenshteinDistance function;

	/*
	 * Constructor
	 * 
	 * If not initialization is required other than invoking the super constructor
	 * then don't code this constructor
	 */
	public _SampleProbabilistic() {
		// Always call the superclass constructor
		super();
		// Instantiate the external algorithm class if needed
		//this.function = new LevenshteinDistance();
	}
	
	/*
	 * Configuration method
	 * If the only parameter is the threshold and it is required
	 * then do not code this method, let the parent class handle it
	 */
	public void configure(String parameters) {

		/*
		 * validate arguments is not null or blank
		 */
		if (!isArgValid(parameters)) {
			return;
		}

		/*
		 * Use the argument parser in the base OysterFunction class
		 */
		String[] args = parseArgs(parameters);
		
		/*
		 * Validate and store the configuration parameters.
		 * 
		 */
		if (args.length != 2) {
			throw new IllegalArgumentException(
					this.getName() + " requires two parameters: x and y");
		}
	}


	/**
	 * Simple compare method that calls the score method and
	 * then compares the returned score tot he threshold.
	 * 
	 * The default implementation also validates the score range
	 * so if you don't need any special logic then don't code this method
	 */
	public boolean compare(String str1, String str2) {
		float result = score(str1,str2);
		return rightAnswer(testScore(result));
	}
	

	/**
	 * This method must be coded for each function
	 * It computes the normalized score for the distance between the to arguments
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return normalized score = 1.0 - distance / maximum operand length
	 */
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		float result = 0.0f;
		
		/*
		 * Add the logic here that computes the normalized score 0.0 - 1.0
		 */
		
		return result;
	}


}
