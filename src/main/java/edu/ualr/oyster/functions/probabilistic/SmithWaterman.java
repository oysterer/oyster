/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.Arrays;
import java.util.Locale;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.ParamInt;
import edu.ualr.oyster.functions.Score;

/**
 * The Smith–Waterman algorithm is a well-known algorithm for performing local
 * sequence alignment; that is, for determining similar regions between two
 * nucleotide or protein sequences. Instead of looking at the total sequence,
 * the Smith–Waterman algorithm compares segments of all possible lengths and
 * optimizes the similarity measure.
 * 
 * (Excerpt taken from Wikipedia)
 * 
 * References:
 * <ul>
 * <li>Smith TF, Waterman MS. Identification of common molecular subsequences. J
 * Mol Biol. 1981 Mar 25;147(1):195-7.</li>
 * <li>Gotoh O. An improved algorithm for matching biological sequences. J Mol
 * Biol. 1982 Dec 15;162(3):705-8.</li>
 * </ul>
 * 
 * @see http://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm
 * @see http://www.clcbio.com/index.php?id=1046 Created on Jun 20, 2012 9:44:13
 *      PM
 * @author Eric. D Nelson
 */
public class SmithWaterman extends OysterFunctionProbabilistic implements Compare, Score {

	/*
	 * Extend ParamInt class to name these parameters
	 */
	private class Match extends ParamInt {}
	private final Match match;

	private class Mismatch extends ParamInt {}
	private final Mismatch mismatch;

	private class Gap extends ParamInt {}
	private final Gap gap;
	
	public SmithWaterman() {
		// Always call the parent function constructor
		super();
		
		// Instantiate the parameter properties
		this.match = new Match();
		this.mismatch = new Mismatch();
		this.gap = new Gap();
	}
	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = parseArgs(parameters);
		if (tokens.length < 3) {
			throw new IllegalArgumentException(this.getName() + " parameters should be (match,mismatch,gap)");
		}
		match.setValue(tokens[0]);
		mismatch.setValue(tokens[1]);
		gap.setValue(tokens[2]);
		
		// Threshold is optional and a property of the parent class
		if (tokens.length == 4) {
			setThreshold(tokens[3]);
		}
	}

	@Override
	public float score(String str1, String str2) {
		// validate strings
		if (!isArgValid(str1)) {
			str1 = "";
		}
		if (!isArgValid(str2)) {
			str2 = "";
		}

		// Determine the longest string
		float maxLength = Math.max(str1.length(), str2.length());

		// Compute the SmithWaterman distance
		float distance = distance(str1, str2);

		return (1.0f - (distance / (maxLength * distance)));
	}

	/*
	 * Computation of SmithWaterman distance 
	 * intput strings are already validated by the caller
	 */
	private float distance(String str1, String str2) {
		float result = 0;
		String sTemp = str1.toUpperCase(Locale.US);
		String tTemp = str2.toUpperCase(Locale.US);

		/** Workspace */
		float[][] matrix = null;

		/** Resulting Alignments */
		String sequence1 = "";
		String sequence2 = "";

		// create the matrix
		matrix = new float[tTemp.length() + 1][sTemp.length() + 1];

		// fill it with zeros
		for (int i = 0; i < tTemp.length(); i++) {
			Arrays.fill(matrix[i], 0.0f);
		}

		// compute the value for each cell
		int[] start = new int[2];
		float check = 0;
		for (int i = 1; i <= tTemp.length(); i++) {
			for (int j = 1; j <= sTemp.length(); j++) {
				// do the characters match?
				if (tTemp.charAt(i - 1) == sTemp.charAt(j - 1)) {
					matrix[i][j] = matrix[i - 1][j - 1] + match.getValue();
				} else {
					float diag = matrix[i - 1][j - 1] + mismatch.getValue();
					float top = matrix[i - 1][j] + gap.getValue();
					float left = matrix[i][j - 1] + gap.getValue();

					float max = Math.max(diag, top);
					max = Math.max(max, left);
					max = Math.max(max, 0);

					matrix[i][j] = max;
				}

				// Check for the max
				if (matrix[i][j] > check) {
					start[0] = i;
					start[1] = j;
					check = matrix[i][j];
				}
			}
		}

		sequence1 = sequence2 = "";
		// backtrack for alignment
		int i = start[0];
		int j = start[1];

		while (true) {
			float current = matrix[i][j];

			if (current <= 0) {
				break;
			}

			float diag = matrix[i - 1][j - 1];
			float top = matrix[i - 1][j];
			float left = matrix[i][j - 1];

			// diag - the letters from two sequences are aligned
			// left - a gap is introduced in the left sequence
			// top - a gap is introduced in the top sequence
			if (diag >= top && diag >= left) {
				sequence1 = sTemp.charAt(j - 1) + sequence1;
				sequence2 = tTemp.charAt(i - 1) + sequence2;
				i--;
				j--;
			} else if (left >= diag && left >= top) {
				sequence1 = sTemp.charAt(j - 1) + sequence1;
				sequence2 = "-" + sequence2;
				j--;
			} else if (top >= diag && top >= left) {
				sequence1 = "-" + sequence1;
				sequence2 = tTemp.charAt(i - 1) + sequence2;
				i--;
			}
		}
		result = matrix[start[0]][start[1]];
		return result;
	}
}
