/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.functions.probabilistic;

import java.util.ArrayList;
import java.util.Locale;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;
import edu.ualr.oyster.functions.ParamWordlist;

/**
 * Matrix comparator takes two strings, parses the strings into a list of tokens
 * according to a regular expression given by the user. After removing any
 * excluded tokens given by the user, all pairs of tokens between the two lists
 * are compared using using the Levenshtein edit distance function. After
 * scoring all pairs, the highest scores for each row and column are averaged.
 * If the average is greater than or equal to the user specified threshold
 * value, the function returns true, otherwise false. Matrix takes three control
 * parameters 1) Threshold value between 0.00 and 1.00, 2) Regular expression
 * for tokenizing the strings 3) Optional list of excluded tokens
 */
public class MatrixComparator extends OysterFunctionProbabilistic implements Compare, Score {

	/*
	 * Extend ParamWordlist to name it for messages
	 */
	private class StopWords extends ParamWordlist {}
	private StopWords stopWords;
	
	private final LevenshteinEditDistance ledScore;

	public MatrixComparator() {
		this.stopWords = new StopWords();
		ledScore = new LevenshteinEditDistance();
	}

	/**
	 * Override configure method to handle Stop Words
	 */
	@Override
	protected void configure(String parms) {
		String[] temp = parseArgs(parms);
		// The first parameter must define a match threshold in the interval [0, 1]
		if (temp.length < 1) {
			throw new IllegalArgumentException(formatMessage("must define a threshold value in interval [0.0, 1.0]"));
		} 
		try {
			setThreshold(temp[0]);
		} catch (IllegalArgumentException ex) {
			throw new IllegalArgumentException(formatMessage(ex.getMessage()));
		}
		setThreshold(temp[0]);
		// Second parameter (Optional) list of stop words separated by pipe (|) character
		if (temp.length > 1) {
			stopWords.setWords(temp[1]);
		}
	}

	/**
	 * Compute the score
	 */
	@Override
	public float score(String arg1, String arg2) {
		float matrixScore = 0.0f;
		ArrayList<String> list1 = makeTokens(arg1);
		int len1 = list1.size();
		ArrayList<String> list2 = makeTokens(arg2);
		int len2 = list2.size();
		// in case either or both strings are empty return false
		if (len1 < 1 || len2 < 1) {
			return matrixScore;
		}
		// if both strings have values, generate matrix of LED values
		float[][] matrix = new float[len1][len2];
		int trials = Math.min(len1, len2);
		float matrixMax = 0.0f;
		// Collect all words with exact agreement between strings
		// words.clear();
		for (int i = 0; i < len1; i++) {
			for (int j = 0; j < len2; j++) {
				float dist = ledScore.score(list1.get(i), list2.get(j));
				matrix[i][j] = dist;
				if (matrixMax < dist)
					matrixMax = dist;
				// if (dist==1.0) words.add(list1.get(i));
			}
		}
		
		/*
		 * if greatest LED score is less than threshold, then stop now
		 * Normally only compare used threshold but this variation
		 * improves performance when there are not matches 
		 */
		if (!testScore(matrixMax)) {
			return matrixScore;
		}
		
		// start process the average the largest value in each row and column
		float matrixTotal = 0.0f;
		for (int k = 0; k < trials; k++) {
			// search for largest value in the matrix
			float maxValue = 0.0f;
			// remember cell of largest values
			int savei = 0;
			int savej = 0;
			// start search
			search_loop: for (int i = 0; i < len1; i++) {
				for (int j = 0; j < len2; j++) {
					if (matrix[i][j] > maxValue) {
						maxValue = matrix[i][j];
						savei = i;
						savej = j;
						// if max value is 1.0 no need to search further
						if (maxValue == 1.0f)
							break search_loop;
					}
				}
			}
			// only continue searching matrix if the last maximum value found is positive
			if (maxValue <= 0.0)
				break;
			matrixTotal = matrixTotal + maxValue;
			// remove this row and column from further consideration
			for (int i = 0; i < len1; i++) {
				matrix[i][savej] = -1.0f;
			}
			for (int j = 0; j < len2; j++) {
				matrix[savei][j] = -1.0f;
			}
		}
		// calculate matrix average
		matrixScore = matrixTotal / (float) trials;
		return matrixScore;
	}

	private ArrayList<String> makeTokens(String inStr) {
		inStr = inStr.toUpperCase(Locale.US);
		String[] temp = inStr.split("[\\W]");
		ArrayList<String> cleanList = new ArrayList<String>();
		if (temp.length == 0)
			return cleanList;
		for (int k = 0; k < temp.length; k++) {
			String token = temp[k];
			if (token.length() > 0) {
				token = token.toUpperCase(Locale.US);
				if (!stopWords.isWord(token))
					cleanList.add(token);
			}
		}
		return cleanList;
	}

}
