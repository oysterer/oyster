/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import org.apache.commons.text.similarity.LevenshteinDistance;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/*
 * LED generates normalized score between two strings and compares to 
 * a user-defined threshold. If score is greater than or equal to the
 * threshold it returns true, otherwise false
 * LED implements the compare interface
 */
public class LevenshteinEditDistance extends OysterFunctionProbabilistic implements Compare, Score {

	private final LevenshteinDistance function;

	/*
	 * Constructor
	 * Create the LevenshteinDistance object to do the work
	 */
	public LevenshteinEditDistance() {
		super();
		this.function = new LevenshteinDistance();
	}

	/**
	 * Computes the normalized score
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return normalized score = 1.0 - distance / maximum operand length
	 */
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		// Because strings have been validates neither are zero length
		float maxLength = Math.max(str1.length(), str2.length());
		float distance = function.apply(str1, str2);
		return (1.0f - (distance / maxLength));
	}

}
