/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;
import info.debatty.java.stringsimilarity.Damerau;

/*
* Similar to Levenshtein, Damerau-Levenshtein distance with transposition 
* (also sometimes calls unrestricted Damerau-Levenshtein distance) is the 
* minimum number of operations needed to transform one string into the other, 
* where an operation is defined as an insertion, deletion, or substitution 
* of a single character, or a transposition of two adjacent characters.
* It does respect triangle inequality, and is thus a metric distance.
* This is not to be confused with the optimal string alignment distance, which
* is an extension where no substring can be edited more than once.
*/
public class DamerauLevenshtein extends OysterFunctionProbabilistic implements Compare, Score {

	private final Damerau function;

	/*
	 * Constructor
	 * Create the Damerau object to do the work
	 */
	public DamerauLevenshtein() {
		super();
		this.function = new Damerau();
	}
	
	/**
	 * Computes the normalized score
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return normalized score = 1.0 - distance / maximum operand length
	 */
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		// Because strings have been validated neither are zero length
		float maxLength = Math.max(str1.length(), str2.length());
		Double distance = function.distance(str1, str2);
		return (1.0f - (distance.floatValue() / maxLength));
	}

}
