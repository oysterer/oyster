/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * 
 * ListOverlapPV takes as input two lists of property-value pairs represented by
 * strings, parses each string into a list of pairs by splitting on the list
 * delimiter character, then splitting each pair by the pair delimiter. After
 * removing empty items, duplicate items, items with missing property values,
 * and items where the property value is a placeholder, the function determines
 * the number of pairs in common between the two lists. The overlap ratio is
 * calculated as the number of common items divided by the number of items in
 * the longer of the two lists. If this ratio is greater than or equal to the
 * threshold value, the function returns true, otherwise false.
 *
 */
public class ListOverlapPV extends OysterFunctionProbabilistic implements Compare, Score {

	/**
	 * Delimiter between lists elements (key,value pairs)
	 */
	private String listDelimiter;

	public String getListDelimiter() {
		return listDelimiter;
	}

	public void setListDelimiter(String listDelimiter) {
		if (!isArgValid(listDelimiter)) {
			this.listDelimiter = "|";
			logger.info("Using default List Delimiter: {}",this.listDelimiter);
			return;
		}
		this.listDelimiter = listDelimiter;;
	}

	/**
	 * Key,Value pair delimiter
	 */
	private String pairDelimiter;

	public String getPairDelimiter() {
		return pairDelimiter;
	}

	public void setPairDelimiter(String pairDelimiter) {
		if (!isArgValid(pairDelimiter)) {
			this.pairDelimiter = ":";
			logger.info("Using default Pair Delimiter: {}",this.pairDelimiter);
			return;
		}
		this.pairDelimiter = pairDelimiter;
//		if (invalid value test)) {
//			throw new IllegalArgumentException(this.getName() + " listDelimiter must be ???");
//		}
	}

	private final LinkedHashSet<String> placeHolderValues;
	
	public void setPlaceHolderValues(String placeHolderValues) {
		String[] myArray = placeHolderValues.split("[|]");
		this.placeHolderValues.addAll(Arrays.asList(myArray));
	}

	public boolean isPlaceHolder(String item) {
		if (placeHolderValues.contains(item))
			return true;
		return false;
	}
	
	public ListOverlapPV() {
		super();
		placeHolderValues = new LinkedHashSet<String>();
	}
	
	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = parseArgs(parameters);
		if (tokens.length != 4) {
			throw new IllegalArgumentException(this.getName() + " parameters should be (threshold, listDelimiter, pairDelimiter, placeHolderList)");
		}
		// Threshold cannot be optional since it is the first parameter
		setThreshold(tokens[0]);
		setListDelimiter(tokens[1]);
		setPairDelimiter(tokens[2]);
		setPlaceHolderValues(tokens[3]);
	}

	/**
	 * Compute the score of the two lists
	 */
	@Override
	public float score(String arg1, String arg2) {
		float result = 0.0f;
		String[] list1;
		String[] list2;
		int len1, len2, max, overlap;

		list1 = convertStringToListPV(arg1);
		len1 = list1.length;
		list2 = convertStringToListPV(arg2);
		len2 = list2.length;

		if (len1 > len2)
			max = len1;
		else
			max = len2;
		LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
		hashSet.addAll(Arrays.asList(list1));
		hashSet.retainAll(Arrays.asList(list2));
		overlap = hashSet.size();
		if (max > 0)
			result = (float) overlap / (float) max;
		return result;
	}

	private String[] convertStringToListPV(String inStr) {

		String[] listRaw = inStr.split("[" + listDelimiter + "]");
		LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
		// Start loop to check for valid, non-duplicate property-value pairs
		String property = "";
		String value = "";
		for (int i = 0; i < listRaw.length; i++) {
			String pair = (listRaw[i].trim()).toUpperCase(Locale.US);
			if (pair.length() > 0) {
				int splitPoint = pair.indexOf(pairDelimiter);
				if (splitPoint > 0) {
					property = (pair.substring(0, splitPoint)).trim();
					if (splitPoint < pair.length()) {
						value = (pair.substring(splitPoint + 1)).trim();
						if (value.length() > 0 && !isPlaceHolder(value)) {
							pair = property + pairDelimiter + value;
							hashSet.add(pair);
						}
					}
				}
			}
		}
		String[] listClean = hashSet.toArray(new String[0]);
		return listClean;
	}
}
