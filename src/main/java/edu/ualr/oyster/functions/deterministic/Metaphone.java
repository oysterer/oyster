/*
 * Copyright 2013-2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * Metaphone is a phonetic algorithm, an algorithm published in 1990 for 
 * indexing words by their English pronunciation. The algorithm should be 
 * limited to English only.
 */
public class Metaphone extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {
	
	private final org.apache.commons.codec.language.Metaphone codec;
	
	public Metaphone() {
		super();
		this.codec = new org.apache.commons.codec.language.Metaphone();
	}

	/*
	 * Metaphone provides its own comparator method 
	 */
	public boolean compare(String str1, String str2){
        // Ensure there is something to compare
        if (!isArgValid(str1,str2)) {
        	return rightAnswer(false);
        }
        return rightAnswer(codec.isMetaphoneEqual(str1, str2));
    }
    public String transform(String str) {
        // Null or empty string
        if (StringUtils.isBlank(str)) {
        	return DEFAULT_TRANSFORM_RESULT;
        }

        String result = codec.encode(str);
        return result;
    }
}
