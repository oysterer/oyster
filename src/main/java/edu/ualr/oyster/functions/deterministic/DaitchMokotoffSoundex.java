/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * Daitch–Mokotoff Soundex also called the Jewish Soundex or Eastern European 
 * Soundex, was designed to allow greater accuracy in the matching of Slavic and 
 * Yiddish surnames which have similar pronunciation but different spellings.
 * @see <a href="http://en.wikipedia.org/wiki/Daitch%E2%80%93Mokotoff_Soundex" > The Soundex </a>
 * @author Eric D. Nelson
 * Created on May 16, 2010
 */
public class DaitchMokotoffSoundex extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

	// use folding encoding
	private boolean folding;
	public boolean isFolding() {
		return folding;
	}
	protected void setFolding(boolean folding) {
		if (this.folding != folding) {
			if (isFolding()) {
				this.folding = false;
			} else {
				this.folding = true;
			}
			this.codec = new org.apache.commons.codec.language.DaitchMokotoffSoundex(this.folding);
		}
	}

	// Folding mode
	private enum Folding {
		FOLDING, NOFOLDING;

		public static Folding getByName(String name) {
			for (Folding entry : Folding.values()) {
				if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
					return entry;
				}
			}
			return null;
		}
	}
	
	// This isn't final because it may need to be replaced in the configure method
	private org.apache.commons.codec.language.DaitchMokotoffSoundex codec;

    /**
     * Creates a new instance of DaitchMokotoffSoundex
     */
    public DaitchMokotoffSoundex() {
    	super();
    	this.folding = true;
		this.codec = new org.apache.commons.codec.language.DaitchMokotoffSoundex(this.folding);
    }

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Save the parameters
		this.parameters = parameters;

		// No arguments is allowed
		if (!isArgValid(parameters)) {
			return;
		}

		// Only one argument token is allowed
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length > 1) {
			throw new IllegalArgumentException(
					this.getName() + " the parameter should be one of: " + getOptions(Folding.class));
		}

		// Validate and get the mode
		Folding folding = Folding.getByName(tokens[0]);
		if (folding == null) {
			throw new IllegalArgumentException(
					this.getName() + " folding: " + tokens[0] + " invalid, use: " + getOptions(Folding.class));
		}
		
		// If the requested folding mode is not currently selected, reallocate the codec
		switch (folding) {
		case NOFOLDING:
			setFolding(false);
			break;
		case FOLDING:
		default:
			setFolding(true);
		}
	}
	
    /**
     * This method determines the Daitch–Mokotoff Soundex hash for the input 
     * String. This method potentially returns two differently encodings this is
     * due to some names having multiple possible pronunciations.
     * @param str The input string
     * @return a String array of length 2 containing the Daitch–Mokotoff encoding
     */
    public String transform(String str) {
    	if (!isArgValid(str)) {
    		return DEFAULT_TRANSFORM_RESULT;
    	}
    	return codec.encode(str);
    }
	
    /**
     * This method determines the Daitch–Mokotoff Soundex hash for the input 
     * String. This method potentially returns two differently encodings this is
     * due to some names having multiple possible pronunciations.
     * @param s The input string
     * @return a String array of length 2 containing the Daitch–Mokotoff encoding
     */
    public String[] tokenize(String str) {
    	if (!isArgValid(str)) {
    		return new String[0];
    	}
    	return StringUtils.split(codec.soundex(str),'|');
    }
}
