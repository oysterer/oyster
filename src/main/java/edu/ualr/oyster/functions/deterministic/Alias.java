/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.resource.AliasTableResource;

/*
 * Alias loads the specified reference file into the alias table.
 * The key is a String, and value is a list of Strings.
 * <p>
 * When it compares two strings, it first checks to see if the
 * first string is a key. If it is a key, then it checks to see if
 * the second string is in the list for that key.
 * If the first string is not a key, then it checks to see if the
 * second string is a key. If it is, the checks to see if the
 * first string is in the list for that key.
 * <p>
 * If either condition is satisfied, the comparison is true,
 * otherwise it is false.
 */
public class Alias extends OysterFunctionDeterministic implements Compare {

	// Alias table
	private HashMap<String, ArrayList<String>> aliasTable;
	
	// Reference instance
	private AliasTableResource reference;
	
	public Alias() {
		super();
		this.aliasTable = null;
		this.reference = null;
	}
	
	/**
	 * This class has to get the singleton reference in the configure 
	 * method so that OysterFunction can pass the parameter with the file name
	 */
	@Override
	protected void configure(String parameters) {
		if (!isArgValid(parameters)) {
			 throw new IllegalArgumentException(String.format("An alias file pathname must be supplied"));
		 }
		this.reference = AliasTableResource.getInstance(parameters); 
		this.aliasTable = reference.getAliasTable();
	}
	
	/**
	 * Get the pathname specified in the parameters
	 * THis is stored in the base reference class
	 * 
	 * @return the pathname of the alias file
	 */
	public String getPathname() {
		return reference.getPathname();
	}

	public boolean compare(String str1, String str2) {
		if (!isArgValid(str1,str2)) {
			return rightAnswer(false);
		}
		str1 = str1.toUpperCase(Locale.US);
		str2 = str2.toUpperCase(Locale.US);
		ArrayList<String> valueList;
		if (aliasTable.containsKey(str1)) {
			valueList = aliasTable.get(str1);
			if (valueList.contains(str2))
				return rightAnswer(true);
		}
		if (aliasTable.containsKey(str2)) {
			valueList = aliasTable.get(str2);
			if (valueList.contains(str1))
				return rightAnswer(true);
		}
		return rightAnswer(false);
	}

	@Override
	public String transform(String arg) {
		throw new IllegalStateException("Transform function is not implemented");
	}

	@Override
	public String[] tokenize(String arg) {
		throw new IllegalStateException("Tokenize function is not implemented");
	}

}
