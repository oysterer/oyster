/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/*
 * Exact does not alter any string values
 * It implements compare and transform and tokenize methods
 * Note: 
 * Class variables parameterList and reverseLogic are inherited from OysterFunction
 * Class methods isNullOrEmpty() and rightAnswer() are inherited from OysterFunction
 */
public class Exact extends OysterFunctionDeterministic  implements Compare, Transform, Tokenize {
	
	public String transform(String str) {
		if (!isArgValid(str)) {
			return DEFAULT_TRANSFORM_RESULT;
		}
		return str;
	}
}
