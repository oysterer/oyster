/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import java.util.Arrays;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * Conversion of Scan utility to an OysterFunction
 * 
 * To modify the configuration parameters, modify the relevant enumeration
 *
 * Created on Jun 9, 2012 12:12:49 AM
 * @author Eric D. Nelson
 */
public class Scan extends OysterFunctionDeterministic implements Transform, Compare, Tokenize {


	private enum Direction {
		LR, 
		RL;
		
	   	public static Direction getByName(String name) {
    		for (Direction entry : Direction.values()) {
    			if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}
	}

	private enum CharType {
        ALL,
        NONBLANK,
        ALPHA,
        LETTER,
        DIGIT,
        CHINESEDIGIT,
        CHINESEALPHA,
        CHINESELETTER,
        CHINESE;
		
    	public static CharType getByName(String name) {
    		for (CharType entry : CharType.values()) {
    			if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}
	}
	
	private enum Casing {
		TOUPPER,
		TOLOWER,
		KEEPCASE;
		
    	public static Casing getByName(String name) {
    		for (Casing entry : Casing.values()) {
    			if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}
	}
	
	private enum Order {
        L2HKEEPDUP,
        SAMEORDER,
        L2HDROPDUP;
		
    	public static Order getByName(String name) {
    		for (Order entry : Order.values()) {
    			if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
    				return entry;
    			}
    		}
    		return null;
    	}
	}
	
	private Direction direction;
	public String getDirection() {
		if (direction != null) {
			return direction.name();
		} else {
			return "";
		}
	}
	private CharType charType;
	public String getCharType() {
		if (charType != null) {
			return charType.name();
		} else {
			return "";
		}
	}
	private int length;
	public int getLength() {
		return length;
	}
	private Casing casing;
	public String getCasing() {
		if (casing != null) {
			return casing.name();
		} else {
			return "";
		}
	}
	private Order order;
	public String getOrder() {
		if (order != null) {
			return order.name();
		} else {
			return "";
		}
	}
	
	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {
		
		// Tokenize on comma delimiter
		String [] tokens = StringUtils.split(parameters,", ");
		if (tokens.length != 5) {
			throw new IllegalArgumentException(this.getName() + " parameters should be (direction,charType,length,casing,order)");
		}

		// Validate and get the direction
		direction = Direction.getByName(tokens[0]);
		if (direction == null) {
			throw new IllegalArgumentException(this.getName() + " direction: " + tokens[0] + " invalid, use: " + getOptions(Direction.class));
		}

		// Validate and get the Character Type
		charType = CharType.getByName(tokens[1]);
		if (charType == null) {
			throw new IllegalArgumentException(this.getName() + " charType: " + tokens[1] + " invalid, use: " + getOptions(CharType.class));
		}
		
		// Parse the numeric length
		try {
		length = Integer.parseUnsignedInt(tokens[2]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(this.getName() + " length: " + tokens[2] + " invalid, must be unsigned integer");
			
		}
		
		// Validate and get the character casing
		casing = Casing.getByName(tokens[3]);
		if (casing == null) {
			throw new IllegalArgumentException(this.getName() + " character casing: " + tokens[3] + " invalid, use: " + getOptions(Casing.class));
		}
		
		// Validate and get the ordering
		order = Order.getByName(tokens[4]);
		if (order == null) {
			throw new IllegalArgumentException(this.getName() + " ordering: " + tokens[4] + " invalid, use: " + getOptions(Order.class));
		}
	}
    
	@Override
    public String transform(String token){
        String result = DEFAULT_TRANSFORM_RESULT;

        if (StringUtils.isBlank(token)) {
        	return result;
        }

        /*
         * Direction processing
         */
        switch (direction) {
        case LR: 
            result = token;
            break;
        case RL : 
            StringBuilder sb = new StringBuilder(token);
            sb.reverse();
            result = sb.toString();
            break;
        }

        /*
         * CharType processing
         * Because the configured type is already parsed there are no invalid values
         */
        switch (charType) {
        case ALL :
        	break;
        case NONBLANK :
            result = result.replaceAll("\\s", "").trim();
            break;
        case ALPHA : 
            result = result.replaceAll("\\W", "").trim();
            break;
        case LETTER :
            result = result.replaceAll("[^a-zA-Z]", "").trim();
            break;
        case DIGIT :
            result = result.replaceAll("\\D", "").trim();
            break;
        case CHINESEDIGIT :
              result = result.replaceAll("[^\u4e00-\u9fa5&&\\D]", "").trim();
              break;
        case CHINESEALPHA :
              result = result.replaceAll("[^\u4e00-\u9fa5&&\\W]", "").trim();
              break;
        case CHINESELETTER :
              result = result.replaceAll("[^\u4e00-\u9fa5&&[^a-zA-Z]]", "").trim();
              break;
        case CHINESE : 
              result = result.replaceAll("[^\u4e00-\u9fa5]", "").trim();
              break;
        }

        // Shorten the string if needed
        if (length != 0) {
            result = StringUtils.substring(result,0,length);
        }
        
        /*
         * Character casing
         */
        switch (casing) {
        case KEEPCASE : 
        	break;
        case TOUPPER :
        	result = result.toUpperCase(Locale.US);
        	break;
        case TOLOWER :
        	result = result.toLowerCase(Locale.US);
        	break;
        }
    
        /*
         * Final ordering and duplicate removal
         */
        switch (order) {
        case SAMEORDER :
            /*
             * if this was reversed by direction, changed it back to normal
             * TODO is this correct? Should it not respect direction?
             */
            if (Direction.RL.equals(direction)) {
                StringBuilder sb = new StringBuilder(result);
                sb.reverse();
                result = sb.toString();
            }
        	break;
    	case L2HKEEPDUP :
            result = order(result, false);
    		break;
    	case L2HDROPDUP :
            result = order(result, true);
    		break;
        }

        /*
         * Pad the string if it is too short after all other operations
         */
        if ((length != 0) && (result.length() < length)) {
	        switch (direction) {
	        case LR :
	        	result = StringUtils.rightPad(result, length,'*');
	        	break;
	        case RL :
	        	result = StringUtils.leftPad(result, length,'*');
	        	break;
	        }
        }
       return result;
    }
    
	/**
	 * sort order the characters in the string and optionally drop duplicates
	 * @param sto process
	 * @param drop duplicates true/false
	 * @return modified string
	 */
    private String order(String s, boolean drop){
        StringBuilder sb = new StringBuilder(100);
        char [] a = s.toCharArray();
        Arrays.sort(a);
        
        if (a.length > 0) {
            sb.append(a[0]);
            char check = a[0];
            for (int i = 1; i < a.length; i++){
                if (drop){
                    if (check != a[i]) {
                        sb.append(a[i]);
                    }
                } else {
                    sb.append(a[i]);
                }

                check = a[i];
            }
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
    	return super.toString() + 
    			new ToStringBuilder(this,ToStringStyle.NO_CLASS_NAME_STYLE)
    			.append("Direction",direction.name())
    			.append("CharType",charType.name())
    			.append("Length", length)
    			.append("Casing", casing.name())
    			.append("Order", order.name())
    			.toString();
    }
}
