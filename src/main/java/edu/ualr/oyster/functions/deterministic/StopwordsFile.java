/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.resource.StopwordListResource;

/*
 * StopwordsFile loads the specified reference file into the stopword list
 * <p>
 * Transform returns the parameter unchanged unless it is in the list then
 * an empty string is returned
 * <p>
 * Compare first transforms the two parameters using the stopword list
 * If either is transformed into an empty string, it returns false
 * since two stop words do not match
 * Then is does a normal string equals ignore case comparison
 */
public class StopwordsFile extends Stopwords implements Transform, Compare, Tokenize {
	
	// Reference instance
	private StopwordListResource reference;
	
	public StopwordsFile() {
		super();
		this.reference = null;
	}
	
	/**
	 * This class has to get the singleton reference in the configure 
	 * method so that OysterFunction can pass the parameter with the file name
	 */
	@Override
	protected void configure(String parameters) {
		if (StringUtils.isBlank(parameters)) {
			 throw new IllegalArgumentException(String.format("An alias file pathname must be supplied"));
		 }
		this.reference = StopwordListResource.getInstance(parameters); 
		addAllWords(reference.getStopwordList());
	}
	
	/**
	 * Get the pathname specified in the parameters
	 * THis is stored in the base reference class
	 * 
	 * @return the pathname of the alias file
	 */
	public String getPathname() {
		return reference.getPathname();
	}
}
