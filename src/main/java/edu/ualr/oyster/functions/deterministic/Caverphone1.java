/*
 * Copyright 2013-2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.codec.EncoderException;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * The Caverphone1 phonetic matching algorithm was created by David Hood in the 
 * Caversham Project at the University of Otago in New Zealand in 2002, revised 
 * in 2004. It was created to assist in data matching between late 19th century 
 * and early 20th century electoral rolls, where the name only needed to be in a
 * "commonly recognizable form". The algorithm was intended to apply to those 
 * names that could not easily be matched between electoral rolls, after the 
 * exact matches were removed from the pool of potential matches). The algorithm
 * is optimized for accents present in the study area (southern part of the city
 * of Dunedin, New Zealand).
 * 
 * (Excerpt taken from Wikipedia)
 * Created on Apr 29, 2012 9:43:39 AM
 * @see http://caversham.otago.ac.nz/files/working/ctp060902.pdf 
 * @author Eric D. Nelson
 */
public class Caverphone1 extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {
	
	private final org.apache.commons.codec.language.Caverphone1 caverphone;
	
	public Caverphone1() {
		super();
		this.caverphone = new org.apache.commons.codec.language.Caverphone1();
	}
    
    /**
     * This method compares two String using the Caverphone1 encoding. If the 
     * Strings produce the same encoding then the Strings are considered a 
     * match and true is returned.
     * @param str1 input source String
     * @param str2 input target String
     * @return true if the source and target are considered a match, otherwise
     * false.
     */
    public boolean compare(String str1, String str2){
        // Ensure there is something to compare
        if (!isArgValid(str1,str2)) {
        	return rightAnswer(false);
        }
        boolean result = false;
        try {
        	result = caverphone.isEncodeEqual(str1, str2);
        } catch (EncoderException ex) {
            logger.error("{} : {} : error:()",str1,str2,ex.getMessage());
        }
        return rightAnswer(result);
    }

    /**
     * This method determines the David Hood Caverphone1 hash for the input string. 
     * @param str The input string
     * @return a String containing the Caverphone1 encoding or an empty string
     */
    public String transform(String str) {
        String result = DEFAULT_TRANSFORM_RESULT;

        if (!isArgValid(str)) {
    		return result;
    	}

        return caverphone.encode(str);
    }
}
