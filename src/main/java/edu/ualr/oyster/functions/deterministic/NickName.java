/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;

/*
 * NickName is an implementation of Alias that used a predefined
 * alias table name alias.dat
 * 
 * When it compares two strings, it first checks to see if the
 * first string is a key. If it is a key, then it checks to see if
 * the second string is in the list for that key.
 * If the first string is not a key, then it checks to see if the
 * second string is a key. If it is, the checks to see if the
 * first string is in the list for that key.
 * If either condition is satisfied, the comparison is true,
 * otherwise it is false.
 */
public class NickName extends Alias implements Compare {
	
	private static final String NICKNAME_PATH_NAME = "data/alias.dat";
	
	/**
	 * No parameters are passed for this function
	 * Use the base class method to throw the error
	 */
	protected void configure(String parameters) {
		if (!StringUtils.isBlank(parameters)) {
			 throw new IllegalArgumentException(String.format("Function %1$s does not take a filename parameter, use ALIAS instead", this.name));
		}
		super.configure(NICKNAME_PATH_NAME);
	}
}
