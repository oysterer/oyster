/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/** 
 * Substring transform and compare function
 * 
 * LEFT, MID, RIGHT gets the number of characters for length beginning at the
 * LEFT, specified midpoint start, RIGHT
 * If the length of the string is less than the specified length the partial result is returned
 * If the string is null or empty, and empty string is returned
 * 
 * If A is a substring of, but not equal to, B, then A is called a proper 
 * substring of B, written A ⊊ B (A is a proper substring of B) or B ⊋ A 
 * (B is a proper super-string of A).
 * The proper transform just returns the original string
 * http://en.wikipedia.org/wiki/Substring
 * @author Eric D. Nelson
 * 
 * @author jatrue
 *
 */
public class Substring extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {
	
	/**
	 * Substring starting position (0 relative)
	 */
	protected int start = 0;
	public int getStart() {
		return start;
	}
	protected void setStart(int start) {
		this.start = start;
		if (this.start < 0) {
			throw new IllegalArgumentException(this.getName() + " start: " + length + " must be a positive number");
		}
	}

	/**
	 * Substring length
	 */
	protected int length = 0;
	public int getLength() {
		return length;
	}
	protected void setLength(int length) {
		this.length = length;
		if (this.length < 1) {
			throw new IllegalArgumentException(this.getName() + " length: " + length + " must be greater than 0");
		}
	}

	// Substring mode
	protected enum Mode {
		LEFT, MID, RIGHT, PROPER;

		public static Mode getByName(String name) {
			for (Mode entry : Mode.values()) {
				if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
					return entry;
				}
			}
			return null;
		}
	}
	protected Mode mode; 
	public Mode getMode() {
		return mode;
	}
	protected void setMode(Mode mode) {
		this.mode = mode;
	}
	
	/**
	 * No argument constructor
	 */
	public Substring() {
		super();
	}
	
	/**
	 * Constructor to be used by subclasses so they don't have to use configure()
	 * 
	 * @param mode
	 */
	public Substring(Mode mode) {
		super();
		setMode(mode);
	}

	@Override
	public void configure(String parameters) {

		// Save the parameters
		this.parameters = parameters;

		// Only one argument token is allowed
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length < 2) {
			throw new IllegalArgumentException(
					this.getName() + " the mode parameter should be one of: " + getOptions(Mode.class));
		}

		// Validate and get the mode
		this.mode = Mode.getByName(tokens[0]);
		if (this.mode == null) {
			throw new IllegalArgumentException(
					this.getName() + " mode: " + tokens[0] + " invalid, use: " + getOptions(Mode.class));
		}		
		switch (this.mode) {
		case MID :
			// Parse the numeric start
			try {
			setStart(Integer.parseUnsignedInt(tokens[1]));
			} catch (Exception e) {
				throw new IllegalArgumentException(this.getName() + " start: " + parameters + " invalid, must be unsigned integer");
			}
			// Parse the numeric length
			try {
			setLength(Integer.parseUnsignedInt(tokens[2]));
			} catch (Exception e) {
				throw new IllegalArgumentException(this.getName() + " length: " + parameters + " invalid, must be unsigned integer");
			}
			break;
		case LEFT :
		case RIGHT :
		case PROPER :
			// Start is not used but make sure it is initialized to something
			setStart(0);
			// Parse the numeric length
			try {
			setLength(Integer.parseUnsignedInt(tokens[1]));
			} catch (Exception e) {
				throw new IllegalArgumentException(this.getName() + " length: " + parameters + " invalid, must be unsigned integer");
			}
			break;
		}
	}
	
    public String transform(String str) {
    	// Null or blank arguments return empty string
    	if (StringUtils.isBlank(str)) {
        	return DEFAULT_TRANSFORM_RESULT;
        }
    	String result = str;
    	switch (this.mode) {
    	case LEFT :
    		result = StringUtils.left(str,length);
    		break;
    	case MID :
    		result = StringUtils.mid(str,start,length);
    		break;
    	case RIGHT :
    		result = StringUtils.right(str,length);
    		break;
    	case PROPER :
    	default:
    		// just return the original string
    	}
    	return result;
    }
    
    public boolean compare(String str1, String str2){
    	// Transform the two arguments
        String transformed1 = transform(str1);
        String transformed2 = transform(str2);
        
        // Ensure there is something to compare
        if (!isArgValid(transformed1,transformed2)) {
        	return rightAnswer(false);
        }
        if (Mode.PROPER.equals(this.mode)) {
        	// str2 is a substring of str1
    		if (str1.toLowerCase(Locale.US).contains(str2.toLowerCase(Locale.US))) {
    			return rightAnswer(true);
    		}
    		// str1 is a substring of str2
    		if (str2.toLowerCase(Locale.US).contains(str1.toLowerCase(Locale.US))) {
    			return rightAnswer(true);
    		}
    		// no matching substrings
    		return rightAnswer(false);
        } else {
        	return rightAnswer(transformed1.equalsIgnoreCase(transformed2));
        }
    }
    
    /*
     *  Validates minimum length of arguments
     */
	public boolean isArgValid(String... args) {
		// Test each argument
		for (String arg : args) {
			// Null or blank arguments are invalid
			if (StringUtils.isBlank(arg)) {
				return false;
			}
			// for PROPER mode, validate strings are minimum length
			if (Mode.PROPER.equals(this.mode)) {
				if (arg.length() < length) {
					return false;
				}
			}
		}
		// All arguments are valid
		return true;
	}

}
