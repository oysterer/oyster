/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/** 
 * Mid substring transform and compare function 
 * This is a facade over Substring and could be deprecated
 *
 * @author James True
 *
 */
public class SubstringMid extends Substring implements Compare, Transform, Tokenize {
	/**
	 * Constructor invokes Substring constructor with Mode indicator
	 */
	public SubstringMid() {
		super(Mode.MID);
	}
	
	@Override
	public void configure(String parameter) {
		
		// Tokenize on comma delimiter
		String [] tokens = StringUtils.split(parameters,", ");
		if (tokens.length != 2) {
			throw new IllegalArgumentException(this.getName() + " parameters should be start, length");
		}
		
		// Parse the numeric start
		try {
		setStart(Integer.parseUnsignedInt(tokens[0]));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(this.getName() + " start: " + parameter + " invalid, must be unsigned integer");
		}

		// Parse the numeric length
		try {
		setLength(Integer.parseUnsignedInt(tokens[1]));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(this.getName() + " length: " + parameter + " invalid, must be unsigned integer");
		}
	}
}
