/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.functions.deterministic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;
/**
 * Stopwords is an indexing and DataPrep function designed to filter
 * placeholder values before making a comparison. 
 * Stopwords takes a string representing a list of placeholder values and
 * an input value. If an input value matches one of the placeholder values, the
 * input value is replaced by an empty string (""), otherwise the input value is
 * returned unchanged. Both the placeholder values and the input value ingnore case
 * when performing the lookup.
 * The Stopwords takes one control parameter
 * 1) The list of placeholder value given a single string with the placeholder
 *    values separated by a pipe (|) delimiter
 */
public class Stopwords extends OysterFunctionDeterministic implements Transform, Compare, Tokenize {
	
	// Word list
    protected Set<String> words = new LinkedHashSet<String>();
    
    /**
     * Get the current list of words
     * 
     * @return immutable list of words 
     */
	public Set<String> getWords() {
		return Collections.unmodifiableSet(words);
	}
	protected void addWords(String word) {
		// Don't store empty words
		if (isArgValid(word)) {
			// Store as upper case for comparison
			this.words.add(word.toUpperCase());
		}
	}
	protected void addAllWords(Collection<String> words) {
		// NPE protection
		if (words == null)
			return;
		// Use addWords(String) for edits
		for (String word : words) {
			addWords(word);
		}
	}

	public Stopwords() {
		super();
		this.words = new LinkedHashSet<String>();
	}
	
	/**
	 * Parse list of words and store them for reuse
	 */
	@Override
	protected void configure(String parameters) {
		if (StringUtils.isBlank(parameters)) {
			 throw new IllegalArgumentException(String.format("A list of '|' delimited words must be supplied"));
		 }
		String words = StringUtils.upperCase(parameters);
    	String[] wordArray = StringUtils.split(words,"|");
    	addAllWords(Arrays.asList(wordArray));
	}

	/**
	 * Transform converts stop words to empty strings
	 * Test is case-insensitive
	 * 
	 * @param String to transform
	 * @return the original string if it is not a stop word or an empty "" string
	 */
	@Override
	public String transform(String arg) {
		if (words == null)
			return arg;
		if (words.contains(arg.toUpperCase()))
			return DEFAULT_TRANSFORM_RESULT;
		return arg;
	}
	
	/**
	 * Comparator
	 * Transform removes stop words
	 * Two stop words (both empth) are not equal
	 * Case sensitive comparision of resulting strings
	 * 
	 * @param arg1 left argument
	 * @param arg2 right argument
	 * @return true=strings are equal, false=strings are not equal or both empty
	 */
	@Override
	public boolean compare(String arg1, String arg2) {
		String word1 = transform(arg1);
		String word2 = transform(arg2);
		// If both words are stop words, not a match
		if ((word1.length() == 0) && (word2.length() == 0)) {
			return false;
		}
		return word1.equals(word2);
	}
}
