/*
 * Copyright 2013-2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * DoubleMetaphone.java
 *
 * http://en.wikipedia.org/wiki/Double_Metaphone
 * Created on Apr 29, 2012 8:38:38 AM
 * @author Eric D. Nelson
 */
public class DoubleMetaphone extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

	// use alternate encoding
	private boolean alternate;
	public boolean isAlternate() {
		return alternate;
	}
	protected void setAlternate(boolean alternate) {
		this.alternate = alternate;
	}

	// This isn't final because it may need to be replaced in the configure method
	private org.apache.commons.codec.language.DoubleMetaphone codec;

	// Encoding mode
	private enum Encoding {
		STANDARD, ALTERNATE;

		public static Encoding getByName(String name) {
			for (Encoding entry : Encoding.values()) {
				if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
					return entry;
				}
			}
			return null;
		}
	}

	
	/**
     * Creates a new instance of DoubleMetaphone
     */
    public DoubleMetaphone(){
		super();
		// Default is not to use alternate encoding
		this.alternate = false;
		this.codec = new org.apache.commons.codec.language.DoubleMetaphone();
    }

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Save the parameters
		this.parameters = parameters;

		// No arguments is allowed
		if (!isArgValid(parameters)) {
			return;
		}

		// Only one argument token is allowed
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length > 1) {
			throw new IllegalArgumentException(
					this.getName() + " the parameter should be one of: " + getOptions(Encoding.class));
		}

		// Validate and get the encoding
		Encoding encoding = Encoding.getByName(tokens[0]);
		if (encoding == null) {
			throw new IllegalArgumentException(
					this.getName() + " mode: " + tokens[0] + " invalid, use: " + getOptions(Encoding.class));
		}
		
		// Sent alternate encoding true or false
		switch (encoding) {
			case ALTERNATE :
				alternate = true;
				break;
			case STANDARD :
				alternate = false;
			default:
				break;
		}
	}

    /**
     * This method compares two String using the Double Metaphone. If the
     * Strings produce the same encoding then the Strings are considered a
     * match and true is returned.
     * @param str1 input source String
     * @param str2 input target String
     * @return true if the source and target are considered a match, otherwise
     * false.
     */
    public boolean compare(String str1, String str2){
        // Ensure there is something to compare
        if (!isArgValid(str1,str2)) {
        	return rightAnswer(false);
        }
        return rightAnswer(codec.isDoubleMetaphoneEqual(str1, str2, alternate));
    }

    public String transform(String str) {
    	if (!isArgValid(str)) {
    		return DEFAULT_TRANSFORM_RESULT;
    	}
    	return codec.doubleMetaphone(str, alternate);
    }
}
