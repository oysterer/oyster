/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/*
 * Missing values
 * 
 * @author James True
 */
public class Missing extends OysterFunctionDeterministic  implements Compare, Transform, Tokenize {

	// use folding encoding
	private Mode mode;
	public Mode getMode() {
		return mode;
	}
	public void setMode(String name) {
		Mode mode = Mode.getByName(name);
		if (mode == null) {
			throw new IllegalArgumentException(
					this.getName() + " mode: " + name + " invalid, use: " + getOptions(Mode.class));
		}
		this.mode = mode;
	}
	protected void setMode(Mode mode) {
		this.mode = mode;
	}

	// Folding mode
	private enum Mode {
		EITHER, BOTH;
		public static Mode getByName(String name) {
			for (Mode entry : Mode.values()) {
				if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
					return entry;
				}
			}
			return null;
		}
	}

	public Missing() {
		setMode(Mode.EITHER);
	}
	
	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Save the parameters
		this.parameters = parameters;

		// No arguments is allowed
		if (!isArgValid(parameters)) {
			logger.info("{} using default parameter: {}", this.getName(), mode.name());
			return;
		}

		// Only one argument token is allowed
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length > 1) {
			throw new IllegalArgumentException(
					this.getName() + " the parameter should be one of: " + getOptions(Mode.class));
		}
		setMode(tokens[0]);
	}


	/**
	 * Compares strings for missing values
	 * 
	 * @param str1 left string to compare
	 * @param str2 right string to compare
	 * @return true=either or both strings are missing
	 */
	@Override
	public boolean compare(String str1, String str2) {
		switch (this.mode) {
		case EITHER :
			if (!isArgValid(str1) || !isArgValid(str2)) { 
				return rightAnswer(true);
			}
		case BOTH :
			if (!isArgValid(str1) && !isArgValid(str2)) { 
				return rightAnswer(true);
			}
		}
		return rightAnswer(false);
	}
	
	/**
	 * Transform does not alter the strings
	 */
	@Override
	public String transform(String str) {
		if(!isArgValid(str)) {
			return DEFAULT_TRANSFORM_RESULT;
		}
		return str;
	}
}
