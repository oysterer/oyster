/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * Fingerprint is based on the Google Refine Method Fingerprint methodology:
 * The fingerprinting method is fast and simple yet works relatively well in a 
 * variety of contexts and it's the least likely to produce false positives, 
 * which is why it is the default method. 
 * 
 * The process that generates the key from a string value is the following (note
 * that the order of these operations is significant): 
 * <ul>
 * <li>remove leading and trailing whitespace </li>
 * <li>change all characters to their lowercase representation </li>
 * <li>remove all punctuation and control characters </li>
 * <li>split the string into whitespace-separated tokens </li>
 * <li>sort the tokens and remove duplicates </li>
 * <li>join the tokens back together </li>
 * <li>normalize extended western characters to their ASCII representation (for 
 * example "gödel" → "godel") </li>
 * </ul>
 * 
 * There are several factors that play a role in this fingerprint: 
 * <ul>
 * <li>because whitespace is normalized, characters are lowercased, and 
 * punctuation is removed, those parts don't play a differentiation role in the 
 * fingerprint. Because these attributes of the string are the least significant
 * in terms of meaning differentiation, these turn out to be the most varying 
 * parts of the strings and removing them has a substantial benefit in emerging 
 * clusters. 
 * <li>because the string parts are sorted, the given order of tokens doesn't 
 * matter (so "Cruise, Tom" and "Tom Cruise" both end up with a fingerprint 
 * "cruise tom" and therefore end up in the same cluster) 
 * <li>normalizing extended western characters plays the role of reproducing 
 * data entry mistakes performed when entering extended characters with an 
 * ASCII-only keyboard. Note that this procedure can also lead to false 
 * positives, for example "gödel" and "godél" would both end up with "godel" as 
 * their fingerprint but they're likely to be different names, so this might 
 * work less effectively for datasets where extended characters play substantial
 * differentiation role. 
 * </ul>
 * 
 * Created on Apr 29, 2012 8:02:08 AM
 * @author Eric D. Nelson
 */
public class Fingerprint extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

    public String transform(String str) {
        String result = DEFAULT_TRANSFORM_RESULT;
        
        if (!isArgValid(str)) {
        	return result;
        }
        String temp = "";
        try {
        	// Clean up the strings
            temp = str.toLowerCase(Locale.US).trim();
            temp = temp.replaceAll("\\p{Punct}|\\p{Cntrl}", "");
            // split by char
            TreeSet<String> set = new TreeSet<String>();
            set.addAll(Arrays.asList(temp.split("")));

            StringBuilder sb = new StringBuilder();
            Iterator<String> i = set.iterator();
            while (i.hasNext()) {
                sb.append(i.next());
                sb.append(' ');
            }
            result = sb.toString();
        } catch (RuntimeException ex) {
            logger.error("arg:{}, temp:{}, result:(), error:()", str,temp,result,ex.getMessage());
            result = DEFAULT_TRANSFORM_RESULT;
        }
        return result;
    }
}
