/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.experimental;

import java.util.Locale;

import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.ParamInt;

/**
 * The Needleman–Wunsch algorithm performs a global alignment on two sequences 
 * (called A and B here). It is commonly used in bioinformatics to align protein
 * or nucleotide sequences. The algorithm was published in 1970 by Saul B. 
 * Needleman and Christian D. Wunsch.[1]
 * 
 * (Excerpt taken from Wikipedia)
 * 
 * References:
 * <ul>
 * <li>Needleman, Saul B.; and Wunsch, Christian D. (1970). "A general method 
 * applicable to the search for similarities in the amino acid sequence of 
 * two proteins". Journal of Molecular Biology 48 (3): 443–53. </li>
 * <li></li>
 * </ul>
 * Created on Jun 20, 2012 9:44:43 PM
 * @author Eric. D Nelson
 */
public class NeedlemanWunsch extends OysterFunctionProbabilistic {

	/*
	 * Extend ParamInt class to name these parameters
	 */
	private class Match extends ParamInt {}
	private final Match match;

	private class Mismatch extends ParamInt {}
	private final Mismatch mismatch;

	private class Gap extends ParamInt {}
	private final Gap gap;
	
	public NeedlemanWunsch() {
		// Always call the parent function constructor
		super();
		
		// Instantiate the parameter properties
		this.match = new Match();
		this.mismatch = new Mismatch();
		this.gap = new Gap();
	}
	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = parseArgs(parameters);
		if (tokens.length < 3) {
			throw new IllegalArgumentException(this.getName() + " parameters should be (match,mismatch,gap)");
		}
		match.setValue(tokens[0]);
		mismatch.setValue(tokens[1]);
		gap.setValue(tokens[2]);
		
		// Threshold is optional and a property of the parent class
		if (tokens.length == 4) {
			setThreshold(tokens[3]);
		}
	}

    // FIXME: Does not give the same answer as examples, not sure why
	@Override
    public float score(String s, String t) {
        float result = 0.0f;
        int lengthOfLong = 0;
        float [][] matrix = null;
        String sequence1 = "", sequence2 = "";
        lengthOfLong = 0;
        if (!isArgValid(s,t)) {
        	return result;
        }
        String sTemp = s.toUpperCase(Locale.US);
        String tTemp = t.toUpperCase(Locale.US);
        
        lengthOfLong = Math.max(tTemp.length(), sTemp.length());
        
        // create the matrix
        matrix = new float[tTemp.length()+1][sTemp.length()+1];
        
        // calculate the first row and column
        for (int i = 0; i < tTemp.length()+1; i++) {
            matrix[i][0] = gap.getValue() * i;
        }
        for (int j = 0; j < sTemp.length()+1; j++) {
            matrix[0][j] = gap.getValue() * j;
        }
        
        // compute the value for each cell
        for (int i = 1; i <= tTemp.length(); i++) {
            for (int j = 1; j <= sTemp.length(); j++) {
                System.out.println("Matrix[" + i + "]" + "[" + j + "]");
                System.out.println("char @ " + (i-1) + ": " + tTemp.charAt(i-1));
                System.out.println("char @ " + (j-1) + ": " + sTemp.charAt(j-1));
                // do the characters match?
                if (tTemp.charAt(i-1) == sTemp.charAt(j-1)){
                    // Match ← F(i-1,j-1) + S(Ai, Bj)
                    matrix[i][j] = matrix[i-1][j-1] + match.getValue();
                    System.out.println("Match   : " + matrix[i][j]);
                }
                else {
                    float diag = matrix[i-1][j-1] + mismatch.getValue();
                    // Delete ← F(i-1, j) + d
                    float top =    matrix[i-1][j] + gap.getValue();
                    // Insert ← F(i, j-1) + d
                    float left = matrix[i][j-1] + gap.getValue();
                    
                    float max = Math.max(diag, top);
                    max = Math.max(max, left);
                    
                    matrix[i][j] = max;
                    System.out.println("Nonmatch: " + matrix[i][j]);
                }
                System.out.println();
            }
        }
        
        int i = tTemp.length();
        int j = sTemp.length();
        
        while (i > 0 && j > 0){
            float score = matrix[i][j];
            float diag = matrix[i - 1][j - 1];
            float up = matrix[i][j - 1];
            float left = matrix[i - 1][j];
            
            // FIXME: Need to change to use BigDecimal @see http://findbugs.sourceforge.net/bugDescriptions.html#FE_FLOATING_POINT_EQUALITY
            if (score == diag + match.getValue()){
                sequence1 = sTemp.charAt(j - 1) + sequence1;
                sequence2 = tTemp.charAt(i - 1) + sequence2;
                i--;
                j--;
            }
            else if (score == left + gap.getValue()) {
                sequence1 = "-" + sequence1;
                sequence2 = tTemp.charAt(i - 1) + sequence2;
                i--;
            }
            else if (score == up + gap.getValue()) {
                sequence1 = sTemp.charAt(j - 1) + sequence1;
                sequence2 = "-" + sequence2;
                    j--;
                }
            }
  
            while (i > 0) {
                sequence1 = "-" + sequence1;
                sequence2 = tTemp.charAt(i - 1) + sequence2;
                i--;
            }
  
            while (j > 0) {
                sequence1 = sTemp.charAt(j - 1) + sequence1;
                sequence2 = "-" + sequence2;
            j--;
        }
        
        System.out.println("Input    :");
        System.out.println("\t" + s);
        System.out.println("\t" + t);
        System.out.println("Alignment:");
        System.out.println("\t" + sequence1);
        System.out.println("\t" + sequence2);
//      result = matrix[start[0]][start[1]];
        
        return Math.max(result, 0f)/(lengthOfLong * match.getValue());
    }
    
//    public float computeNormalizedScore2 () {
//        float norm;
//        
//        int gaps = Math.max(sequence1.split("[-]").length-1, sequence2.split("[-]").length-1);
//        float maxLen = lengthOfLong;
//        norm = 1f - ((float)gaps / maxLen);
//        
//        return norm;
//    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NeedlemanWunsch sw = new NeedlemanWunsch();
        sw.configure("10, -1, -5");
        // Sequence 1 = A-CACACTA
        // Sequence 2 = AGCACAC-A
        System.out.println("Normalized Score  : " + sw.score("AGCT", "AGCT"));
    }

}
