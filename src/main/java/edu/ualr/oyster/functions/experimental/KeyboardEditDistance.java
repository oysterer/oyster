/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.experimental;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ualr.oyster.formatter.ErrorFormatter;

/**
 * KeyboardEditDistance.java
 * Created on Jan 1, 2011 5:05:11 AM
 * @author Eric D. Nelson
 */
public class KeyboardEditDistance {
    private int distance = 0;
    //private int lengthOfShort = 0;
    private int lengthOfLong = 0;
    private boolean debug = false;

    private Map<String, HashMap<String, Integer>> keyboard = new HashMap<String, HashMap<String, Integer>>();
    
    /**
     * Creates a new instance of KeyboardEditDistance
     */
    public KeyboardEditDistance(){
        String read, key;
        String [] text;
        HashMap<String, Integer> m;
        BufferedReader infile = null;

        try{
            File file = new File("data/keyboard_edits.dat");
//            System.out.println("Loading " + file.getName());
            infile = new BufferedReader(new FileReader(file));
            while((read = infile.readLine()) != null){
                if (!read.startsWith("!!")) {
                    text = read.split("[\t]");
                    
                     m = new HashMap<String, Integer>();
                     for (int i = 1; i < text.length; i++){
                         int value = Integer.parseInt(text[i]);
                         
                         switch (i){
                             case 1: key = "A"; break;
                             case 2: key = "B"; break;
                             case 3: key = "C"; break;
                             case 4: key = "D"; break;
                             case 5: key = "E"; break;
                             case 6: key = "F"; break;
                             case 7: key = "G"; break;
                             case 8: key = "H"; break;
                             case 9: key = "I"; break;
                             case 10: key = "J"; break;
                             case 11: key = "K"; break;
                             case 12: key = "L"; break;
                             case 13: key = "M"; break;
                             case 14: key = "N"; break;
                             case 15: key = "O"; break;
                             case 16: key = "P"; break;
                             case 17: key = "Q"; break;
                             case 18: key = "R"; break;
                             case 19: key = "S"; break;
                             case 20: key = "T"; break;
                             case 21: key = "U"; break;
                             case 22: key = "V"; break;
                             case 23: key = "W"; break;
                             case 24: key = "X"; break;
                             case 25: key = "Y"; break;
                             case 26: key = "Z"; break;
                             default: key = "";
                         }
                         m.put(key, value);
                     }
                     keyboard.put(text[0], m);
                }
            }
        }
        catch(IOException ex){
            Logger.getLogger(KeyboardEditDistance.class.getName()).log(Level.WARNING, ErrorFormatter.format(ex), ex);
        } finally {
            try {
                if (infile != null) {
                    infile.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(KeyboardEditDistance.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            }
        }
    }
    
    public int getDistance () {
        return distance;
    }

    public void setDistance (int distance) {
        this.distance = distance;
    }

    public void clear () {
    }

    public boolean isDebug () {
        return debug;
    }

    public void setDebug (boolean debug) {
        this.debug = debug;
    }

    /**
     * 
     * @param s source String
     * @param t target String
     * @return 0 if the strings are exact match, otherwise the number of 
     * transpositions to change String s to String t
     */
    public int computeDistance(String s, String t) {
        int result = 0, cost;
        if (s == null && t != null){
            //lengthOfShort = 0;
            lengthOfLong = t.length();
            result = t.length();
        }
        else if (s != null && t == null){
            lengthOfLong = s.length();
            //lengthOfShort = 0;
            result = s.length();
        }
        else if (s != null && t != null){
            //lengthOfShort = Math.min(s.length(),t.length());
            lengthOfLong = Math.max(s.length(),t.length());
            int m = s.length()+1;
            int n = t.length()+1;
            
            s = s.toUpperCase(Locale.US);
            t = t.toUpperCase(Locale.US);
            
            // d is a table with m+1 rows and n+1 columns
            int [][] d = new int[m][n];
            
            for (int i = 0; i < m; i++) {
                d[i][0] = i;
            }
            for (int j = 0; j < n; j++) {
                d[0][j] = j;
            }
            
            for (int j = 1; j < n; j++){
                for (int i = 1; i < m; i++) {
                    if (s.charAt(i-1) == t.charAt(j-1)) {
                        cost = 0;
                    } else {
                        cost = getCost(s.charAt(i-1), t.charAt(j-1));
                    }
                    d[i][j] = Math.min(Math.min(d[i-1][j] + 1,     // insertion
                                                d[i][j-1] + 1),    // deletion
                                                d[i-1][j-1] + cost // substitution
                              );
                }
            }
            result = d[m-1][n-1];
        }
        distance = result;
        return result;
    }

    private int getCost(char c0, char c1) {
        int result = 10;
        HashMap<String, Integer> m = keyboard.get("" + c0);
        
        if (m != null){
            result = m.get("" + c1);
        }
        
        return result;
    }
    
    /** This method computes the normalized edit distance as ratio of edit 
     *  distance to shorter string length
     * @return float as the normalized distance of the two strings being compared
     */
    public float computeNormalizedScore () {
        float norm;
        
        float maxLen = lengthOfLong;
        norm = 1f - ((float) distance / maxLen);
        
        return norm;
    }

}
