/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.experimental;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Transform;

/**
 * Metaphone3.java
 * 
 * 
 * Created on Apr 29, 2012 8:38:38 AM
 * @author Eric D. Nelson
 */
public class Metaphone3 extends OysterFunctionDeterministic implements Compare, Transform {

	@Override
    public String transform(String arg) {
        // Null or empty string
        if (StringUtils.isBlank(arg)) {
        	return "";
        }

        String result = "";
        String temp = "";

        try {
            temp = arg.toUpperCase(Locale.US).trim();

            // 1. Standardize the string by removing all punctuations and spaces
            temp = temp.replaceAll("\\p{Punct}", "");
            temp = temp.replaceAll("\\p{Space}", "");
            temp = temp.trim();

            if (StringUtils.isBlank(temp)) {
            	return DEFAULT_TRANSFORM_RESULT;
            }
        } catch (RuntimeException ex) {
            logger.error("arg:{}, temp:{}, result:(), error:()", arg,temp,result,ex.getMessage());
            result = "";
        }
        return result;
    }
}
