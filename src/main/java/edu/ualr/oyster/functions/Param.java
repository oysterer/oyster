package edu.ualr.oyster.functions;

/**
 * Base parameter class
 * 
 * This stores the state of the parameter
 * 
 * @author James True
 *
 */
public abstract class Param {
	
	/*
	 * Has the parameter been initialized
	 */
	protected boolean valid;
	
	/**
	 * Determine if the value has been set
	 * 
	 * @return true=value set, false=value not set
	 */
	public boolean isValid() {
		return valid;
	}
	/**
	 * No-argument constructor
	 */
	public Param() {
		this.valid = false;
	}
}
