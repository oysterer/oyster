/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.tokenizer;

import java.util.LinkedHashSet;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.OysterFunctionTokenizer;
import edu.ualr.oyster.functions.Tokenize;

/**
 * ListTokenizer is the indexing hash function designed to support the 
 * ListOverlap Comparator 
 * 
 * The ListTokenizer takes a string representing a list of items and
 * splits it into separate list times according to the list delimiter character
 * given by the user. After splitting, the list of items is filtered by two
 * criteria. If the user has provided a minimum item length, then any tokens
 * shorter than the minimum item length are removed. Secondly, if the user has
 * given a list of excluded item values, then any item in the input list
 * matching one of the excluded item values is removed. 
 * 
 * The ListTokenizer takes three control parameters 
 * 1) The list delimiter character used to split the string into list items 
 *     If not delimiter character is specified, the comma is used by default 
 * 2) An integer value specifying the minimum length of a token to index 
 * 3) Optional list of excluded items
 */

public class ListTokenizer extends OysterFunctionTokenizer implements Tokenize {

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = parseArgs(parameters);
		if ((tokens.length < 2) ||(tokens.length > 3)) {
			throw new IllegalArgumentException(
					this.getName() + " parameters should be (list-delimiter, minimum-length, [exclude-list])");
		}
		// Threshold cannot be optional since it is the first parameter
		setListDelimiters(tokens[0]);
		setMinimumLength(tokens[1]);
		// Optional stop word list
		if (tokens.length == 3) {
			stopWords.setWords(tokens[2]);
		}
	}

	public String[] tokenize(String arg) {
		LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
		String[] listRaw = StringUtils.split(arg, listDelimiters);

		// Start loop to check for valid, non-duplicate property-value pairs
		for (int i = 0; i < listRaw.length; i++) {
			String token = (listRaw[i].trim()).toUpperCase(Locale.US);
			if (token.length() > 0) {
				if (token.length() >= minimumLength && !stopWords.isWord(token))
					hashKeys.add(token);
			}
		}
		return hashKeys.toArray(new String[0]);
	}
}
