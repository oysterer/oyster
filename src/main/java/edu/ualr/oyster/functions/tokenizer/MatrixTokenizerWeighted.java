/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.tokenizer;

import java.util.LinkedHashSet;
import java.util.Locale;

import edu.ualr.oyster.functions.OysterFunctionTokenizer;
import edu.ualr.oyster.functions.ParamFloat;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.resource.TokenWeightTableResource;

/**
 * MatrixTokenizer is the indexing function designed to support the
 * MatrixComparatorWEighted The MatrixTokenizerWeighted takes a string and
 * splits it into tokens delimited by any non-Word character, i.e. any
 * characther except a letter or digit Each resulting token is trimmed and
 * letters uppercased After splitting, the list of tokens is filtered by it
 * token weight. If the token weight is greater than or equal to the user
 * provided cutoff score, the token used to index the record. Otherwise it is
 * omitted from the index. The MatrixTokenizerWeighted only takes one parameter,
 * the cuttoff score 1) Floating point value specifying the minimum weight of a
 * token to index
 */
public class MatrixTokenizerWeighted extends OysterFunctionTokenizer implements Tokenize {

	/**
	 * Threshold value
	 * Extend ParamFloat so that error messages will reflect Threshold
	 */
	private class Threshold extends ParamFloat {};
	private final Threshold threshold;

	/**
	 * Set the threshold from a string value
	 * 
	 * @param threshold as a string
	 */
	protected void setThreshold(String threshold) {
		try {
			this.threshold.setValue(threshold);
		} catch (Exception e) {
			throw new IllegalArgumentException(formatMessage(e.getMessage()));
		}
	}

	// Token weight table, initialized in configure
	private TokenWeightTableResource tokenWeightTable;

	public MatrixTokenizerWeighted() {
		this.threshold = new Threshold();
	}

	@Override
	public void configure(String parameters) {
		String[] tokens = parseArgs(parameters);
		if ((tokens.length < 1) || (tokens.length > 2)) {
			throw new IllegalArgumentException(
					formatMessage("parameters should be (Threshold [TokenWeightTableFilename])"));
		}
		// Threshold cannot be optional since it is the first parameter
		setThreshold(tokens[0]);
		if (tokens.length < 2) {
			// Use default file name
			this.tokenWeightTable = TokenWeightTableResource.getInstance();
		} else {
			// Use specified file name
			this.tokenWeightTable = TokenWeightTableResource.getInstance(tokens[1]);
		}
	}

	@Override
	public String[] tokenize(String inStr) {
		if (tokenWeightTable == null) {
			throw new IllegalStateException(formatMessage("Weight table not initialized"));
		}
		LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
		// minimum length of a cleaned token to index
		String[] listRaw = inStr.split("[\\W+]");
		// Start loop to add tokens to hashKey set∂
		for (int i = 0; i < listRaw.length; i++) {
			// Trim tokens and change letters to uppercase
			String token = (listRaw[i].trim()).toUpperCase(Locale.US);
			// System.out.println(token);
			// Remove all non-alphanumeric characters
			// token = token.replaceAll("\\W", "").trim();
			if (token.length() > 0) {
				// Check if token should be excluded
				float tokenWeight = tokenWeightTable.getTokenWeight(token);
				if (threshold.test(tokenWeight))
					hashKeys.add(token);
			}
		}
		return hashKeys.toArray(new String[0]);
	}
}
