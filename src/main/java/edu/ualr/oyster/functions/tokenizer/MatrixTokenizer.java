/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.tokenizer;

import java.util.LinkedHashSet;
import java.util.Locale;

import edu.ualr.oyster.functions.OysterFunctionTokenizer;
import edu.ualr.oyster.functions.Tokenize;

/**
 * MatrixTokenizer is the indexing function designed to support the
 * MatrixComparator The MatrixTokenizer takes a string and splits it into tokens
 * delimited by any non-word character, i.e. anything not a letter or digit.
 * Each resulting token is then upper-cased. After splitting a string, the list
 * of generated tokens is filtered by two criteria. The user must provide a
 * minimum token length. Any tokens shorter than the minimum token length are
 * removed. Secondly, if the user has given a list of excluded token values,
 * then any token matching one of the excluded token values is removed. Excluded
 * tokens are also cleared by non-word characters and upper-cased Parameters 1)
 * An integer value specifying the minimum length of a token to index 2)
 * Optional list of excluded tokens
 * 
 * FIXME This class stoes intermediate results in instance properties, not
 * reentrant
 */
public class MatrixTokenizer extends OysterFunctionTokenizer implements Tokenize {

	@Override
	public void configure(String parms) {
		String[] temp = parseArgs(parms);
		
		if (temp.length == 0) {
			throw new IllegalArgumentException(this.getName() + " first parameter must define a minimum token length]");
		}
		// The first parameter must define minimum token length, shorter tokens will be skipped
		setMinimumLength(temp[0]);
		
		// Second parameter (Optional) defines a list of excluded tokens separated by pipe (|) character 
		// Any token in the StopWord list will not be skipped for indexing
		if (temp.length > 1) {
			stopWords.setWords(temp[1]);
		}
	}

	public String[] tokenize(String inStr) {
		LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
		String[] listRaw = inStr.split("[\\W]");
		// Start loop to add tokens to hashKey set
		for (int i = 0; i < listRaw.length; i++) {
			// Change letters to uppercase
			String token = (listRaw[i].trim()).toUpperCase(Locale.US);
			// Remove all non-alphanumeric characters
			// token = token.replaceAll("\\W", "").trim();
			if (token.length() > 0) {
				// Check if token minimum length and not in excluded value list
				if (token.length() >= minimumLength && !stopWords.isWord(token))
					hashKeys.add(token);
			}
		}
		return hashKeys.toArray(new String[0]);
	}
}
