package edu.ualr.oyster.functions.tokenizer;

import java.util.LinkedHashSet;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.OysterFunctionTokenizer;
import edu.ualr.oyster.functions.Tokenize;

public class ListTokenizerPV extends OysterFunctionTokenizer implements Tokenize {

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = parseArgs(parameters);
		if ((tokens.length < 2) || (tokens.length > 3)) {
			throw new IllegalArgumentException(
					this.getName() + " parameters should be (list-delimiter, pair-delimiter, [exclude-list])");
		}
		// Threshold cannot be optional since it is the first parameter
		setListDelimiters(tokens[0]);
		setPairDelimiters(tokens[1]);
		// Optional stop word list
		if (tokens.length == 3) {
			stopWords.setWords(tokens[2]);
		}
	}

	public String[] tokenize(String arg) {
		LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
		String[] listRaw = StringUtils.split(arg, listDelimiters);
		// Start loop to check for valid, non-duplicate property-value pairs
		for (int i = 0; i < listRaw.length; i++) {
			String pair = listRaw[i].trim().toUpperCase(Locale.US);
			String[] keyValue = StringUtils.split(pair, pairDelimiters);
			if (keyValue.length == 2) {
				String value = keyValue[1].trim();
				if (value.length() > 0 && !stopWords.isWord(value)) {
					hashKeys.add(pair);
				}
			}
		}
		return hashKeys.toArray(new String[0]);
	}
}
