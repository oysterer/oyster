package edu.ualr.oyster.functions;

/**
 * Validates and stores numeric parameter values for OysterFunctions
 * 
 * @author James True
 *
 */
public abstract class OysterFunctionDeterministic extends OysterFunction implements Compare, Transform, Tokenize {

	/**
	 * No-argument constructor
	 * Defaults range values
	 */
	protected OysterFunctionDeterministic() {
		super();
	}

	/**
	 * Default configuration is no parameters
	 */
	@Override
	protected void configure(String parameters) {
		super.configure(parameters);
	}	
	/**
	 * Default compare function, compares the results of transform on both strings
	 * If either argument is null or empty or all blanks, returns false
	 * Performs a case-sensitive comparison between the two arguments
	 *
	 * @see edu.ualr.oyster.functions.Compare#compare(java.lang.String,java.lang.String)
	 * 
	 * @param arg1 left compare argument
	 * @param arg2 right compare argument
	 * @return true=both arguments are identical after the transform
	 * 		false=arguments are different, null, or blank
	 */
	public boolean compare(String arg1, String arg2) {
		// NPE protection
		if (!isArgValid(arg1,arg2)) {
			return rightAnswer(false);
		}
		
		// Transform the two arguments
		String result1 = transform(arg1);
		String result2 = transform(arg2);
	       
        // Ensure there is something to compare
        if (!isArgValid(result1,result2)) {
			return rightAnswer(false);
        }
        	
        return rightAnswer(result1.equals(result2));
	}
	
	/**
	 * Default tokenize function
	 * Stores the result of the transform in a single element array
	 * This is used primarily for indexing which requires an array of values
	 *
	 * @see edu.ualr.oyster.functions.Tokenize#tokenize(java.lang.String)
	 * 
	 * @param arg is the argument string to transform and return
	 * @return single element array of transformed argument, or an empty array
	 */
	public String[] tokenize(String arg) {
		// No argument returns an empty array
    	if (!isArgValid(arg)) {
    		return new String[0];
    	}
    	return new String[] {transform(arg)};
	}
	
	/**
	 * The transform method must be implemented in the subclass
	 * This is where the real work is done for compare and tokenize
	 * 
	 * @see edu.ualr.oyster.functions.Transform#transform(java.lang.String)
	 */
	public abstract String transform(String arg);

}
