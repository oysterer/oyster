package edu.ualr.oyster.functions;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * Generic word list 
 * This is used for stop words, excludes, etc.
 * 
 * @author jatrue
 *
 */
public class ParamWordlist extends Param {
	
	private char delimiter;
	private final LinkedHashSet<String> words;
	
	public char getDelimiter() {
		return delimiter;
	}
	
	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	/*
	 * Set of words (unique)
	 */

	public Set<String> getStopWords() {
		return words;
	}

	
	public boolean hasWords() {
		return (words.size() > 0);
	}

	/*
	 * Tokens are trimmed of white space and converted to upper case
	 * for comparison to the word list.
	 */
	public boolean isWord(String token) {
		String word = StringUtils.upperCase(StringUtils.trim(token));
		if (words.contains(word)) {
			return true;
		}
		return false;
	}

	/*
	 * Words are trimmed of white space and stored in upper case
	 */
	public void setWords(String words) {
		this.valid = false;
		String[] wordArray = StringUtils.split(words,delimiter);
		for (String word : wordArray) {
			this.words.add(StringUtils.upperCase(StringUtils.trim(word)));
		}
		this.valid = true;
	}
	/**
	 * no-argument constructor
	 * Initializes the stop words map
	 */
	public ParamWordlist() {
		super();
		this.delimiter = '|';
		this.words = new LinkedHashSet<String>();
	}
	
	/**
	 * Constructor taking the list of words using the default delimiter
	 * @param words
	 */
	public ParamWordlist(String words) {
		this();
		setWords(words);
	}
	
	public ParamWordlist(char delimiter, String words) {
		this();
		setDelimiter(delimiter);
		setWords(words);
	}
}
