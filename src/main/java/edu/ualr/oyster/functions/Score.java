/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions;

/**
 * Interface Score
 * Created on July 4, 2019 10:54 AM
 * This interface must be implemented by Oyster functions generating a numeric similarity score
 * @author John R. Talburt
 */
public interface Score extends OysterAction {
	
	/**
	 * Compute similarity score between two string using the implemented algorithm
	 * 
	 * @param arg1 left candidate string
	 * @param arg2 right candidate string
	 * @return the normalized score value between 0.0 - 1.0
	 */
	public float score(String arg1, String arg2);
	
}
