package edu.ualr.oyster.functions;

/**
 * Validates and stores numeric parameter values for OysterFunctions
 * 
 * @author James True
 *
 */
public abstract class OysterFunctionProbabilistic extends OysterFunction implements Compare, Score {
	
	// Create a named sublcass for the threshold parameter
	private class Threshold extends ParamFloat {
		public Threshold() {
			super();
		}
		public Threshold(Float minimum, Float maximum) {
			super(minimum,maximum);
		}
	}
	private final Threshold threshold;
	
	public float getThreshold() {
		return this.threshold.getValue();
	}
	
	/**
	 * Convenience function to set the threshold from a string representation
	 * 
	 * @param parameters
	 * @throws IllegalArgumentException
	 */
	public void setThreshold(String parameters) throws IllegalArgumentException {
		try {
			this.threshold.setValue(parameters); 
		} catch (IllegalArgumentException ex) {
			throw new IllegalArgumentException(formatMessage(ex.getMessage()));
		}
	}
	
	/**
	 * Convenience function to set the threshold from a string representation
	 * 
	 * @param parameters
	 * @throws IllegalArgumentException
	 */
	public void setThreshold(float parameters) throws IllegalArgumentException {
		try {
			this.threshold.setValue(parameters); 
		} catch (IllegalArgumentException ex) {
			throw new IllegalArgumentException(formatMessage(ex.getMessage()));
		}
	}
	
	/**
	 * Test the computed score against the threshold value 
	 * and normalize the retult based on the functions reverseLogic setting
	 * 
	 * @param score value to test
	 * @return true=score is equal to or above threshold, adjusted by reverseLogic
	 * 		  false=score is below threshold, adjusted by reverseLogic
	 */
	public boolean testScore(float value) {
		return rightAnswer(threshold.test(value));
	}

	/**
	 * No-argument constructor
	 * Defaults range values
	 */
	protected OysterFunctionProbabilistic() {
		super();
		this.threshold = new Threshold();
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	protected OysterFunctionProbabilistic(float minimum, float maximum) {
		super();
		this.threshold = new Threshold(minimum,maximum);
	}

	/**
	 * Default configuration is a single optional "threshold" parameter specification
	 * 
	 * NOTE the threshold parameter could be required here based on whether the class
	 * implements Compare, but then that would require that the threshold be specified
	 * when the class is being constructed for the Transform interface
	 */
	@Override
	protected void configure(String parameters) throws IllegalArgumentException {
		if (!isArgValid(parameters)) {
			return;
		}
		setThreshold(parameters); 
	}

	/**
	 * Default comparator implementation
	 * This only works if the score returns a value between minThreshold and maxThreshold
	 * Calls the scoring function and if the resulting score is:
	 *  - equal to or greater than the threshold returns true
	 *  - less than the threshold returns false
	 */
	public boolean compare(String str1, String str2) throws IllegalStateException {
		// Is the threshold value set
		if (!threshold.isValid()) {
			throw new IllegalStateException(this.getName() + " Threashold value must be specified for Compare");
		}
		// Is the score a normalized value
		float result = score(str1,str2);
		if (!threshold.isNormalized(result)) {
			throw new IllegalStateException(String.format("%1$s Score %2$f is not a normalized value",this.getName(),result));
		}
		return testScore(result);
	}

	@Override
	public abstract float score(String str1, String str2);
}
