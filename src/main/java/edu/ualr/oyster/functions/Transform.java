/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions;

/**
 * Interface Transform
 * Created on July 4, 2019 10:54 AM
 * This interface must be implemented by Oyster functions used for DataPrep
 * @author John R. Talburt
 */
public interface Transform extends OysterAction {
	
	// Default value to return on a transform error
	public static final String DEFAULT_TRANSFORM_RESULT = "";
	
	/**
	 * Transform takes one string as input and returns a transformed value as output
	 * The implementation determines the nature of the transformation
	 * 
	 * @param arg string to transform
	 * @return transformed string or DEFAULT_TRANSFORM_RESULT 
	 */
	public String transform(String arg);
}
