package edu.ualr.oyster.utilities;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.Score;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.functions.OysterFunction.Builder;

import java.io.IOException;

public class OysterFunctionCli {
	
	protected final static Logger logger = LoggerFactory.getLogger(OysterFunction.class.getName());

	public static void main(String[] args) {
		try{
        	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        	System.out.println("Enter functions signature");
        	String signature = reader.readLine();
        	System.out.println("Enter function type");
        	String type = reader.readLine(); 
        	OysterFunction func = null;
        	if(type.equalsIgnoreCase("transform")) {
	        	System.out.println("Enter Test Value");
	        	String testValue = reader.readLine();
	    		try {
	    			func = new OysterFunction.Builder<OysterFunction>(signature).build();
	    			//System.out.println(func.toString());
	    		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
	    			e.printStackTrace();
	    		}
	    		if (func != null) {
	    			if(func instanceof Transform) {
	    				System.out.println(((Transform)func).transform(testValue));
	    			} else {
	    				String name = func.getName();
	    				logger.error(name+": Does not implement transform method");
	    				
	    			}
	    		}
        	}
        	if(type.equalsIgnoreCase("compare")) {
	        	System.out.println("Enter Test Value1");
	        	String testValue1 = reader.readLine();
	        	System.out.println("Enter Test Value2");
	        	String testValue2 = reader.readLine();
	    		try {
	    			func = new OysterFunction.Builder<OysterFunction>(signature).build();
	    			//System.out.println(func.toString());
	    		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
	    			e.printStackTrace();
	    		}
	    		if (func != null) {
	    			if(func instanceof Compare) {
	    				System.out.println(((Compare)func).compare(testValue1, testValue2));
	    			} else {
	    				String name = func.getName();
	    				System.out.println(name+": Does not implement compare method");
	    			}
	    		}
        	}
        	
        	if(type.equalsIgnoreCase("score")) {
	        	System.out.println("Enter Test Value1");
	        	String testValue1 = reader.readLine();
	        	System.out.println("Enter Test Value2");
	        	String testValue2 = reader.readLine();
	    		try {
	    			func = new OysterFunction.Builder<OysterFunction>(signature).build();
	    			//System.out.println(func.toString());
	    		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
	    			e.printStackTrace();
	    		}
	    		if (func != null) {
	    			if(func instanceof Score) {
	    				System.out.println(((Score)func).score(testValue1, testValue2));
	    			} else {
	    				String name = func.getName();
	    				System.out.println(name+": Does not implement score method");
	    			}
	    		}
        	}
        	
        	if(type.equalsIgnoreCase("tokenize")) {
	        	System.out.println("Enter Test Value");
	        	String testValue = reader.readLine();
	    		try {
	    			func = new OysterFunction.Builder<OysterFunction>(signature).build();
	    			//System.out.println(func.toString());
	    		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
	    			e.printStackTrace();
	    		}
	    		if (func != null) {
	    			if(func instanceof Tokenize) {
	    				System.out.println(((Tokenize)func).tokenize(testValue));
	    			} else {
	    				String name = func.getName();
	    				System.out.println(name+": Does not implement tokenize method");
	    			}
	    		}
        	}
        	
        } catch(IOException ex){
            System.out.println("File Opening IO Exception Occurred\n");
            System.out.println(ex.getMessage());
        } 

	}

}
