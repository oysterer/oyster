package edu.ualr.oyster.utilities;

import java.util.Scanner;
import org.apache.commons.text.StrTokenizer;
import org.apache.commons.text.StrMatcher;

public class ReaderTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String inString="Start";
		//StrMatcher delim = StrMatcher.charSetMatcher('|');
		//StrMatcher quote = StrMatcher.quoteMatcher();
		while (inString.length() > 0) {
			System.out.print("Enter String to Test\n");
			inString = scanner.nextLine();
			StrTokenizer line = new StrTokenizer(inString,',','\"');
			line.setIgnoredChar(' ');
       	 	while (line.hasNext()) {
       	 		String token = line.next();
       	 		System.out.println(token);
       	 	}
		}
		scanner.close();
	}
		
}
