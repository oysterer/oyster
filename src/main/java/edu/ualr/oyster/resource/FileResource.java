/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.resource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract base class for reference file collections
 * This provide the functionality read the file into memory
 * and standardizes the properties
 * <p>
 * The reference collection should be allocated by the child class.  
 * This base class is only concerned with file operations, all collection
 * management should be in the child class
 *
 */
public abstract class FileResource {

	protected static final Logger logger = LoggerFactory.getLogger(FileResource.class);

	protected String pathname;

	/** 
	 * Name of the NickName configuration file
	 * @return the pathname of the file loaded, or attempted to be loaded
	 */
	public String getPathname() {
		return pathname;
	}
	
	private int lineCount;
	
	/**
	 * Count of non-comment lines in the file
	 * @return line count
	 */
	public int getLineCount() {
		return lineCount;
	}

	private int commentCount;
	
	/**
	 * Count of comment lines in the file
	 * @return comment line count
	 */
	public int getCommentCount() {
		return commentCount;
	}
	
	/**
	 * The constructor initializes the properties of this instance
	 * 
	 * @param pathname to load
	 */
	protected FileResource(String pathname) {
		this.pathname = pathname;
		this.lineCount = 0;
		this.commentCount = 0;
	}

	/**
	 * Load reads file and calls the parse() method to store the table into memory. 
	 * The child class should call this method and test the result
	 * <p>
	 * It catches and logs
	 * 	IOExceptions on for file access errors
	 * 	InvalidParameterExceptions for parsing errors
	 * 
	 * @return true=success, false=error
	 */
	protected boolean load() {
		
		// Validate the pathname
		if (StringUtils.isBlank(pathname)) {
			logger.error("PathName cannot be missing or blank");
			return false;
		}
		logger.info("Loading reference file: {}", this.pathname);
	
		/*
		 * Read the file and call the parse method
		 */
		try (BufferedReader infile = new BufferedReader(new FileReader(this.pathname))) {
			String line;
			while ((line = infile.readLine()) != null) {
				// call the line parser
				if (parse(line)) {
					// line processed and stored
					++lineCount;
				} else {
					// Line ignored
					++commentCount;
				}
			}
			logger.info(" Loaded reference file: {} : lines {}, comments {}", 
					new Object[] {this.pathname, this.lineCount, this.commentCount});
			return true;
		} catch (IOException | IllegalArgumentException ex) {
			// Make sure any errors are logged
			logger.error(ex.getMessage());
		}
		return false;
	}
	
	/**
	 * Helper null-safe method to determine if a line is a comment
	 * 
	 * @param line to evaluate
	 * @param array of comment indicator prefix strings so there can be more than one
	 * @return true=comment, false=not a comment
	 */
	protected boolean isComment(String line, String [] indicators) {
		// Blank lines are comments
		if (StringUtils.isBlank(line)) {
			return true;
		}
		
		// No comment indicators to test
		if ((indicators == null) || (indicators.length == 0)) {
			return false;
		}
		
		// Iterate through the comment prefix indicators
		for (String indicator : indicators) {
			// If it starts with the indicator returnt rue
			if (line.startsWith(indicator)) {
				return true;
			}
		}
		// No match, not a comment
		return false;
	}
	
	/**
	 * Abstract parse method that must be implemented by the child class
	 * The return value indicates whether the line was processed or
	 * ignored and is considered a comment
	 * 
	 * @param line of text from the file
	 * @return true=processed, false=ignored/comment
	 * @throws IllegalArgumentException on parsing errors
	 */
	protected abstract boolean parse(String line) throws IllegalArgumentException;

}
