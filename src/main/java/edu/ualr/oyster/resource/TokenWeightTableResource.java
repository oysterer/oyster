/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.resource;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class builds and accesses the table of tokens and weights The key value
 * is a string token, and value is the weight of the token The table has two
 * purposes 1) When called by the MatrixWeighted comparator, the comparator
 * provides a token, the table returns the weight of the token 2) When called by
 * the ListWeightTokenizer, the tokenizer provides a a token, and the table
 * returns true/false if the token is stop word (true) or not a stop word
 * (false)
 * <ul>
 * <li>Two column tab delimited</li>
 * <li>Any comments must be preceded by !!</li>
 * <li>The first column is a string token value</li>
 * <li>The second column is a float weight value</li>
 * </ul>
 * 
 * @author John R. Talburt
 */

public class TokenWeightTableResource extends FileResource {

	protected static final Logger logger = LoggerFactory.getLogger(TokenWeightTableResource.class);

	// Lines starting with any of theses sequences are considered comments
	private static final String DEFAULT_PATHNAME = "data/tokenWeightTable.dat";
	private static final String[] commentIndicators = { "!!" };
	

	/**
	 * HashMap of pathnames to instances
	 */
	private static final Map<String, TokenWeightTableResource> instances = new HashMap<String, TokenWeightTableResource>();
	
	/*
	 * Get the instance using the default token weight table name
	 */
	public static TokenWeightTableResource getInstance() {
		return getInstance(DEFAULT_PATHNAME);
	}

	/**
	 * Get a named (pathname) token weight table reference
	 * 
	 * @param pathname
	 * @return the singleton instance for that pathname
	 */
	public static synchronized TokenWeightTableResource getInstance(String pathname) {

		// See if we have an instance for that name already
		TokenWeightTableResource instance = instances.get(pathname);

		// If we don't, create one and store it for reuse
		if (instance == null) {
			instance = new TokenWeightTableResource(pathname);
			instances.put(pathname, instance);
		}

		// Return the instance for the specified pathname
		logger.info("  Using reference file: {}",pathname); 
		return instance;
	}

	/** The Token Weight table */
	private Map<String, Float> tokenWeightTable;

	/**
	 * Returns a reference to the mutable Token Weight Table
	 * 
	 * @return the Token Weight Table
	 */
	public Map<String, Float> getTokenWeightTable() {
		return tokenWeightTable;
	}

	/**
	 * Returns the weight for a given token for this
	 * <code>TokenWeightTableResource</code> Returns weight of 1.0 if token is not
	 * in table
	 * 
	 * @param token
	 */
	public float getTokenWeight(String token) {
		Float weight = tokenWeightTable.get(normalizeToken(token));
		if (weight == null) {
			return 1.0f;
		} else {
			return weight;
		}
	}

	/**
	 * Determines if token is a Stop Word for this
	 * <code>TokenWeightTableResource</code> If token weight is less than or equal
	 * to cutoff parameter, then returns true If token weight is above cutoff or if
	 * token not found, returns false
	 * 
	 * @param cutoff value
	 * @return true if token weight <= cutoff, else return false
	 */
	public boolean isStopWord(String token, Float cutoff) {
		String key = normalizeToken(token);
		Float weight = tokenWeightTable.get(key);
		if (weight != null) {
			if (weight <= cutoff) {
				return true;
			}
		}
		return false;
	}

	/**
	 * loads the tokens and weights into the table
	 * 
	 * @param filename the data file to be loaded
	 */
	public TokenWeightTableResource(String pathname) {
		super(pathname);

		// Allocation the collection object
		this.tokenWeightTable = new Hashtable<String, Float>();

		// Load the file from disk into the collection
		if (!load()) {
			logger.error("Error loading Stop Words reference file: {}", pathname);
			// Clear partial contents
			this.tokenWeightTable.clear();
			throw new IllegalStateException("Error loading Stop Word table from: " + pathname);
		}
	}
	
	/*
	 * Normalize the token string for storing and fetching
	 * 
	 * @param token is the raw string
	 * @return upper case string with white space removed
	 */
	protected String normalizeToken(String token) {
		String result = StringUtils.deleteWhitespace(token);
		return StringUtils.upperCase(result);
	}

	/*
	 * Parse each line from the reference file
	 * 
	 * @param line of text to process
	 * 
	 * @return true=stored, false=comment, blank, ignored
	 * 
	 * @see edu.ualr.oyster.resource.FileResource#parse(java.lang.String)
	 */
	@Override
	protected boolean parse(String line) throws IllegalArgumentException {
		if (isComment(line, commentIndicators)) {
			return false;
		}
		if (StringUtils.isBlank(line)) {
			return false;
		}

		try {
			String[] text = line.split("[\t]");
			// load token as key and weight as value
			String token = normalizeToken(text[0]);
			float weight = Float.parseFloat(text[1]);
			tokenWeightTable.put(token, weight);
		} catch (Exception ex) {
			throw new IllegalArgumentException(
					String.format("Illegal weight argument, missing delimiter or non-numeric value [%1$s]", line));
		}
		return true;
	}
}
