/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton instance of a specified alias table
 */
public class AliasTableResource extends FileResource {

	protected static final Logger logger = LoggerFactory.getLogger(AliasTableResource.class);
	
	// Lines starting with any of theses sequences are considered comments
	private static String [] commentIndicators = {"!!","#"};
	
	/**
	 * HashMap of pathnames to instances
	 */
	private static final Map<String, AliasTableResource> instances = new HashMap<String, AliasTableResource>();

	/**
	 * Get a named (pathname) instance of an Alias Reference
	 * 
	 * @param pathname
	 * @return the singleton instance for that pathname
	 */
	public static synchronized AliasTableResource getInstance(String pathname) {

		// See if we have an instance for that name already
		AliasTableResource instance = instances.get(pathname);

		// If we don't, create one and store it for reuse
		if (instance == null) {
			instance = new AliasTableResource(pathname);
			instances.put(pathname, instance);
		}
		
		// Return the instance for the specified pathname
		logger.info("Using reference file: {}",pathname); 
		return instance;
	}

	/*
	 *  Alias table for this instance
	 */
	private final HashMap<String, ArrayList<String>> aliasTable;

	/**
	 * Get the alias collection
	 * 
	 * @return a mutable reference to the table
	 */
	public HashMap<String, ArrayList<String>> getAliasTable() {
		return aliasTable;
	}

	private AliasTableResource(String pathname) throws IllegalStateException {
		// Initialize the base class which sets the filename
		super(pathname);
		// Allocation the collection object
		aliasTable = new HashMap<String, ArrayList<String>>();
		// Load the file from disk into the collection
		if (!load()) {
			logger.error("Error loading Alias reference file: {}", pathname);
			// Clear partial contents
			this.aliasTable.clear();
			throw new IllegalStateException("Error loading Alias table from:" + pathname);
		}
	}

	/**
	 * Parse each line from the file and create the alias table
	 * The key can have multiple alias values
	 */

	@Override
	protected boolean parse(String line) throws IllegalArgumentException {
		if (isComment(line, commentIndicators)) {
			return false;
		}
		
		String[] text = line.split("[\t]");
		if (text.length != 2) {
			throw new IllegalArgumentException(this.pathname + " : key-value entry is invalid : " + line);
		}
		String key = text[0].toUpperCase(Locale.US);
		String value = text[1].toUpperCase(Locale.US);
		if (StringUtils.isBlank(key) || StringUtils.isBlank(value)) {
			throw new IllegalArgumentException(this.pathname + " : entry has blank value : " + line );
		}
		ArrayList<String> valueList;
		if (!aliasTable.containsKey(key)) {
			// If key is not already in table, add key-value pair
			valueList = new ArrayList<String>();
			valueList.add(value);
			aliasTable.put(key, valueList);
		} else {
			// If key is already in table,
			// add value to list provided value not already in list
			valueList = aliasTable.get(key);
			if (!valueList.contains(value)) {
				valueList.add(value);
				aliasTable.put(key, valueList);
			}
		}
		return true;
	}

}
