/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.resource;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Use getInstance to get the instance of the table for a file pathname
 * 
 * This class is used to find the agreement weight for certain value. Each line
 * of the text file specified by the WgtTable must have two, table-separated
 * values. The first value of the attribute defined by the Item attribute of the
 * <Term>. The second value in each line of the text file is an integer value
 * containing only decimal digits with an optional prefix plus symbol or a minus
 * prefix symbol in the case of negative values, e.g. "100", "+100", or "-100".
 * The integer value represents the agreement weight for the corresponding
 * attribute value given as the first value.
 * <ul>
 * <li>Two column tab delimited</li>
 * <li>Any comments must be preceded by !!</li>
 * </ul>
 * 
 * @author Cheng Chen Revised by John Talburt 2018-11-04 to include a
 *         disagreement weight in addition to the agreement weigh for each
 *         token. The weight table was changed from LinkedHashMap <String,
 *         Double> to LinkedHashMap <String, OysterWeightPair> where
 *         OysterWeightPair is a new data class for an ordered pair of Double
 *         values. The first Double is the agreement weight, the second is the
 *         disagreement weight To be compatible with previously created table,
 *         the disagreement weight is optional (null)
 */

public class WeightTableResource extends FileResource {

	protected static final Logger logger = LoggerFactory.getLogger(WeightTableResource.class);

	// Lines starting with any of theses sequences are considered comments
	private static String[] commentIndicators = { "!!" };

	/**
	 * HashMap of pathnames to instances
	 */
	private static final Map<String, WeightTableResource> instances = new HashMap<String, WeightTableResource>();

	/**
	 * Get a named (pathname) instance of an Weight Table Reference
	 * 
	 * @param pathname
	 * @return the singleton instance for that pathname
	 */
	public static synchronized WeightTableResource getInstance(String pathname) {

		// See if we have an instance for that name already
		WeightTableResource instance = instances.get(pathname);

		// If we don't, create one and store it for reuse
		if (instance == null) {
			instance = new WeightTableResource(pathname);
			instances.put(pathname, instance);
		}

		// Return the instance for the specified pathname
		logger.info("  Using reference file: {}",pathname); 
		return instance;
	}

	/*
	 * Table of key values with weight pairs. Key value is always lower case for
	 * case insensitive access
	 */
	private Map<String, WeightPair> weightTable;

	/**
	 * Returns a reference to the mutable Weight Table
	 * 
	 * @return the Weight Table
	 */
	public Map<String, WeightPair> getWeightTable() {
		return weightTable;
	}

	public void addWeightPair(String valueKey, WeightPair pair) {
		// NPE protection
		if ((valueKey == null) || (pair == null)) {
			return;
		}
		// Set the flag if any entry has disagreement
		if (pair.getDisagreeWgt() > 0.0) {
			hasDisagreementWeight = true;
		}
		// Add the entry with a lower-case key
		weightTable.put(valueKey.toLowerCase(), pair);
	}

	/**
	 * Returns the weight pair from the weight table or null if not found
	 * 
	 * @param token
	 * @return weight pair
	 */
	public WeightPair getWeightPair(String token) {
		return weightTable.get(token.toLowerCase(Locale.US));
	}

	/**
	 * Weight table contains one or more disagreement weights
	 */
	private boolean hasDisagreementWeight;

	/**
	 * Indicates that the disagreement weight should be used
	 * 
	 * @param token
	 * @return weight pair
	 */
	public boolean usesDisagreement() {
		return hasDisagreementWeight;
	}

	/**
	 * Creates a new instance of <code>OysterNickNameTable</code>
	 * 
	 * @param filename the data file to be loaded
	 */
	private WeightTableResource(String pathname) {
		super(pathname);

		// Allocation the collection object
		weightTable = new LinkedHashMap<String, WeightPair>();

		// Initially no disagreement weights
		this.hasDisagreementWeight = false;

		// Load the file from disk into the collection
		if (!load()) {
			logger.error("Error loading Weight Table reference file: {}", pathname);
			// Clear partial contents
			this.weightTable.clear();
			throw new IllegalStateException("Error loading Weight Table from: " + pathname);
		}
	}

	/*
	 * Parse each line as it is read from the file
	 * 
	 * @param input line
	 * 
	 * @return true=line processed, false=line comment, blank or ignored
	 */
	@Override
	protected boolean parse(String line) throws IllegalArgumentException {
		if (isComment(line, commentIndicators)) {
			return false;
		}
		if (StringUtils.isBlank(line)) {
			return false;
		}
		try {
			String[] text = line.split("[\t]");
			String valueKey = text[0].toLowerCase(Locale.US);
			double weight = Double.parseDouble(text[1]);
			WeightPair pair = new WeightPair();
			pair.setAgreeWgt(weight);
			if (text.length == 3) {
				hasDisagreementWeight = true;
				weight = Double.parseDouble(text[2]);
				pair.setDisagreeWgt(weight);
			} else {
				pair.setDisagreeWgt(0.0);
			}
			addWeightPair(valueKey, pair);
		} catch (Exception ex) {
			throw new IllegalArgumentException(
					String.format("Illegal weight argument, non-numeric values: %1$s", line));
		}
		return true;
	}
}
