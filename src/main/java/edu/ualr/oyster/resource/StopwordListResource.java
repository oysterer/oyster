/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.resource;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton instance of a specified alias table
 * 
 * Use getInstance to get the instance of the table for a file pathname
 */
public class StopwordListResource extends FileResource {

	protected static final Logger logger = LoggerFactory.getLogger(StopwordListResource.class);
	
	// Lines starting with any of theses sequences are considered comments
	private static String [] commentIndicators = {"#"};
	
	/**
	 * HashMap of pathnames to instances
	 */
	private static final Map<String, StopwordListResource> instances = new HashMap<String, StopwordListResource>();

	/**
	 * Get a named (pathname) instance of an Alias Reference
	 * 
	 * @param pathname
	 * @return the singleton instance for that pathname
	 */
	public static synchronized StopwordListResource getInstance(String pathname) {

		// See if we have an instance for that name already
		StopwordListResource instance = instances.get(pathname);

		// If we don't, create one and store it for reuse
		if (instance == null) {
			instance = new StopwordListResource(pathname);
			instances.put(pathname, instance);
		}
		
		// Return the instance for the specified pathname
		logger.info("Using reference file: {}",pathname); 
		return instance;
	}

	/*
	 *  Alias table for this instance
	 */
	private final Set<String> stopwordList;

	/**
	 * Get the stop word collection
	 * 
	 * @return a mutable reference to the table
	 */
	public Set<String> getStopwordList() {
		return stopwordList;
	}

	private StopwordListResource(String pathname) throws IllegalStateException {
		// Initialize the base class which sets the filename
		super(pathname);
		// Allocation the collection object
		stopwordList = new LinkedHashSet<String>();
		// Load the file from disk into the collection
		if (!load()) {
			logger.error("Error loading Stop Words reference file: {}", pathname);
			// Clear partial contents
			this.stopwordList.clear();
			throw new IllegalStateException("Error loading Stop Word table from: " + pathname);
		}
	}

	/**
	 * Parse each line from the file and populate the stop word set
	 * Duplicates are ignored and indicated as comments to get an accurate count of stop words
	 * 
	 * @param line from the file to process.  It is stored with only whitespace trimming.
	 * Comments, blank lines, and nulls are ignored
	 * 
	 * @return true=stored, false=ignored
	 */
	@Override
	protected boolean parse(String line) throws IllegalArgumentException {
		if (isComment(line, commentIndicators)) {
			return false;
		}
		if (StringUtils.isBlank(line)) {
			return false;
		}
		
		// Trim the string and upper-case it
		String text = StringUtils.trim(line).toUpperCase();
		// Count duplicates as comments
		return stopwordList.add(text);
	}

}
