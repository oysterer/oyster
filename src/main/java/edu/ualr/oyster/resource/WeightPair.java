/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.resource;

/**
 * This is a new data class to hold both the agreement weight and disagreement weight returned from the scoring rule weight table
 * It creates a data structure containing two double values for storage in the weight table.
 * The first value is the agreement weight and second is the disagreement weight
 * To be compatible with previous versions using only the agreement weight, the disagreement weight can be set to null.
 * <ul>
 * <li>Two double values</li>
 * <li>Second value may be null </li>
 * </ul>
 * @author John R. Talburt
 */

public class WeightPair {
	
	double agreeWgt;
	double disagreeWgt;
	
	public void setAgreeWgt (double wgt){
		agreeWgt = wgt;
	}
	
	public void setDisagreeWgt (double wgt){
		disagreeWgt = wgt;
	}
	
	public double getAgreeWgt (){
		return agreeWgt;
	}
	
	public double getDisagreeWgt (){
		return disagreeWgt;
	}
	
	public WeightPair() {
		this.agreeWgt = 0;
		this.disagreeWgt = 0;
	}
	
	
	public WeightPair(double agreeWgt) {
		this.agreeWgt = agreeWgt;
		this.disagreeWgt = 0;
	}
	
	public WeightPair(double agreeWgt, double disagreeWgt) {
		this.agreeWgt = agreeWgt;
		this.disagreeWgt = disagreeWgt;
	}

}
