/*
 * Copyright 2012 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.core;

import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.resource.WeightTableResource;
import edu.ualr.oyster.functions.Compare;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * ScoringRuleTerm.java Created on June 5th, 2013 9:58:16 PM
 * Modified by John R. Talburt, 2018-11-06
 * Modified by John R. Talburt, 2019-08-03
 * Allows for multiple similarity and dataprep functions
 * Made similarity and dataprep functions cast to Compare and Transform, respectively
 * Fixed bug in trying to create table reference for when not table name given
 * @author Cheng Chen
 */
public class ScoringRuleTerm {
	private String item = null;
	private ArrayList<String> similaritySignatures = new ArrayList<String>();
	private ArrayList<String> dataPrepSignatures = new ArrayList<String>();
	private double agreewgt = 0;
	private String wgttable = null;
	private WeightTableResource weightTable = null;
	private double disagreewgt = 0;
	private double missing = 0;
	private boolean hasmissingweight = false;
	private Compare similarityFunction;
	private Transform dataprepFunction;
	ArrayList<Compare> similarityFunctions = new ArrayList<Compare>();
	ArrayList<Transform> dataPrepFunctions = new ArrayList<Transform>();
		
	/**
	 * Creates a new instance of RuleTerm
	 */
	public ScoringRuleTerm() {
	}
	
	public String getItem() {
		return item;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	
	public String getSimilaritySignature(int index) {
		return similaritySignatures.get(index);
	}
	
	public void addSimilaritySignature(String similarity) {
		this.similaritySignatures.add(similarity);
	}
	
	public ArrayList<String> getSimilaritySignatures() {
		return similaritySignatures;
	}
	
	public Compare getSimilarityFunction(int index) {
		return similarityFunctions.get(index);
	}
	
	public void addSimilarityFunction(Compare similarity) {
		this.similarityFunctions.add(similarity);
	}
	
	public ArrayList<Compare> getSimilarityFunctions() {
		return similarityFunctions;
	}
	
	public String getDataPrepSignature(int index) {
		return dataPrepSignatures.get(index);
	}
	
	public void addDataPrepSignature(String similarity) {
		this.dataPrepSignatures.add(similarity);
	}
	
	public ArrayList<String> getDataPrepSignatures() {
		return dataPrepSignatures;
	}
	
	public Transform getDataPrepFunction(int index) {
		return dataPrepFunctions.get(index);
	}
	
	public void addDataPrepFunction(Transform dataprep) {
		this.dataPrepFunctions.add(dataprep);
	}
	
	public ArrayList<Transform> getDataPrepFunctions() {
		return dataPrepFunctions;
	}
	
	public double getAgreewgt() {
		return agreewgt;
	}
	
	public void setAgreewgt(double agreewgt) {
		this.agreewgt = agreewgt;
	}
	
	public String getWgttable() {
		return wgttable;
	}
	
	public WeightTableResource getWeightTable() {
		return weightTable;
	}
	
	public void setWgttable(String wgttable) {
		this.wgttable = wgttable;
		if (!StringUtils.isEmpty(wgttable)) {
			weightTable = WeightTableResource.getInstance(wgttable);
		}
	}
	
	public double getDisagreewgt() {
		return disagreewgt;
	}
	
	public void setDisagreewgt(double disagreewgt) {
		this.disagreewgt = disagreewgt;
	}
	
	public double getMissing() {
		return missing;
	}
	
	public void setMissing(double missing) {
		this.missing = missing;
	}
	
	public boolean isHasmissingweight() {
		return hasmissingweight;
	}

	public void setHasmissingweight(boolean hasmissingweight) {
		this.hasmissingweight = hasmissingweight;
	}
	
	public void setSimilarityFunction(Compare function) {
		this.similarityFunction = function;
	}
	
	public Compare getSimiliarityFunction() {
		return similarityFunction;
	}

	public void setDataPrepFunction(Transform function) {
		this.dataprepFunction = function;
	}
	
	public Transform getDataPrepFunction() {
		return dataprepFunction;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ScoringRuleTerm other = (ScoringRuleTerm) obj;
		if (this.item == null || !this.item.equals(other.item)) {
			return false;
		}
		for (int j=0; j<similaritySignatures.size(); j++) {
			if (this.similaritySignatures.get(j) == null || !this.similaritySignatures.get(j).equals(other.similaritySignatures.get(j))) {
				return false;
			}
		}	
		for (int j=0; j<dataPrepSignatures.size(); j++) {
			if (this.dataPrepSignatures.get(j) == null || !this.dataPrepSignatures.get(j).equals(other.dataPrepSignatures.get(j))) {
				return false;
			}
		}	
		if (this.agreewgt!=other.agreewgt) {
			return false;
		}
		//weight table is optional
		if (!this.wgttable.equals(other.wgttable)) {
			return false;
		}
		if (this.disagreewgt!=other.disagreewgt) {
			return false;
		}
		if(this.hasmissingweight!= other.hasmissingweight){
			return false;
		}
		else{
			if (this.missing!=other.missing) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash2;
		double hash = 7;
		hash = 17 * hash + (this.item != null ? this.item.hashCode() : 0);
		for (int j=0; j<similaritySignatures.size(); j++) {
			hash = 17 * hash + (this.similaritySignatures.get(j) != null ? this.similaritySignatures.get(j).hashCode() : 0);
		}
		for (int j=0; j<dataPrepSignatures.size(); j++) {
			hash = 17 * hash + (this.dataPrepSignatures.get(j) != null ? this.dataPrepSignatures.get(j).hashCode() : 0);
		}
		hash = 17 * hash + this.agreewgt;
		hash = 17 * hash + (this.wgttable != null ? this.wgttable.hashCode() : 0);
		hash = 17 * hash + this.disagreewgt;
		hash = 17 * hash + this.missing;
		hash2 = (int) hash;
		return hash2;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName());
		sb.append("[item=");
		sb.append(this.item != null ? this.item : "");
		for (int j=0; j<similaritySignatures.size(); j++) {
			sb.append(", similarity=");
			sb.append(this.similaritySignatures.get(j) != null ? this.similaritySignatures.get(j) : "");
		}
		for (int j=0; j<dataPrepSignatures.size(); j++) {	
			sb.append(", dataprep=");
			sb.append(this.dataPrepSignatures.get(j) != null ? this.dataPrepSignatures.get(j) : "");
		}
		sb.append(", agreewgt=");
		sb.append(this.agreewgt);
		sb.append(", weightTable=");
		sb.append(this.wgttable != null ? this.wgttable : "");
		sb.append(", disagreewgt=");
		sb.append(this.disagreewgt);
		if(hasmissingweight){
			sb.append(", missing=");
			sb.append(this.missing);
		}
		sb.append("]");
		return sb.toString();
	}
}
