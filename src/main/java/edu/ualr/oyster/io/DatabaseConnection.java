/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.core.ReferenceItem;

/**
 * This class is used to read the Database files.
 * @author Eric D. Nelson
 */

public class DatabaseConnection {
	
	private static Logger logger = Logger.getLogger(DatabaseConnection.class.getName());

    /** The database connection */
    private Connection conn = null;

    /** Used to determine what type of database is being connected to */
    private String connectionType = null;
    
    /** Additional connection parameters */
    private String connectionParms = null;

    /** The server on which the database resides. This can be an IP address or
        Server name if a valid DSN entry exist */
    private String serverName = null;
    
    /** The port on which the database listens */
    private String portNumber = null;
    
    /** The name of the database */
    private String database = null;
    
    /** The name of the table or view that contains the data */
    private String tableName = null;
    
    /** The user's ID */
    private String userid = null;
    
    /** The user's Password */
    private String password = null;
    
    /** The custom SQL string to run */
    private String overRideSQL = null;

    /**
     * Creates a new instance of <code>DatabaseConnectionr</code>.
     * 
     * @param tableName the database table name to be read.
     * @param connectionType the database connection type. The current supported
     * types are Oracle, MySQL, ODBC, and SQL server.
     * @param connectionParms additional connection parameters
     * @param serverName the name of the server on which the database resides. 
     * This can be a DSN name or dotted decimal name.
     * @param portNumber the port on which the database listens.
     * @param database the name of the database.
     * @param userid the user id if applicable.
     * @param password the users password if applicable.
     * @param ri the ArrayList of <code>ReferenceItems</code> that will be used to
     * store the parsed data.
     */
    public DatabaseConnection(String tableName, 
    		String connectionType, 
    		String connectionParms, 
    		String serverName, 
    		String portNumber, 
    		String database, 
    		String userid, 
    		String password) {
        
        this.connectionType = connectionType;
        this.connectionParms = connectionParms;
        this.serverName = serverName;
        this.tableName = tableName;
        this.portNumber = portNumber;
        this.database = database;
        this.userid = userid;
        this.password = password;
    }
    
    /**
     * Returns the <code>Connection</code> for this <code>OysterDatabaseReader</code>.
     * @return the <code>Connection</code>.
     */
    public Connection getConn () {
        return conn;
    }

    /**
     * Sets the <code>Connection</code> for this <code>OysterDatabaseReader</code>.
     * @param conn the <code>Connection</code> to be set.
     */
    public void setConn (Connection conn) {
        this.conn = conn;
    }

    /**
     * Returns the connection type for this <code>OysterDatabaseReader</code>.
     * @return the connection type.
     */
    public String getConnectionType() {
        return connectionType;
    }

    /**
     * Sets the connection type for this <code>OysterDatabaseReader</code>.
     * @param connectionType the connection type to be set.
     */
    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    /**
     * Returns the connection parms for this <code>OysterDatabaseReader</code>.
     * @return the connection parms.
     */
    public String getConnectionParms() {
        return connectionParms;
    }

    /**
     * Sets the connection parms for this <code>OysterDatabaseReader</code>.
     * @param connectionParms additional connection parametsrs.
     */
    public void setConnectionParms(String connectionParms) {
        this.connectionParms = connectionParms;
    }

    /**
     * Returns the server name for this <code>OysterDatabaseReader</code>.
     * @return the server name.
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * Sets the server name for this <code>OysterDatabaseReader</code>.
     * @param serverName the server name to be set.
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * Returns the port number for this <code>OysterDatabaseReader</code>.
     * @return the port number.
     */
    public String getPortNumber() {
        return portNumber;
    }

    /**
     * Sets the port number for this <code>OysterDatabaseReader</code>.
     * @param portNumber the port number to be set.
     */
    public void setPortNumber(String portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * Returns the database name for this <code>OysterDatabaseReader</code>.
     * @return the database name.
     */
    public String getDatabase() {
        return database;
    }

    /**
     * Sets the database name for this <code>OysterDatabaseReader</code>.
     * @param database the database name to be set.
     */
    public void setDatabase(String database) {
        this.database = database;
    }
    
    /**
     * Returns the table name for this <code>OysterDatabaseReader</code>.
     * @return the table name.
     */
    public String getTableName () {
        return tableName;
    }

    /**
     * Sets the table name for this <code>OysterDatabaseReader</code>.
     * @param tableName the table name to be set.
     */
    public void setTableName (String tableName) {
        this.tableName = tableName;
    }

    /**
     * Returns the userid for this <code>OysterDatabaseReader</code>.
     * @return the userid.
     */
    public String getUserid() {
        return userid;
    }

    /**
     * Sets the userid for this <code>OysterDatabaseReader</code>.
     * @param userid the userid to be set.
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * Returns the password for this <code>OysterDatabaseReader</code>.
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password for this <code>OysterDatabaseReader</code>.
     * @param password the password to be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Returns the overRideSQL for this <code>OysterDatabaseReader</code>.
     * @return the overRideSQL.
     */
    public String getOverRideSQL() {
        return overRideSQL;
    }

    /**
     * Sets the overRideSQL for this <code>OysterDatabaseReader</code>.
     * @param overRideSQL the overRideSQL to be set.
     */
    public void setOverRideSQL(String overRideSQL) {
        this.overRideSQL = overRideSQL;
    }
    
    /**
     * Returns whether the connection to the database is established.
     * 
     * @return true if the connection to the database is established
     * 		   false if it connect connect for any reason
    */
	public boolean isConnected() {
		String url;
		boolean flag = false;
		if (conn == null) { // Connection has not been initialized
			if (connectionType == null) {
				logger.log(Level.INFO, "No Input Datbase CType Specified");
				return false;
			}
			try {
				// connect to database
				if (connectionType.equalsIgnoreCase("oracle")) {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					// default port number
					if (portNumber == null || portNumber.equals("")) {
						portNumber = "1521";
					}
					url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + database;
				} else if (connectionType.equalsIgnoreCase("mysql")) {
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					// default port number
					if (portNumber == null || portNumber.equals("")) {
						portNumber = "3306";
					}
					url = "jdbc:mysql://" + serverName + ":" + portNumber + "/" + database
							+ "?netTimeoutForStreamingResults=1200";
				} else if (connectionType.equalsIgnoreCase("sqlserver")) {
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					// default port number
					if (portNumber == null || portNumber.equals("")) {
						portNumber = "1433";
					}
					url = "jdbc:sqlserver://" + serverName+ ":" + portNumber + ";databasename=" + database;
					if (StringUtils.isNotBlank(connectionParms)) {
						url = url + ";" + connectionParms;
					}
				} else if (connectionType.equalsIgnoreCase("postgresql")) {
					Class.forName("org.postgresql.Driver");
					// default port number
					if (portNumber == null || portNumber.equals("")) {
						portNumber = "5432";
					}
					url = "jdbc:postgresql://" + serverName + ":" + portNumber + "/" + database;
				} else { // The JDBC-ODBC bridge is deprecated in Java 8
					throw new IllegalArgumentException("No valid Database CType specified:" + connectionType);
				}

				logger.log(Level.INFO, "Connecting to:"+url);
				// Pass the userid and password if both are supplied
				if (userid != null && password != null) {
					conn = DriverManager.getConnection(url, userid, password);
				} else {
					conn = DriverManager.getConnection(url);
				}

				System.out.println("Connection: " + conn);
			} catch (Exception ex) {
				logger.log(Level.SEVERE, "Error loading database driver", ex);
			}
		} else { 
			// conn != null
			try {
				flag = !conn.isClosed();
			} catch (SQLException e) {
				flag = false;
			}
		}
		return flag;
	}
    
    /**
     * This method creates a select statement for the this datasource based on
     * the set reference items. If no reference items are set then a SELECT * is
     * created.

     * If the format type is of type date you will have to know what are the acceptable
     * format patterns for a particular database. Links are list below for each database.
     * 
     * <ul>
     * <li>MySQL - http://dev.mysql.com/doc/refman/5.1/en/date-and-time-functions.html#function_date-format</li>
     * <li>Oracle - http://download.oracle.com/docs/cd/B19306_01/server.102/b14200/sql_elements004.htm#i34510</li>
     * <li>PostGreSQL - http://www.postgresql.org/docs/8.1/interactive/functions-formatting.html</li>
     * <li>SQL Server - http://www.mssqltips.com/tip.asp?tip=1145</li>
     * </ul>
     * @return an SQL select statement for the current reference items.
     */
    public String createConnectionString (List<ReferenceItem> referenceItems) {
    	
    	if (referenceItems == null) {
    		logger.log(Level.SEVERE, "Cannot create connection string, reference item list is missing");
    		// This might deserve an exception
    		return null;
    	}

    	StringBuilder sql = new StringBuilder(256);
        sql.append("select ");
        
        // This is used to get around the java.io.EOFException: Can not read response from server. 
        // Expected to read X bytes, read N bytes before connection was unexpectedly lost.
        // See: http://forums.mysql.com/read.php?39,203407,217196#msg-217196
        if (connectionType.equalsIgnoreCase("MYSQL")) {
            sql.append("SQL_NO_CACHE ");
        }
        int itemCount = referenceItems.size();
        if (itemCount == 0){
            sql.append("*");
        } else {
            for (int i = 0; i < itemCount; i++) {
            	// comma separator on subsequent items
            	if (i > 0) {
            		sql.append(" ,");
            	}
                ReferenceItem item = referenceItems.get(i);
                if(item.getFormat() != null){
                    if (item.getFormatType().equalsIgnoreCase("Date")){
                        if (connectionType.equalsIgnoreCase("ODBC")) {
                            sql.append("Format(").append(this.tableName).append(".").append(item.getName()).append(", '").append(item.getFormat()).append("') as ").append(item.getName());
                        } else if (connectionType.equalsIgnoreCase("ORACLE") || connectionType.equalsIgnoreCase("POSTGRESQL")) {
                            sql.append("to_char(").append(this.tableName).append(".").append(item.getName()).append(", '").append(item.getFormat()).append("') as ").append(item.getName());
                        } else if (connectionType.equalsIgnoreCase("MYSQL")) {
                            sql.append("DATE_FORMAT(").append(this.tableName).append(".").append(item.getName()).append(", '").append(item.getFormat()).append("') as ").append(item.getName());
                        } else if (connectionType.equalsIgnoreCase("SQLSERVER")) {
                            sql.append("convert(varchar, ").append(this.tableName).append(".").append(item.getName()).append(", '").append(item.getFormat()).append("') as ").append(item.getName());
                        }
                    }
                } else {
                    sql.append(item.getName());
                }
            }
         }
        sql.append(" from ").append(this.tableName);
        return sql.toString();
    }
}

