/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import edu.ualr.oyster.OysterMain;
import edu.ualr.oyster.core.OysterAttribute;
import edu.ualr.oyster.core.OysterAttributes;
import edu.ualr.oyster.core.OysterRule;
import edu.ualr.oyster.core.OysterScoringRule;
import edu.ualr.oyster.core.RuleTerm;
import edu.ualr.oyster.core.ScoringRuleTerm;
import edu.ualr.oyster.formatter.ErrorFormatter;
import edu.ualr.oyster.index.IndexRule;
import edu.ualr.oyster.index.IndexSegment;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.Tokenize;

/**
 * This class is used to parse the OysterAttribute XML file and return an 
 * instantiated <code>OysterAttributes</code> object.
 * @author Eric D. Nelson
 */

public class AttributesParser extends OysterXMLParser {
 
    /** The <code>OysterAttributes</code> to be populated */
    private OysterAttributes attributes = null;

    /** The <code>OysterAttribute</code> used during parsing */
    private OysterAttribute attribute = null;
    
    /** The identityRules used during parsing */
    ArrayList<OysterRule> identityRules = null;
    
    /** The <code>OysterRule</code> used during parsing */
    OysterRule rule = null;
    
    /** The termItem used during parsing */
    String termItem = null;
    
    /** The termItem used during parsing */
    Set<String> compareTo = null;
    
    /** The matchResult used during parsing */
    String matchResult = null;
    
    /** The indexingRules used during parsing */
    ArrayList<IndexRule> indexingRules = null;
    
    /** The <code>OysterRule</code> used during parsing */
    IndexRule indexRule = null;
    
    /** The <code>OysterRule</code> used during parsing */
    IndexSegment segment = null;
    
    /** The Indexing Rule Item used during parsing */
    String ruleItem = null;
    
    /** The Indexing Rule Hash used during parsing */
    String hash = null;
    
    /** The <code>OysterScoringRule</code> used during parsing */
    OysterScoringRule scoringrule = null;
    
    /**The scoringrule similarity used during parsing*/
    String similarity = null;
    
    /**The scoringrule's dataprep used during parsing*/
    String dataprep = null;
    
    /**The scoringrule's agreeWeight used during parsing*/
    double agreewgt = 0;
    
    /**The scoringrule's weight table used during parsing*/
    String wgttable = null;
    
    /**The scoringrule's disagree weight used during parsing*/
    double disagreewgt = 0;
    
    /**The scoringrule's missing weight used during parsing*/
    double missing = 0;   
    
    /** Used to hold the current XML parent tag */
    private String parent = "";
    
    /** Used to hold the list of Similarity Function and DataPrep signatures */
    private ArrayList<String> similaritySignatures = new ArrayList<String>();
    private ArrayList<String> dataPrepSignatures = new ArrayList<String>();
//    
//    /** Used to find the ScoringRule and IdentityRules's conflict */
//    boolean identityScolingRuleConflict=true;
    
    boolean scoringruletag=false;
    boolean IdentityRulestag=false;
    boolean hasmissingweight=false;
    boolean hasReviewScore=false;
    
    /**
     * Creates a new instance of <code>AttributesParser</code>.
     */
    public AttributesParser () {
        super();
        attributes = new OysterAttributes();
        attributes.setIdentityScolingRuleConflict(true);
    }

    /**
     * Returns the <code>OysterAttributes</code> for this <code>AttributesParser</code>.
     * @return the <code>OysterAttributes</code>.
     */
    public OysterAttributes getAttributes () {
        return attributes;
    }

    /**
     * Sets the <code>OysterAttributes</code> for this <code>AttributesParser</code>.
     * @param attributes the <code>OysterAttributes</code> to be set.
     */
    public void setAttributes (OysterAttributes attributes) {
        this.attributes = attributes;
    }

    /**
     * Called when the Parser starts parsing the Current XML File. Handle any
     * document specific initialization code here.
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startDocument () throws org.xml.sax.SAXException {
    }

    /**
     * Called when the Parser Completes parsing the Current XML File. Handle any
     * document specific clean up code here.
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endDocument () throws org.xml.sax.SAXException {
    }
 
    /**
     * Called when the starting of the Element is reached. For Example if we have
     * Tag called <Title> ... </Title>, then this method is called when <Title>
     * tag is Encountered while parsing the Current XML File. The attrs Parameter 
     * has the list of all Attributes declared for the Current Element in the 
     * XML File.
     * @param namespaceURI URI for this namespace
     * @param lName local XML name
     * @param qName qualified XML name
     * @param attrs list of all Attributes declared for the Current Element
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startElement (String namespaceURI, String lName, String qName, Attributes attrs) throws org.xml.sax.SAXException {
	    
	    String eName = lName; // element name
        if ("".equals(eName)) {
            eName = qName;
        }
        // namespaceAware = false emit("<"+eName);
        
        // clear the data
        data = "";
        
        if (eName.equalsIgnoreCase("OysterAttributes")){
            parent = eName;
        } else if (eName.equalsIgnoreCase("Attribute")){
            attribute = new OysterAttribute();
            parent = eName;
        } else if (eName.equalsIgnoreCase("IdentityRules")){
      	  attributes.setIdentityScolingRuleConflict(!attributes.isIdentityScolingRuleConflict());
      	  IdentityRulestag=true;
            identityRules = new ArrayList<OysterRule>();
        } else if (eName.equalsIgnoreCase("Indices")){
            indexingRules = new ArrayList<IndexRule>();
        } else if (eName.equalsIgnoreCase("ScoringRule")){
      	  attributes.setIdentityScolingRuleConflict(!attributes.isIdentityScolingRuleConflict());
      	  scoringruletag=true;
      	  scoringrule = new OysterScoringRule();
      	  parent = eName;
        } 
        else if (eName.equalsIgnoreCase("Rule")){
            rule = new OysterRule();
            parent = eName;
        } else if (eName.equalsIgnoreCase("Term")){
            parent = eName;
            similaritySignatures.clear();
            dataPrepSignatures.clear();
        } else if (eName.equalsIgnoreCase("Index")){
            indexRule = new IndexRule();
            parent = eName;
        } else if (eName.equalsIgnoreCase("Segment")){
        	segment = new IndexSegment();
            parent = eName;
            hash = null;
            dataprep=null;
        }
        
        // get XML attributes
        if (attrs != null) { 
            for (int i = 0; i < attrs.getLength(); i++) { 
                String aName = attrs.getLocalName(i); 
                // Attr name 
                if ("".equals(aName)) {
                    aName = attrs.getQName(i);
                } 
                
                String token = attrs.getValue(i).trim();
                
                if(aName.equalsIgnoreCase("System")){
                    attributes.setSystem(token);
                }
/*                
                else if(aName.equalsIgnoreCase("RefID")){
                    attributes.setRefID(token);
                }
*/ 
                else if(aName.equalsIgnoreCase("Item")){
                    if (parent.equalsIgnoreCase("Attribute")){
                        attribute.setName(token);
                        // prep so that no nulls exist for any item that is set.
                        attribute.setAlgorithm("");
                    } else if (parent.equalsIgnoreCase("Term")){
                        termItem = token;
                    } else if (parent.equalsIgnoreCase("Segment")){
                        segment.setItemName(token);
                    }
                } else if(aName.equalsIgnoreCase("CompareTo")){
                    compareTo = new HashSet<String>();
                    String [] temp = token.split("[;]");
                    compareTo.addAll(Arrays.asList(temp));
                } else if(aName.equalsIgnoreCase("Algo")){
                    attribute.setAlgorithm(token);
                } else if(aName.equalsIgnoreCase("Ident")){
                    if (parent.equalsIgnoreCase("Rule")) {
                        rule.setRuleIdentifer(token);
                    } else if (parent.equalsIgnoreCase("Index")) {
                        indexRule.setRuleIdentifier(token);
                    }else if (parent.equalsIgnoreCase("ScoringRule")) {
                  	  scoringrule.setRuleIdentifer(token);
                      }
                } else if((aName.toUpperCase()).startsWith("MATCHRESULT")){
                    matchResult = token;
            	    similaritySignatures.add(matchResult);
                } else if(aName.equalsIgnoreCase("Hash")){
                    hash = token;
                }else if((aName.toUpperCase()).startsWith("SIMILARITY")){
            	    similarity = token;
            	    similaritySignatures.add(similarity);
                }else if((aName.toUpperCase()).startsWith("DATAPREP")){
            	    dataprep = token;
            	    dataPrepSignatures.add(dataprep);
                }else if(aName.equalsIgnoreCase("AgreeWgt")){
            	    agreewgt = Double.parseDouble(token);
                }else if(aName.equalsIgnoreCase("WgtTable")){
            	    wgttable = token;
                }else if(aName.equalsIgnoreCase("DisagreeWgt")){
            	    disagreewgt = Double.parseDouble(token);
                }else if(aName.equalsIgnoreCase("Missing") || aName.equalsIgnoreCase("MissingWgt")){
            	    missing = Double.parseDouble(token);
            	    hasmissingweight=true;
                }else if(aName.equalsIgnoreCase("MatchScore")){
            	    scoringrule.setMatchScore(Double.parseDouble(token));
                }else if(aName.equalsIgnoreCase("ReviewScore")){
            	    scoringrule.setReviewScore(Double.parseDouble(token));
            	    hasReviewScore = true;
                }
            }
        }
    }

    /**
     * Called when the Ending of the current Element is reached. For example in 
     * the above explanation, this method is called when </Title> tag is reached
     * @param namespaceURI URI for this namespace
     * @param sName
     * @param qName qualified XML name
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endElement (String namespaceURI, String sName, String qName) throws org.xml.sax.SAXException {
        String eName = sName; // element name 
        if ("".equals(eName)) {
            eName = qName;
        }
        
        if (eName.equalsIgnoreCase("Attribute")){
            attributes.addAttribute(attribute);
            parent = "";
        } else if (eName.equalsIgnoreCase("IdentityRules")){
      	  IdentityRulestag=false;
            attributes.setIdentityRules(identityRules);
            attributes.setIdentityRulestag(true);

        } else if (eName.equalsIgnoreCase("Rule")){
      	  this.identityRules.add(rule);
        } else if (eName.equalsIgnoreCase("Term")) {
        	// Now at the end of an Identity (Boolean) Rule Term
			if (IdentityRulestag) {
				RuleTerm rt = new RuleTerm();
				if (termItem == null) {
					Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n################\n###ERROR: Item in Term is required in Attribute file!!!\n################\n");
					System.out.println("\n################\n###ERROR: Item in Term is required in Attribute file!!!\n################\n");
				} else {
					rt.setItem(termItem);
				}
				
				rt.setCompareTo(compareTo);
				// Create and set all of the Similarity and DataPrep Functions
				int similarityCount = similaritySignatures.size();
				int dataPrepCount = dataPrepSignatures.size();
				if (similarityCount > 0) {
					for (int j=0; j<similarityCount; j++) {
						String sig = similaritySignatures.get(j);
						rt.addSimilaritySignature(sig);
						try {
							rt.addSimilarityFunction(new OysterFunction.Builder<Compare>(sig).build());
						} catch (Exception ex) {
							System.out.println("\n################\n###ERROR:"+sig+" cannot be used for Similarity!!!\n################\n");
				    		  System.exit(0);
						}
					}
				} else {
					Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n################\n###ERROR: Similarity or DataPrep in Term is required in Attribute file!!!\n################\n");
					System.out.println("\n################\n###ERROR: At least one Similarity Function must be defined in each Rule Term!!!\n################\n");
					System.exit(0);
				}
				if (dataPrepCount>0) {
					for (int j=0; j<dataPrepCount; j++) {
						String sig = dataPrepSignatures.get(j);
						rt.addDataPrepSignature(sig);
						try {
							rt.addDataPrepFunction(new OysterFunction.Builder<Transform>(sig).build());
						} catch (Exception ex) {
							System.out.println("\n################\n###ERROR:"+sig+" cannot be used for DataPrep!!!\n################\n");
				    		  System.exit(0);							
						}
					}
				}
				
				rule.getTermList().add(rt);
				parent = "";
				compareTo = null;
				termItem = null;
				matchResult = null;
				similarity = null;
				dataprep = null;
			}

      	  if(scoringruletag){	
      		  // Now at the end of a ScoringRule Term
      		  ScoringRuleTerm srt=new ScoringRuleTerm();
      		  if (termItem == null) {
      			  Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n################\n###ERROR: Item in Term is required in Attribute file!!!\n################\n");
      			  System.out.println("\n################\n###ERROR: Item in Term is required in Attribute file!!!\n################\n");
      			  System.exit(0);
      		  } else {
      			  srt.setItem(termItem);
      		  }
      		  // Create and set the Similarity and Data Prep Functions
				int similarityCount = similaritySignatures.size();
				int dataPrepCount = dataPrepSignatures.size();
				if (similarityCount > 0) {
					for (int j=0; j<similarityCount; j++) {
						String sig = similaritySignatures.get(j);
						srt.addSimilaritySignature(sig);
						try {
							srt.addSimilarityFunction(new OysterFunction.Builder<Compare>(sig).build());
						} catch (Exception ex) {
							System.out.println("\n################\n###ERROR:"+sig+" cannot be used for Similarity!!!\n################\n");
				    		  System.exit(0);
						}
					}
				} else {
					Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n################\n###ERROR: Similarity or DataPrep in Term is required in Attribute file!!!\n################\n");
					System.out.println("\n################\n###ERROR: At least one Similarity Function must be defined in each Rule Term!!!\n################\n");
					System.exit(0);
				}
				if (dataPrepCount>0) {
					for (int j=0; j<dataPrepCount; j++) {
						String sig = dataPrepSignatures.get(j);
						srt.addDataPrepSignature(sig);
						try {
							srt.addDataPrepFunction(new OysterFunction.Builder<Transform>(sig).build());
						} catch (Exception ex) {
							System.out.println("\n################\n###ERROR:"+sig+" cannot be used for DataPrep!!!\n################\n");
				    		  System.exit(0);							
						}
					}
				}
      		  
      		  srt.setAgreewgt(agreewgt);
      		  srt.setWgttable(wgttable);
      		  srt.setDisagreewgt(disagreewgt);
      		  if(hasmissingweight){
      			  srt.setMissing(missing);
      			  srt.setHasmissingweight(true);
      		  }
      		  scoringrule.getTermList().add(srt);
      		  parent="";
      		  dataprep=null;
      		  wgttable=null;
      		  missing=0;
      		  hasmissingweight=false;
      		  termItem=null;
      		  
      	  }
        } else if (eName.equalsIgnoreCase("Indices")){
            attributes.setIndexingRules(indexingRules);
        } else if (eName.equalsIgnoreCase("Index")){
            this.indexingRules.add(indexRule);
        } else if (eName.equalsIgnoreCase("Segment")){
        	segment.setHashSignature(hash);
        	try {
        		segment.setHashFunction(new OysterFunction.Builder<Tokenize>(hash).build());
        	} catch (Exception ex) {
				System.out.println("\n################\n###ERROR:"+hash+" cannot be used for Hash!!!\n################\n");
	    		  System.exit(0);	
        	}
        	if (dataPrepSignatures.size()>1) {
				System.out.println("\n################\n###ERROR: Only one DataPrep can be defined per Index Segment!!!\n################\n");
	    		  System.exit(0);
        	}
        	segment.setDataPrepSignature(dataprep);
        	if (dataprep != null) {
        		try {
        			segment.setDataPrepFunction(new OysterFunction.Builder<Transform>(dataprep).build());
        		} catch (Exception ex) {
					System.out.println("\n################\n###ERROR:"+dataprep+" cannot be used for DataPrep!!!\n################\n");
		    		  System.exit(0);	
        		}
        	}
            indexRule.insertSegment(segment);
            parent = "";
        }
        else if(eName.equalsIgnoreCase("ScoringRule")){
      	  if(scoringrule.getRuleIdentifer()==null){
      		  Logger.getLogger(OysterMain.class.getName()).log(Level.SEVERE, "\n################\n###ERROR: Ident is required in Scoring Rule in Attribute file!!!\n################\n");
      		  System.out.println("\n################\n###ERROR: Ident is required in Scoring Rule in Attribute file!!!\n################\n");
      		  System.exit(0);
		  } else {
      		  attributes.setScoringRule(scoringrule);
      		  scoringruletag=false;
      		  attributes.setScoringruletag(true);
      		  // new code to set logic parameters
      		char parm1 = 'X';
      		char parm2 = 'S';
      		char parm3 = 'L';
      		char parm4 = 'E';
      		char parm5 = 'E';
      		char parm6 = 'N';
      		boolean errorInParms = false;
      		String parmString = scoringrule.getRuleIdentifer();
  			char[] parms = parmString.toCharArray();
  			if(parms[0]=='$') {
  				if(parms.length==7) {
  					char p1 = parms[1];
  					if((p1=='A' || p1=='B' || p1=='X')) {
  						parm1 = p1;
  					} else {
  						errorInParms = true;
  					}
  					char p2 = parms[2];
  					if(p2=='A' || p2=='L' || p2=='S') {
  						parm2 = p2;
  					} else {
  						errorInParms = true;
  					}
  					char p3 = parms[3];
  					if(p3=='A' || p3=='L' || p3=='S') {
  						parm3 = p3;
  					} else {
  						errorInParms = true;
  					}
  					char p4 = parms[4];
  					if(p4=='E' || p4=='T') {
  						parm4 = p4;
  					} else {
  						errorInParms = true;
  					}
  					char p5 = parms[5];
  					if(p5=='E' || p5=='T') {
  						parm5 = p5;
  					} else {
  						errorInParms = true;
  					}
  					char p6 = parms[6];
  					if(p6=='N' || p6=='B'|| p6=='E') {
  						parm6 = p6;
  					} else {
  						errorInParms = true;
  					}
  				} else {
  					errorInParms = true;
  				}
  	      		if(errorInParms) {
  	      			System.out.println(parmString+": parameter settings are invalid, parameters set to default value");

  	      		}
      		} else {
	      			System.out.println(parmString+": No parameter settings given , parmeters set to default value");
      		}
	      	scoringrule.setParm1(parm1);
	      	scoringrule.setParm2(parm2);
	      	scoringrule.setParm3(parm3);
	      	scoringrule.setParm4(parm4);
	      	scoringrule.setParm5(parm5);
	      	scoringrule.setParm6(parm6);
  			System.out.println("Scoring Rule Parameter Set as follows");
      		System.out.println("P1="+parm1+", P2="+parm2+" ,P3="+parm3+", P4="+parm4+", P5="+parm5+", P6="+parm6);	
      	  }
      	  // Added for version 3.7.4 to default review score to match score if review score not given
      	  if(hasReviewScore==false) scoringrule.setReviewScore(scoringrule.getMatchScore());
        }
    }
    
    /**
     * This method is the main entry point for the SAX Parser.
     * @param file the XML file to be parsed.
     * @return <code>OysterAttributes</code> containing data from the file.
     */
    public OysterAttributes parse(String file){
        // Use an instance of ourselves as the SAX event handler 
        DefaultHandler handler = new AttributesParser(); 
        // Use the default (non-validating) parser 
        SAXParserFactory factory = SAXParserFactory.newInstance(); 
        factory.setNamespaceAware(true);
        
        try {
            // Set up output stream 
            setOut(new OutputStreamWriter(System.out, "UTF8"));
            
            // Parse the input 
            SAXParser saxParser = factory.newSAXParser();
/*            
            if (saxParser.isNamespaceAware())
                System.out.println("Namespace Aware");
            else System.out.println("NOT Namespace Aware");
*/            
            saxParser.parse( new File(file), handler);
        } catch (IOException ex) {
            Logger.getLogger(AttributesParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(AttributesParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        } catch (SAXException ex) {
            Logger.getLogger(AttributesParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        }
        return attributes;
    }

//
//public boolean isIdentityScolingRuleConflict() {
//	return identityScolingRuleConflict;
//}
    
    // FIXME: Need to add Parser Level and XML Level validation
}

