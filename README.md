# README #

Description
Entity Resolution is the process by which a data-set is processed and records are identified that represent the same real-world entity.

OYSTER (Open sYSTem Entity Resolution) is an entity resolution system that supports probabilistic direct matching, transitive linking, and asserted linking. To facilitate prospecting for match candidates (blocking), the system builds and maintains an in-memory index of attribute values to identities. Because OYSTER has an identity management system, it also supports persistent identity identifiers. OYSTER is unique among other ER systems in that it is built to incorporate Entity Identity Information Management (EIIM). OYSTER supports EIIM by providing methods that enforce identifiers to be unique among identities, maintain persistent IDs over the life of an identity, and allowing the ability to fix false-positive and false-negative resolutions, which cannot be done with matching rules, through the use of assertion, trace-ability, and other features.

The original project is stored in [SourceForge](https://sourceforge.net/projects/oysterer/)

### What is this repository for? ###

This repository contains an updated version of the OYSTER project with the goals of:

* Converting it to a maven project so that dependencies are managed automatically and it is easier to build without having to have other knowledge. This will also allow for snapshot and release builds and automate unit testing.  
* Do a code review and refactor the code and separate it into multiple projects to facilitate team development with fewer merge conflicts.
* Identify and correct any know bugs
* Identify opportunities for performance improvements through code review and profiling
* Adding unit tests to the build to validate code changes

### How do I get set up? ###

Instructions forking the repository nnd getting started with the Eclipse IDE can be found in the Wiki through the menu on the left.

### Contribution guidelines ###

* To make contributions to this project you will need a bitbucket account, obviously
* Make sure there is an [issue](https://bitbucket.org/oysterer/oyster/issues?status=new&status=open) created to describe the change or bug.  Please include relevant information or attach samples to assist in recreating the issue and testing the fix.
* Fork the repository into a private branch under your account
* Make your changes
* Thoroughly test your changes using unit tests and A-B comparisons against the standard data-set
* Create a pull request to submit your changes
* Participate in the code review

For further information on development, setting up and environment, etc. please see the project [Wiki](https://bitbucket.org/oysterer/oyster/wiki/Home)

### Who do I talk to? ###

* Repo administrators
* Other community or team contact
* [Bitbucket Tutorials](https://confluence.atlassian.com/bitbucket/bitbucket-tutorials-teams-in-space-training-ground-755338051.html)
* [Bitbucket markdown](https://bitbucket.org/tutorials/markdowndemo)